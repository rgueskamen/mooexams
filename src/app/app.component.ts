

import { Component, ViewChild } from '@angular/core';
import {Nav, Platform, Config, MenuController, Events} from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { LoaderPage } from '../pages/loader/loader';
import { HomeParentPage } from "../pages/home-parent/home-parent";
import { ContactPage } from "../pages/contact/contact";
import { FeedbacksPage } from "../pages/feedbacks/feedbacks";
import { MyCoursePage } from "../pages/my-course/my-course";
import { MyChallengePage } from "../pages/my-challenge/my-challenge";
import { ParamRevisionPage} from "../pages/param-revision/param-revision";
import { ParamExamPage } from "../pages/param-exam/param-exam";
import { LeaderBoardPage } from "../pages/leader-board/leader-board";
import { InviteUserPage } from "../pages/invite-user/invite-user";
import { SurveyPage } from './../pages/survey/survey';


import { TranslateService } from '@ngx-translate/core';
import { ApiProvider } from "../providers/api/api";
import { ChallengeAttendedPage} from "../pages/challenge-attended/challenge-attended";
import { UserProvider} from "../providers/user/user";
// import { OneSignal } from '@ionic-native/onesignal';




@Component({
    templateUrl: 'app.html'
})

export class MooExams {

    @ViewChild(Nav) nav: Nav;

    rootPage: any;
    keys : string[];
    user:any;
    userClasse:any;
    logo:string;


    pages: Array<{title: string,icon: string,component: any,role:number}>;

    constructor(
        private platform: Platform,
        private statusBar: StatusBar,
        private splashScreen: SplashScreen,
        private storage :Storage,
        private translate: TranslateService,
        private config: Config,
        public api : ApiProvider,
        public menuCtrl: MenuController,
        public userService:UserProvider,
       // private oneSignal: OneSignal,
        public events:Events


    ) {

        this.logo="assets/img/MooExams_00.png";

        //Get the user informations
        this.storage.get('user').then(data=>{
            if(data){
                this.user=data;
            }
        });

        // Update  the user informations
        events.subscribe('user:created', (user, time) => {
            this.user=user;
        });

         // Update  the user informations
          events.subscribe('user:updated', (user, time) => {
            this.user=user;
         });

        //Get the user classe
        this.storage.get('user-classes').then(data=>{
            this.userClasse=data;
        });

        // Update  the user classes
         events.subscribe('user:classes', (classe, time) => {
            this.userClasse=classe;
         });

        // Set the default language for translation strings, and the current language.

        this.translate.setDefaultLang('fr');

        if (this.translate.getBrowserLang() !== undefined) {
            this.translate.use(this.translate.getBrowserLang());
        } else {
            this.translate.use('fr'); // Set your language here
        }

        this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
            this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
        });

        this.keys=["MENU_OPTION1", "MENU_OPTION2","MENU_OPTION3", "MENU_OPTION4", "MENU_OPTION5","MENU_OPTION10","MENU_OPTION11","CHALLENGE_USER","MY_COURSES","CHALLENGE_USER_PARTICIPATE","GROUP_OF_FRIENDS","MENU_OPTION12","MENU_OPTION13"];

        this.translate.get(this.keys).subscribe((value) => {

            this.pages = [
                { title: value.MENU_OPTION1,icon:'home',component: HomePage,role:4},
                { title: value.MENU_OPTION13,icon:'barcode',component: SurveyPage,role:0},
                { title: value.MENU_OPTION2,icon:'document',component:ParamRevisionPage,role:4},
                { title: value.MENU_OPTION3,icon:'paper',component: ParamExamPage,role:4 },
                { title: value.MENU_OPTION4,icon:'stats',component: LeaderBoardPage,role:4 },
                { title: value.CHALLENGE_USER,icon:'game-controller-b',component: MyChallengePage,role:4},
                { title: value.CHALLENGE_USER_PARTICIPATE,icon:'star',component: ChallengeAttendedPage,role:4 },
                { title: value.GROUP_OF_FRIENDS,icon:'person-add',component: InviteUserPage,role:4 },//6
                { title: value.MENU_OPTION12,icon:'share',component: 'share',role:0},
                { title: value.MENU_OPTION1,icon:'home',component: HomeParentPage,role:5},
                { title: value.MENU_OPTION1,icon:'home',component: HomeParentPage,role:6},
                { title: value.MY_COURSES,icon:'document',component: MyCoursePage,role:6},
                { title: value.MENU_OPTION10,icon:'mail',component: ContactPage,role:0},
                { title: value.MENU_OPTION11,icon:'help-circle',component: FeedbacksPage,role:0}
            ];

        });

        /* this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.styleDefault();
            this.splashScreen.hide();

         this.oneSignal.startInit("6912bccf-4c63-4808-b24d-364401b71f25", "640915688016");
            this.oneSignal.handleNotificationReceived().subscribe((jsonData)=>{
            });
            this.oneSignal.handleNotificationOpened().subscribe((jsonData)=> {
            });
            this.oneSignal.endInit();

        });*/

        this.verifyLogin();

        this.api.getCountryByIpAdress();

    }

    initUserInfos(){

        this.storage.get('token').then(token=> {
            if(token)
                this.userService.userInfos(token);
        });
    }


    verifyLogin(){

        //check if a user is login and make redirection
        this.storage.get('user-is-login').then(value=>{

            if(value){

                this.initUserInfos();
                //Get the user role
                this.storage.get('user').then(data=>{

                    this.user=data;
                    switch (data.role_id){

                        case 4:
                            this.rootPage=HomePage;
                            break;

                        case 5:
                            this.rootPage=HomeParentPage;
                            break;

                        case 6:
                            this.rootPage=HomeParentPage;
                            break;

                        default:
                            this.rootPage=HomePage;
                            break;
                    }

                });

            }else{
                this.rootPage=LoaderPage;
            }

        });

    }


    openPage(page){

        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario

        if(page.icon=='person-add'){

            this.storage.get('users-friends').then(user=>{

                if(user){
                    this.nav.setRoot(page.component,{users:user,matiere:null});
                }else{
                    this.nav.setRoot(page.component,{users:[],matiere:null});
                }
            });

        }else if(page.component=='share'){

            let data={
                message:"Invitez vos amis à télécharger l'application MooExams afin de booster leur performance scolaire.\n\n",
                name:"MooExams",
                image:"",
                link:"https://goo.gl/u34l3k"
            };


            this.api.share(data).then(reponse=>{

            }).catch(err=>{
                this.translate.get(['SHARE_MESSAGE_FAILLED']).subscribe(value=>{
                    this.userService.toastAlert(value.SHARE_MESSAGE_FAILLED);
                });
            });

        }else{
            this.nav.setRoot(page.component);
        }

    }
}
