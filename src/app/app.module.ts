

import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { IonicStorageModule } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Camera } from '@ionic-native/camera';
import { DatePicker } from '@ionic-native/date-picker';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { EmailComposer } from '@ionic-native/email-composer';
import { SocialSharing } from '@ionic-native/social-sharing';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { Geolocation } from '@ionic-native/geolocation';


/*Manage translation*/
//run this command to load the package : npm install @ngx-translate/core --save
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
//run this command to load the package : npm install @ngx-translate/http-loader --save
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { HideHeaderDirective } from '../directives/hide-header/hide-header';


import { MooExams } from './app.component';


import { HomePage } from '../pages/home/home';
import { LoaderPage } from '../pages/loader/loader';
import { LoginPage } from '../pages/login/login';
import { ForgotPassPage } from '../pages/forgot-pass/forgot-pass';
import { TermesPage } from '../pages/termes/termes';
import { WelcomePage} from '../pages/welcome/welcome';
import { ModalClassesPage } from '../pages/modal-classes/modal-classes';
import { ModalMatieresPage } from "../pages/modal-matieres/modal-matieres";
import { ModalPaysPage } from "../pages/modal-pays/modal-pays";
import { ParamExamPage } from '../pages/param-exam/param-exam';
import { ParamRevisionPage } from  '../pages/param-revision/param-revision';
import { TimeTablePage } from '../pages/time-table/time-table';
import { MatiereClassePage } from "../pages/matiere-classe/matiere-classe";
import { LeconsPage } from '../pages/lecons/lecons';
import { QuestionRevisionPage } from '../pages/question-revision/question-revision';
import { QuestionDefisPage } from "../pages/question-defis/question-defis";
import { QuestionChallengePage } from "../pages/question-challenge/question-challenge";
import { ForumPage } from "../pages/forum/forum";
import { ForumCommentsPage } from "../pages/forum-comments/forum-comments"
import { ProfilPage } from '../pages/profil/profil';
import { HomeParentPage } from '../pages/home-parent/home-parent';
import { ManageAccountPage } from "../pages/manage-account/manage-account";
import { LeaderBoardPage } from "../pages/leader-board/leader-board";
import { ReportsPage } from "../pages/reports/reports";
import { InviteUserPage } from "../pages/invite-user/invite-user";
import { AddPostPage } from '../pages/add-post/add-post';
import { AddUserPage } from '../pages/add-user/add-user';
import { DetailUserPage } from '../pages/detail-user/detail-user';
import { AlertSettingPage } from '../pages/alert-setting/alert-setting';
import { CourseResumePage } from '../pages/course-resume/course-resume';
import { ModalBadgePage } from '../pages/modal-badge/modal-badge';
import { ModalSuccessPage } from "../pages/modal-success/modal-success";
import { ModalFailedPage } from "../pages/modal-failed/modal-failed";
import { ResultsPage } from "../pages/results/results";
import { AnswersPage } from '../pages/answers/answers';
import { ChallengePage } from "../pages/challenge/challenge";
import { ChallengeOptionPage } from "../pages/challenge-option/challenge-option";
import { CreateChallengePage} from "../pages/create-challenge/create-challenge";
import { ChallengeAttendedPage} from "../pages/challenge-attended/challenge-attended";
import { ContactPage  } from "../pages/contact/contact";
import { FeedbacksPage } from "../pages/feedbacks/feedbacks";
import { MyChallengePage } from '../pages/my-challenge/my-challenge';
import { ChallengeUserPage } from '../pages/challenge-user/challenge-user';
import { ChallengeParticipantsPage } from "../pages/challenge-participants/challenge-participants";
import { ChallengeResultPage } from "../pages/challenge-result/challenge-result";
import { MyCoursePage } from "../pages/my-course/my-course";
import { SurveyPage } from './../pages/survey/survey';
import { ConcoursDashPage } from './../pages/concours-dash/concours-dash';
import { ConcoursQuestionsPage } from './../pages/concours-questions/concours-questions';
import { ModalConcoursPage } from './../pages/modal-concours/modal-concours';





import { ApiProvider } from '../providers/api/api';
import { UserProvider } from '../providers/user/user';
import { ClasseProvider } from '../providers/classe/classe';
import { MatiereProvider } from '../providers/matiere/matiere';
import { LeconsProvider } from '../providers/lecons/lecons';
import { ScoreProvider } from '../providers/score/score';
import { QuestionsProvider } from '../providers/questions/questions';
import { TimetableProvider } from '../providers/timetable/timetable';
import { ChallengeProvider } from '../providers/challenge/challenge';
import { FeedbacksProvider } from '../providers/feedbacks/feedbacks';
import { ForumProvider } from '../providers/forum/forum';
import { AccessProvider } from '../providers/access/access';
import { ConcoursProvider } from './../providers/concours/concours';

import { OneSignal } from '@ionic-native/onesignal';
import { Ionic2RatingModule } from 'ionic2-rating';
import { NativeAudio } from '@ionic-native/native-audio';


import { User } from '../models/user';
import { ModalMessagePage } from "../pages/modal-message/modal-message";

import { TeacherStudentsPage} from "../pages/teacher-students/teacher-students";
import { MyStudentsPage } from "../pages/my-students/my-students";
import { HttpClient, HttpClientModule } from "@angular/common/http";


// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}





@NgModule({

    declarations: [
        MooExams,
        HomePage,
        LoaderPage,
        LoginPage,
        ForgotPassPage,
        TermesPage,
        WelcomePage,
        ModalClassesPage,
        ModalMatieresPage,
        ModalPaysPage,
        ParamExamPage,
        ParamRevisionPage,
        TimeTablePage,
        MatiereClassePage,
        LeconsPage,
        QuestionRevisionPage,
        QuestionDefisPage,
        QuestionChallengePage,
        ForumPage,
        ForumCommentsPage,
        ProfilPage,
        HomeParentPage,
        ManageAccountPage,
        LeaderBoardPage,
        ReportsPage,
        InviteUserPage,
        AddPostPage,
        AddUserPage,
        DetailUserPage,
        AlertSettingPage,
        CourseResumePage,
        ModalBadgePage,
        ModalSuccessPage,
        ModalFailedPage,
        ResultsPage,
        AnswersPage,
        ChallengePage,
        ChallengeOptionPage,
        CreateChallengePage,
        ChallengeAttendedPage,
        ContactPage,
        FeedbacksPage,
        MyChallengePage,
        ChallengeUserPage,
        ChallengeParticipantsPage,
        ChallengeResultPage,
        MyCoursePage,
        ModalMessagePage,
        TeacherStudentsPage,
        MyStudentsPage,
        SurveyPage,
        ConcoursDashPage,
        ConcoursQuestionsPage,
        ModalConcoursPage,
        HideHeaderDirective
    ],

    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        Ionic2RatingModule,
        HttpClientModule,
        IonicModule.forRoot(MooExams, {
            tabsPlacement: 'bottom',
            platforms: {
                ios: {
                    tabsPlacement: 'top',
                }
            }
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        IonicStorageModule.forRoot({
            name: '__mooexamsdb',
            driverOrder: ['sqlite', 'websql','indexeddb']
        })
    ],

    bootstrap: [IonicApp],

    entryComponents: [
        MooExams,
        HomePage,
        LoaderPage,
        LoginPage,
        ForgotPassPage,
        TermesPage,
        WelcomePage,
        ModalClassesPage,
        ModalMatieresPage,
        ModalPaysPage,
        ParamExamPage,
        ParamRevisionPage,
        TimeTablePage,
        MatiereClassePage,
        LeconsPage,
        QuestionRevisionPage,
        QuestionDefisPage,
        QuestionChallengePage,
        ForumPage,
        ForumCommentsPage,
        ProfilPage,
        HomeParentPage,
        ManageAccountPage,
        LeaderBoardPage,
        ReportsPage,
        InviteUserPage,
        AddPostPage,
        AddUserPage,
        DetailUserPage,
        AlertSettingPage,
        CourseResumePage,
        ModalBadgePage,
        ModalSuccessPage,
        ModalFailedPage,
        ResultsPage,
        AnswersPage,
        ChallengePage,
        ChallengeOptionPage,
        CreateChallengePage,
        ChallengeAttendedPage,
        ContactPage,
        FeedbacksPage,
        MyChallengePage,
        ChallengeUserPage,
        ChallengeParticipantsPage,
        ChallengeResultPage,
        MyCoursePage,
        ModalMessagePage,
        TeacherStudentsPage,
        SurveyPage,
        MyStudentsPage,
        ConcoursDashPage,
        ConcoursQuestionsPage,
        ModalConcoursPage
    ],

    providers: [
        StatusBar,
        SplashScreen,
        Camera,
        DatePicker,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        ApiProvider,
        UserProvider,
        ClasseProvider,
        MatiereProvider,
        LeconsProvider,
        ScoreProvider,
        QuestionsProvider,
        TimetableProvider,
        ChallengeProvider,
        FeedbacksProvider,
        ForumProvider,
        ConcoursProvider,
        YoutubeVideoPlayer,
        EmailComposer,
        User,
        OneSignal,
        AccessProvider,
        SocialSharing,
        InAppBrowser,
        NativeGeocoder,
        Geolocation,
        ConcoursProvider,
        NativeAudio
        
    ]
})
export class AppModule {}
