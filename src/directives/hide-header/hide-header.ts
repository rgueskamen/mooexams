import {Directive, Input, ElementRef, Renderer, EventEmitter} from '@angular/core';

/**
 * Generated class for the HideHeaderDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
@Directive({
  selector: '[hide-header]', // Attribute selector
   host: {
    '(ionScroll)': 'onContentScroll($event)'
  },
    outputs: ['headerShow']
})
export class HideHeaderDirective {
  headerShow=new EventEmitter();

  @Input("header") header: HTMLElement;
  headerHeight;
  scrollContent;


  constructor(
    public element: ElementRef, 
    public renderer: Renderer
  ) {

  }

  ngOnInit(){
    this.headerShow.emit(false);
    this.headerHeight = this.header.clientHeight;
    this.renderer.setElementStyle(this.header, 'webkitTransition', 'top 700ms');
    this.scrollContent = this.element.nativeElement.getElementsByClassName("scroll-content")[0];
    this.renderer.setElementStyle(this.scrollContent, 'webkitTransition', 'margin-top 700ms');
  }

  onContentScroll(event){

    if(event.scrollTop > 152){
      this.headerShow.emit(true);
      this.renderer.setElementStyle(this.header, "top", "-152px");
      this.renderer.setElementStyle(this.scrollContent, "margin-top", "0px");
    } else {
      this.headerShow.emit(false);
      this.renderer.setElementStyle(this.header, "top", "0px");
      this.renderer.setElementStyle(this.scrollContent, "margin-top", "152px");
    }
  }
  
}
