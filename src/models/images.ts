
export class Images{

    id:number;
    url ?:string;
    value ?:string;
    commentaire ?:string;
    question_id :number;
    created_at ?:Date;
    updated_at ?:Date;

}