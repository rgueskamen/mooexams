

/*Modele d'une Matiere*/
export class Matiere{

    id : number ;
    id_groupe?:number;
    NOM_MATIERE :string ;
    image ?: string ;
    banniere_phone ?:string ;
    banniere_tablette ?:string ;
    created_at ?:Date;
    updated_at ?:Date;

}