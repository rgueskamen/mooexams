
/*Modele d'un participant*/
export class Participants{

    id:number;
    id_challenge:number;
    participant_id:number;
    nbrePoints:number;
    bonne_reponse:number;
    mauvaise_reponse:number;
    liste_bonne_reponse:string; //ids des questions séparés par des virgules
    liste_mauvaise_reponse:string; //ids des questions séparés par des virgules
    state:number;
    date_invitation:Date;
    date_participation:Date;
    date_annulation:Date;
    date_modification:Date;
}