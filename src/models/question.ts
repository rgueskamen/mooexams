
import { Possibilite } from '../models/possibilite';
import { Images } from '../models/images';

export  class  Question{

    id : number;
    intitule: string;
    choix ?:boolean;
    value ?:number;
    bonne_reponse: Possibilite;
    commentaire_reponse:string;
    image_commentaire:string;
    images:Array<Images>;
    possibilites:Array<Possibilite>;

}