export  class  Resultat{

    id:number;
    mode:number;
    id_user:number;
    id_matiere:number;
    id_lecon:number;
    id_niveau:number;
    liste_bonne_reponse:string;
    liste_mauvaise_reponse:string;
    nbrePoints:number;
    nbre_bonne_reponse:number;
    nbre_mauvaise_reponse:number;
    annee:string
    created_at:Date;
    updated_at:Date;

}