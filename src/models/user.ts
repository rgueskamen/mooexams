
/*Modele d'un utilisateur*/
export class User{

    id?:number;
    username?:string;
    password?:string;
    role_id?:number;
    seen?:number;
    valid?:number;
    confirmed?:number;
    confirmation_code?:string;
    created_at ?:Date;
    updated_at ?:Date;
    remember_token ?:string;
    nom ?: string;
    prenom ?: string;
    tel ?: string;
    image_url ?: string;
    image_data ?: string;
    parent_id ?: number;
    teacher_id ?:number;
    rapport ?: string;
    device_id ?:string;
    nombres_sessions ?:string;
    gender:string ;
    language :string;
    points ?:number;
    badge ?:string;
    longitude?:number;
    latitude?:number;
    pays ?: string;
    ville ?: string;
    adresse ?:string;
}