import { Component,ViewChild,ElementRef } from '@angular/core';
import { AlertController, LoadingController, NavController, NavParams, ViewController } from 'ionic-angular';
import { FormBuilder,Validators } from "@angular/forms";
import { ForumProvider } from "../../providers/forum/forum";
import { Storage } from "@ionic/storage";
import { UserProvider } from "../../providers/user/user";
import { MatiereProvider } from "../../providers/matiere/matiere";
import { TranslateService } from "@ngx-translate/core";
import {ApiProvider} from "../../providers/api/api";

/**
 * Generated class for the AddPostPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-add-post',
    templateUrl: 'add-post.html',
})
export class AddPostPage {

   @ViewChild('message') message : ElementRef;

    user:any;
    listMatieres:any;
    testRadioResult:string;
    forumLoading:any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public viewCtrl : ViewController,
        public fb : FormBuilder,
        public storage:Storage,
        public forum : ForumProvider,
        public translate:TranslateService,
        public userService: UserProvider,
        public loader : LoadingController,
        public matiere:MatiereProvider,
        public alertCtrl: AlertController,
        public api:ApiProvider
    ) {

        this.storage.get('user').then(user=>{
            this.user=user;
            this.postForm.controls['user_id'].setValue(user.id);
        });
        this.listMatieres=[];
        this.testRadioResult="";
    }

    public postForm =  this.fb.group({
        title: [""],
        slug: [""],
        summary: ["",  Validators.compose([Validators.required,Validators.maxLength(1000)])],
        seen: [0],
        active: [1],
        user_id:[""],
        matiere_id:["",Validators.compose([Validators.required])],
        tag:[""],
        image:[""],
        video:[""],
        audio:[""]
    });

    // this.postForm.controls['matiere_id'].setValue(data.id);


    ionViewDidLoad() {
        this.resize();
    }


    resize(){

        if(this.message['_elementRef']) {
            let element = this.message['_elementRef'].nativeElement.getElementsByClassName("text-input")[0];
            element.style.overflow = 'hidden';
            element.style.height = 'auto';
            element.style.height = element.scrollHeight + 'px';
        }
    }



    //Get the picture of the comments
    getPostPicture(){

        this.api.takePicture().then(imagedata=>{
            this.postForm.controls['image'].setValue(imagedata);
        }).catch(err=>{

            if(err==="none"){
                this.translate.get(['PICTURE_ERROR_MESSAGE']).subscribe(value=>{
                    this.userService.toastAlert(value.PICTURE_ERROR_MESSAGE);
                });
            }
        });
    }



    //Format the list of courses of the forum
    radioList(){

        let alert = this.alertCtrl.create();

        this.translate.get(['FORUM_SELECT_TEXT']).subscribe(value=>{
            alert.setTitle(value.FORUM_SELECT_TEXT);
        });

        for (let i = 0; i < this.listMatieres.length; i++) {

            if(this.listMatieres[i]){
                alert.addInput({
                    type: 'radio',
                    label: this.listMatieres[i].NOM_MATIERE,
                    value: this.listMatieres[i],
                    checked: false
                });
            }
        }

        alert.addButton('Cancel');

        alert.addButton({
            text: 'OK',
            handler: data => {

                if(data){
                    this.postForm.controls['matiere_id'].setValue(data.id);
                    this.testRadioResult=data.NOM_MATIERE;
                }
            }
        });

        alert.present();
    }

    //select the course to add on the post
    selectCourse(event){

            this.matiere.list().then(res=>{

                this.storage.get('matieres').then(matieres=>{
                    this.listMatieres=matieres;

                    if(this.listMatieres&&this.listMatieres.length>0) {
                        this.radioList();
                    }else{

                        let matiere= [];
                        let data=res.json();

                        if(data){

                            Object.keys(data).map(function(key) {
                                matiere.push(data[key]);
                            });

                            this.listMatieres=matiere;
                            this.radioList();

                        }else{

                            this.translate.get(['FORUM_COURSE_MESSAGE']).subscribe(value=> {
                                this.userService.toastAlert(value.FORUM_COURSE_MESSAGE);
                            });

                        }

                    }

                });

            });
    }

    //added a new post
    addaPost(data:any){

        this.forumLoading = this.loader.create({
            content: '',
            dismissOnPageChange:true,
            duration: 60000
        });

        this.storage.get('token').then(token=>{

            this.forumLoading.present();

            this.forum.addPost(data,token)
                .then(res=>{
                    this.forumLoading.dismiss();
                    if(res && res.reponse==="success"){

                        this.translate.get(['FORUM_POST_ADD_SUCCED']).subscribe(value=> {
                            this.userService.toastAlert(value.FORUM_POST_ADD_SUCCED);
                        });

                        this.storage.set('posts',res);
                    }

                    this.viewCtrl.dismiss();

                }).catch(err=>{

                    this.viewCtrl.dismiss();
                    this.forumLoading.dismiss();

                    if(err && err.reponse==="failure"){
                        this.translate.get(['FORUM_POST_ADD_FAILED']).subscribe(value=> {
                            this.userService.toastAlert("");
                        });
                    }

                });
        });

    }

    //close the modal
    close(){
        this.viewCtrl.dismiss();
    }

}
