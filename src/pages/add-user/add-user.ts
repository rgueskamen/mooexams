import { Component } from '@angular/core';
import { LoadingController, ModalController, NavController, NavParams } from 'ionic-angular';
import { FormBuilder,Validators } from "@angular/forms";
import { Storage  } from "@ionic/storage";
import { ModalClassesPage } from '../modal-classes/modal-classes';
import { UserProvider } from "../../providers/user/user";
import {TranslateService} from "@ngx-translate/core";
import {HomeParentPage} from "../home-parent/home-parent";
import {ModalMessagePage} from "../modal-message/modal-message";

/**
 * Generated class for the AddUserPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-add-user',
    templateUrl: 'add-user.html',
})

export class AddUserPage {

    logo:string;
    isActive:boolean;
    annees:string[];
    userInfos:any;
    classe : string;
    loader:any;
    currentUserInfos:any;
    matiereUser:any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public loadingCtrl: LoadingController,
        public fb:FormBuilder,
        public storage:Storage,
        public modalCtrl:ModalController,
        public user:UserProvider,
        public translate:TranslateService
    ) {

        this.logo="assets/img/MooExams_pre.png";
        this.annees=[];
        this.classe="";
        this.isActive=false;
        this.currentUserInfos="";
        this.matiereUser=this.navParams.get('matiere');
        this.setSchoolYear();

    }




    ionViewDidLoad() {
        this.initUserForm();
    }


    // Get device token
    getDeviceToken(){
        this.storage.get('device-token').then(token=>{
            if(token)
                this.userForm.controls["token"].setValue(token);
        });
    }


    //show the current academic year
    setSchoolYear(){
        let date = new Date();
        let an = date.getFullYear();
        let annee1=(an-1)+"-"+an;
        let annee2=an+"-"+(an+1);
        this.annees.push(annee1);
        this.annees.push(annee2);
    };

    //Show and hide the password
    showPass(){
        this.isActive ?  this.isActive=false: this.isActive=true;
    }


    //Select your classe
    selectClass(){

        let modal = this.modalCtrl.create(ModalClassesPage);

        modal.onDidDismiss(data => {

            if(data){
                this.userForm.controls['id_classe'].setValue(data.id);
                if(data.OPTION_CLASSE!='NONE'){
                    this.classe=data.NOM_CLASSE+" "+data.OPTION_CLASSE;
                }else{
                    this.classe=data.NOM_CLASSE;
                }
            }

        });
        modal.present();
    }

    //set user role
    setRole(user:any){

        switch (user.role_id){
            case  5 :
                this.userForm.controls['user_role'].setValue("parent");
                break;
            case  6 :
                this.userForm.controls['user_role'].setValue("enseignant");
                this.userForm.controls['id_matiere'].setValue(this.matiereUser.id);
                break;
            default:
                break;
        }

    }


    //Go to the dashBoard
    openDash(){
        this.navCtrl.setRoot(HomeParentPage);
    }


    public userForm=this.fb.group({
        "user_id":[],
        "user_role":[""],
        "roles":[""],
        "id_classe":[""],
        "annee":[""],
        "nom":["", Validators.compose([Validators.required,Validators.minLength(4),Validators.maxLength(50)])],
        "prenom":["",Validators.compose([Validators.required,Validators.minLength(4),Validators.maxLength(50)])],
        "id_matiere":[""],
        "tel":["", Validators.compose([Validators.required,Validators.pattern('^[0-9]{9,13}')])],
        "email":["", Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z0-9_.]+@[a-zA-Z0-9_.]+\\.[a-zA-Z]{2,5}')])],
        "username":["",Validators.compose([Validators.required,Validators.minLength(4),Validators.maxLength(50)])],
        "password":["",Validators.compose([Validators.required,Validators.minLength(4),Validators.maxLength(50)])],
        "token":[""]
    });


    //init the user Form
    initUserForm(){

        this.userForm=this.fb.group({
            "user_id":[this.navParams.get('user').id,Validators.compose([Validators.required])],
            "user_role":["",Validators.compose([Validators.required])],
            "roles":["student",Validators.compose([Validators.required])],
            "id_classe":["",Validators.compose([Validators.required])],
            "annee":["",Validators.compose([Validators.required])],
            "nom":["", Validators.compose([Validators.required,Validators.minLength(4),Validators.maxLength(50)])],
            "prenom":["",Validators.compose([Validators.required,Validators.minLength(4),Validators.maxLength(50)])],
            "id_matiere":["null"],
            "tel":["", Validators.compose([Validators.required,Validators.pattern('^[0-9]{9,13}')])],
            "email":["", Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z0-9_.]+@[a-zA-Z0-9_.]+\\.[a-zA-Z]{2,5}')])],
            "username":["",Validators.compose([Validators.required,Validators.minLength(4),Validators.maxLength(50)])],
            "password":["",Validators.compose([Validators.required,Validators.minLength(4),Validators.maxLength(50)])],
            "token":[""]
        });

        this.userInfos=this.navParams.get('user');
        this.setRole(this.userInfos);
        this.getDeviceToken();

    }


    //init the user Form
    initUserDataForm(data){

        this.userForm=this.fb.group({
            "user_id":[data.user_id,Validators.compose([Validators.required])],
            "user_role":[data.user_role,Validators.compose([Validators.required])],
            "roles":["student",Validators.compose([Validators.required])],
            "id_classe":[data.id_classe,Validators.compose([Validators.required])],
            "annee":[data.annee,Validators.compose([Validators.required])],
            "nom":[data.nom, Validators.compose([Validators.required,Validators.minLength(4),Validators.maxLength(50)])],
            "prenom":[data.prenom,Validators.compose([Validators.required,Validators.minLength(4),Validators.maxLength(50)])],
            "id_matiere":[data.id_matiere],
            "tel":[data.tel, Validators.compose([Validators.required,Validators.pattern('^[0-9]{9,13}')])],
            "email":[data.email, Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z0-9_.]+@[a-zA-Z0-9_.]+\\.[a-zA-Z]{2,5}')])],
            "username":[data.username,Validators.compose([Validators.required,Validators.minLength(4),Validators.maxLength(50)])],
            "password":[data.password,Validators.compose([Validators.required,Validators.minLength(4),Validators.maxLength(50)])],
            "token":[data.token]
        });

    }


    //Form input controls
    get clas() { return this.userForm.get('id_classe'); }
    get annee() { return this.userForm.get('annee'); }
    get matiere() { return this.userForm.get('id_matiere'); }
    get nom() { return this.userForm.get('nom'); }
    get prenom() { return this.userForm.get('prenom'); }
    get tel() { return this.userForm.get('tel'); }
    get email() { return this.userForm.get('email'); }
    get username() { return this.userForm.get('username'); }
    get password() { return this.userForm.get('password'); }


    //Show the level of the user
    openMessage(message){

        let modal = this.modalCtrl.create(ModalMessagePage,{message:message});

        modal.onDidDismiss(data => {
        });
        modal.present();
    }

    //Save the user informations on the server
    saveUserData(data:any){
        this.loader = this.loadingCtrl.create({
            content: "",
            dismissOnPageChange:true,
            duration: 60000
        });
        this.loader.present();

            this.storage.get('token').then(token=>{

                this.currentUserInfos=data;
                this.user.createAccount(data,token)
                    .then(data=>{
                        this.loader.dismiss();
                        if(data&&data.token=="success"){

                            this.translate.get(['USER_ADD_MESSAGE']).subscribe(value=>{
                                this.user.toastAlert(value.USER_ADD_MESSAGE);
                                this.openMessage(value.USER_ADD_MESSAGE);
                            });

                            if(data&&data.students&&data.students.length>0){
                                this.storage.set('user-students',data);
                            }
                            this.initUserForm();
                        }else{
                            this.initUserDataForm(this.currentUserInfos);
                        }
                    }).catch(err=>{
                        this.initUserDataForm(this.currentUserInfos);
                        this.loader.dismiss();
                    });
            });


    }


}
