import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams, ViewController } from 'ionic-angular';
import { TranslateService } from "@ngx-translate/core";
import { UserProvider } from "../../providers/user/user";
import { Storage } from "@ionic/storage";
import {ProfilPage} from "../profil/profil";

/**
 * Generated class for the AlertSettingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-alert-setting',
    templateUrl: 'alert-setting.html',
})
export class AlertSettingPage {

    periode:Array<{periode:string,title:string,choix:boolean}>;
    loader:any;
    logo:string;
    choix:string;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public viewCtrl:ViewController,
        public translate:TranslateService,
        public loadingCtrl:LoadingController,
        public user:UserProvider,
        public storage:Storage
    ) {

        this.logo="assets/img/MooExams_pre.png";
        this.choix= "";
        this.periode=[{periode:"Daily",title:"",choix:false},{periode: "Weekly",title:"",choix:false},{periode :"Monthly",title:"",choix:false}];
    }

    ionViewDidLoad() {
        this.initForm();
    }

    //Update the user choice
    updateChoix(index:number){
        this.periode[index].choix=!this.periode[index].choix;
        this.periode[index].choix? this.choix=this.periode[index].periode:this.choix="";

        for(let i=0;i<this.periode.length;i++){
                if(index!=i){
                    this.periode[i].choix=false;
                }
        }
    }


    // Init the form
    initForm(){

        this.storage.get('user').then(user=>{


            this.translate.get(["PERIODE_DAILY","PERIODE_WEEKLY","PERIODE_MONTHLY"]).subscribe(value=>{
                this.periode=[{periode:"Daily",title:value.PERIODE_DAILY,choix:false},{periode: "Weekly",title: value.PERIODE_WEEKLY,choix:false},{periode :"Monthly",title :value.PERIODE_MONTHLY,choix:false}];
            });

            if(user&&user.rapport){

                switch(user.rapport){
                    case "Daily":
                        this.periode[0].choix=true;
                        this.choix=this.periode[0].periode;
                        break;
                    case "Weekly":
                        this.periode[1].choix=true;
                        this.choix=this.periode[1].periode;
                        break;

                    case "Monthly":
                        this.periode[2].choix=true;
                        this.choix=this.periode[2].periode;
                        break;

                    default:
                        break;

                }
            }

        });

    }

    //Close the page
    close(){
        this.viewCtrl.dismiss();
    }

    //Go to the user profile
    goProfile(){
        this.navCtrl.setRoot(ProfilPage);
    }


    //Save the mail period receiver
    savePeriode(periode:string){


        this.loader = this.loadingCtrl.create({
            content: '',
            dismissOnPageChange:true,
            duration: 60000
        });

        this.loader.present();

        let param={periode:periode};
        let donnees=param,userData;

        this.storage.get('token').then(token=>{
            this.user.reportPeriod(param,token)
                .then(data=>{

                this.loader.dismiss();
                if(data&&data==="updated"){

                    this.storage.get('user').then(user=>{
                        userData=user;
                        userData.rapport=donnees.periode;
                        this.storage.set('user',userData);
                        this.initForm();
                    });

                    this.translate.get(["MESSAGE_PERIODE_SUCCEED"]).subscribe(value=>{
                        this.user.toastAlert(value.MESSAGE_PERIODE_SUCCEED);
                    });
                }
            }).catch(err=>{
                this.loader.dismiss();
                this.initForm();
                if(err&&err==="failure"){
                    this.translate.get(["MESSAGE_PERIODE_FAILURE"]).subscribe(value=>{
                        this.user.toastAlert(value.MESSAGE_PERIODE_FAILURE);
                    });
                }
            });
        });

    }

}
