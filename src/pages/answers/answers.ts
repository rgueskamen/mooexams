import { Component } from '@angular/core';
import {ModalController, NavController, NavParams} from 'ionic-angular';
import { MatiereProvider } from "../../providers/matiere/matiere";
import { TranslateService } from "@ngx-translate/core";
import { Storage } from "@ionic/storage";
import {ModalBadgePage} from "../modal-badge/modal-badge";
import {UserProvider} from "../../providers/user/user";
import {ProfilPage} from "../profil/profil";

/**
 * Generated class for the AnswersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-answers',
  templateUrl: 'answers.html',
})
export class AnswersPage {


    picture_logo:string;
    choix:string;
    resultats:any;
    questions:any;
    hideShowAnswers:boolean[];
    url:string;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public subjectService:MatiereProvider,
        public translate:TranslateService,
        public storage:Storage,
        public modalCtrl:ModalController,
        public userService:UserProvider
    ) {

        this.picture_logo="assets/img/MooExams_pre.png";
        this.choix="recapitulatif";
        this.resultats=this.navParams.get('resultats');
        this.hideShowAnswers=[];
        this.questions=this.resultats.reponses;
        this.initQuestions();
        this.url="http://mooexams.sdkgames.com/";
    }

    ionViewDidLoad() {
        this.getUserSubjectsLevel();
    }

    initQuestions(){

        this.hideShowAnswers[0]=true;
        for(let i=1;i<this.questions.length;i++){
            this.hideShowAnswers[i]=false;
        }
    }

    GotoRecap(choix:string){
        this.choix=choix;
    }

    GotoResult(choix:string){
        this.choix=choix;
    }

    //Go to the profil Page
    gotoProfile(){
        this.navCtrl.setRoot(ProfilPage);
    }


    hideShowAnswer(index:number){
        this.hideShowAnswers[index]=!this.hideShowAnswers[index];
    }


    //Format the data
    format(chaine:string){

        let ch=String(chaine);
        let resultat=ch.replace(/\\'/g,"'");
        resultat=resultat.replace(/\\"/g,'"');
        return resultat;
    }


    //get user level in subject
    getUserSubjectsLevel(){

        this.storage.get('token').then(token=>{

            this.storage.get('user').then(user=>{

                let param={"id_user":user.id,"id_matiere":this.resultats.param.id_matiere,"annee":this.resultats.param.annee};
                this.subjectService.getUsersCourseLevel(param,token)
                    .then(data=>{

                        if(data&&data.reponse=="success"){

                            if(data&&data.reponse=="success"&&data.niveaux){

                                if(data.niveaux.id_niveau!=this.resultats.param.id_niveau){
                                    this.openBadgeMessage(user,data.niveaux);
                                    this.updateInfos();
                                }
                            }

                        }

                    }).catch(err=>{

                    });
            });

        });

    }

    //Show the level of the user
    openBadgeMessage(user,niveau){

        let modal = this.modalCtrl.create(ModalBadgePage,{resultats:{user:user,level:niveau}});

        modal.onDidDismiss(data => {

        });
        modal.present();
    }


    //Update the user infos
    updateInfos(){

        this.storage.get('token').then(token=> {
            if(token)
                this.userService.userInfos(token);
        });
    }


}
