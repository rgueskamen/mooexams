import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content, AlertController, LoadingController } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { MatiereProvider } from "../../providers/matiere/matiere";
import { TranslateService } from "@ngx-translate/core";
import { ChallengeProvider } from "../../providers/challenge/challenge";
import { UserProvider } from "../../providers/user/user";
import { ChallengeResultPage } from "../challenge-result/challenge-result";
import { ChallengeParticipantsPage } from "../challenge-participants/challenge-participants";
import { ChallengePage } from "../challenge/challenge";

/**
 * Generated class for the ChallengeAttendedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-challenge-attended',
  templateUrl: 'challenge-attended.html',
})
export class ChallengeAttendedPage {

    @ViewChild(Content) content:Content;

    showFilter:boolean;
    testRadioResult:string;
    listMatiere:any;
    searching:boolean;
    pattern:string;
    backgroundColors:string[];
    showDetailChallenge:boolean[];
    challenges:any;
    schools:any;
    users:any;
    participants:any;
    matieres:any;
    classe:any;
    days:any;
    months:any;
    userInfos:any;
    criteres_classe:any;
    serveurReponse:boolean;
    url:string;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public storage:Storage,
        public matiere:MatiereProvider,
        public translate:TranslateService,
        public alertCtrl:AlertController,
        public challenge:ChallengeProvider,
        public userService:UserProvider,
        public loading:LoadingController
    ) {

        this.showFilter=false;
        this.testRadioResult="";
        this.listMatiere=[];
        this.searching=false;
        this.pattern="";
        //this.backgroundColors=["#e0d2c3","#d7efbf","#ffe2c4","#aaf9ff","#c2c6c6"];
        this.backgroundColors=["#e0d2c3","rgba(128, 128, 128,0.2)","rgba(60, 180, 75,0.2)","rgba(255, 215, 180,0.2)","rgba(70, 240, 240,0.2)"];
        this.showDetailChallenge=[];
        this.challenges=[];
        this.users=[];
        this.schools=[];
        this.userInfos={};
        this.criteres_classe=[];
        this.matieres=[];
        this.classe=[];
        this.participants=[];
        this.serveurReponse=false;
        this.getDays();
        this.getMonths();
        this.url="http://mooexams.sdkgames.com/";
    }

    ionViewDidLoad() {

        this.getChallenges(0,false);
    }

    //Get the months format
    getMonths(){

        this.months=[];
        this.translate.get(['MONTH1','MONTH2','MONTH3','MONTH4','MONTH5','MONTH6','MONTH7','MONTH8','MONTH9','MONTH10','MONTH11','MONTH12'])
            .subscribe(months=>{
                this.months.push(months.MONTH1);
                this.months.push(months.MONTH2);
                this.months.push(months.MONTH3);
                this.months.push(months.MONTH4);
                this.months.push(months.MONTH5);
                this.months.push(months.MONTH6);
                this.months.push(months.MONTH7);
                this.months.push(months.MONTH8);
                this.months.push(months.MONTH9);
                this.months.push(months.MONTH10);
                this.months.push(months.MONTH11);
                this.months.push(months.MONTH12);
            });
    }

    //Get the days format
    getDays(){
        this.days=[];
        this.translate.get(['SHORTDAY1','SHORTDAY2','SHORTDAY3','SHORTDAY4','SHORTDAY5','SHORTDAY6','SHORTDAY7'])
            .subscribe(days=>{
                this.days.push(days.SHORTDAY7);
                this.days.push(days.SHORTDAY1);
                this.days.push(days.SHORTDAY2);
                this.days.push(days.SHORTDAY3);
                this.days.push(days.SHORTDAY4);
                this.days.push(days.SHORTDAY5);
                this.days.push(days.SHORTDAY6);
               
            });

    }



    // Go to the user profile
    gotoChallenge(){
        this.navCtrl.push(ChallengePage);
    }


    //Show and hide the detail of the Challenge
    showDetail(index){
        this.showDetailChallenge[index]=!this.showDetailChallenge[index];
    }


    //Get the list of challenges
    getChallengesFromServer(){

        this.storage.get('token').then(token=>{

            let param={"token":token};

            this.storage.get('user').then(user=>{

                param["user_id"]=user.id;

                this.challenge.getChallengeParticpated(param,param.token)
                    .then(data=>{

                        this.serveurReponse=true;
                        if(data&&data.reponse=="success"){
                            if(data.challenge) {
                                this.challenges = data.challenge;
                                for (let j=0;j<this.challenges.length;j++){
                                    this.showDetailChallenge.push(false);
                                }
                            }
                            if(data.users)
                                this.users=data.users;
                            if(data.classes_criteres)
                                this.criteres_classe=data.classes_criteres;
                            if(data.matieres)
                                this.matieres=data.matieres;
                            if(data.classes)
                                this.classe=data.classes;
                            if(data.participants)
                                this.participants=data.participants;
                            if(data.userClasse)
                                this.schools=data.userClasse;
                        }

                    }).catch(err=>{

                            this.serveurReponse=true;
                            this.getChallengesFromLocal();
                    });
            });

        });
    }

    //Get the list form local
    getChallengesFromLocal() {

        this.storage.get('challenges-participated').then(data=>{
            if(data){
                if(data.challenge) {
                    this.challenges = data.challenge;
                    for (let j=0;j<this.challenges.length;j++){
                        this.showDetailChallenge.push(false);
                    }
                }
                if(data.users)
                    this.users=data.users;
                if(data.classes_criteres)
                    this.criteres_classe=data.classes_criteres;
                if(data.matieres)
                    this.matieres=data.matieres;
                if(data.classes)
                    this.classe=data.classes;
                if(data.participants)
                    this.participants=data.participants;
            }
            this.serveurReponse=false;
        });
    }


    //Get the list of challenges
    getChallenges(refresher,refresh){

        if(refresh){
            this.getChallengesFromServer();
        }else{
            this.getChallengesFromLocal();
            if(!this.challenges||this.challenges.length==0){
                this.getChallengesFromServer();
            }
        }

        if(refresher!=0)
            refresher.complete();

    }

    //Filter the challenge with a pattern
    filterItems(searchTerm){
        return this.challenges.filter((item) => {
            return item.titre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });
    }


    formatDate(date:Date) : string{

        if(date){

            let dateFormat=new Date(date);
            return this.days[dateFormat.getDay()]+". "+dateFormat.getDate()+" "+this.months[dateFormat.getMonth()]+". "+dateFormat.getFullYear();
        }else{
            return "";
        }
    }



    //show the result of a challenge
    gotoResult(challenge,participants,users,matieres,classe,school){
        this.navCtrl.push(ChallengeResultPage,
            {
                participants:participants,
                challenge:challenge,
                users:users,
                matieres:matieres,
                classe:classe,
                school:school
            });
    }


    //show the list of the challenges of a challenge
    participantsChallenge(challenge,participants,users,matieres,classe,school){
        this.navCtrl.push(ChallengeParticipantsPage,
            {
                participants:participants,
                challenge:challenge,
                users:users,
                matieres:matieres,
                classe:classe,
                school:school
            });
    }

}
