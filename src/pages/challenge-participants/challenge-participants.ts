import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProfilPage } from "../profil/profil";
import { TranslateService } from "@ngx-translate/core";
import { Storage } from "@ionic/storage";
import { ChallengeProvider } from "../../providers/challenge/challenge";
import { User } from "../../models/user";
import { UserClasse } from '../../models/userClasse';

/**
 * Generated class for the ChallengeParticipantsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
    selector: 'page-challenge-participants',
    templateUrl: 'challenge-participants.html',
})
export class ChallengeParticipantsPage {

    months:any;
    days:any;
    currentDate:Date;
    challenge:any;
    participants:any;
    matiere:any;
    users:any;
    classes:any;
    schools:any;
    serveurReponse:boolean;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public translate:TranslateService,
        public storage:Storage,
        public challengeService:ChallengeProvider
    ) {

        this.getDays();
        this.getMonths();
        this.currentDate=new Date();
        this.challenge={};
        this.participants=[];
        this.matiere={};
        this.users=[];
        this.classes=[];
        this.schools=[];
        this.serveurReponse=false;

    }

    //Get the months format
    getMonths(){

        this.months=[];
        this.translate.get(['MONTH1','MONTH2','MONTH3','MONTH4','MONTH5','MONTH6','MONTH7','MONTH8','MONTH9','MONTH10','MONTH11','MONTH12'])
            .subscribe(months=>{
                this.months.push(months.MONTH1);
                this.months.push(months.MONTH2);
                this.months.push(months.MONTH3);
                this.months.push(months.MONTH4);
                this.months.push(months.MONTH5);
                this.months.push(months.MONTH6);
                this.months.push(months.MONTH7);
                this.months.push(months.MONTH8);
                this.months.push(months.MONTH9);
                this.months.push(months.MONTH10);
                this.months.push(months.MONTH11);
                this.months.push(months.MONTH12);
            });
    }

    //Get the days format
    getDays(){
        this.days=[];
        this.translate.get(['SHORTDAY1','SHORTDAY2','SHORTDAY3','SHORTDAY4','SHORTDAY5','SHORTDAY6','SHORTDAY7'])
            .subscribe(days=>{
                this.days.push(days.SHORTDAY7);
                this.days.push(days.SHORTDAY1);
                this.days.push(days.SHORTDAY2);
                this.days.push(days.SHORTDAY3);
                this.days.push(days.SHORTDAY4);
                this.days.push(days.SHORTDAY5);
                this.days.push(days.SHORTDAY6);
              
            });
    }

    ionViewDidLoad() {
        this.getResult(0,false);
    }

    //Go to the profile page
    goProfile(){
        this.navCtrl.push(ProfilPage);
    }


    //FORMAT THE DATE
    formatDateBar(date:Date):string{

        if(date) {
            let dateFormat = new Date(date);
            return " " + this.days[dateFormat.getDay()] + " " + (dateFormat.getDate() <= 9 ? "0" + dateFormat.getDate() : dateFormat.getDate()) + " | " + ((dateFormat.getMonth() + 1) <= 9 ? "0" + (dateFormat.getMonth() + 1) : (dateFormat.getMonth() + 1)) + " | " + dateFormat.getFullYear();
        }else{
            return "";
        }
    }

    //Format de date with point
    formatDatePoint(date:Date) : string{
        if(date) {
            let dateFormat=new Date(date);
            return this.days[dateFormat.getDay()] + ". " + dateFormat.getDate() + " " + this.months[dateFormat.getMonth()] + ". " + dateFormat.getFullYear();
        }else{
            return "";
        }
    }

    //Get the results of a challenge
    getResultFromServer(){

        this.storage.get('token').then(token=>{

            let param={"id_challenge" : this.challenge.id };

            this.challengeService.challengeResults(param,token)
                .then(data=>{
                    this.serveurReponse=true;
                    if(data&&data.reponse=="success"){
                        if(data.resultat)
                            this.participants=data.resultat;
                        if(data.users)
                            this.users=data.users;
                        if(data.classes)
                            this.classes=data.classes;
                        if(data.userClasse)
                            this.schools=data.userClasse;
                    }

                }).catch(err=>{
                    this.serveurReponse=true;
                    this.getResultFromLocal();
                });

        });
    }

    //Get the results from local
    getResultFromLocal(){

        this.challenge=this.navParams.get('challenge');
        this.participants=this.navParams.get('participants')||[];
        this.users=this.navParams.get('users');
        this.matiere=this.navParams.get('matieres');
        this.classes=this.navParams.get('classe');
        this.schools=this.navParams.get('school');
        this.serveurReponse=true;
    }

    //Get all result
    getResult(refresher,refresh){

        if(refresh){
            this.getResultFromServer();
        }else{
            this.getResultFromLocal();
            if(!this.participants||this.participants&&this.participants.length==0){
                this.getResultFromServer();
            }
        }

        if(refresher!=0)
            refresher.complete();
    }


    //Get infos of a user
    getInfos(users,id){

        let user:User=new User();

        if(users&&users.length>0){

            Object.keys(users).map(function(key) {
                if(users[key].id===id){
                    user=users[key];
                }
            });
        }else{
            if(users.id===id){
                user=users;
            }
        }
        return user;
    }

    //Get the school of a user
    getSchool(schools,id){

        let school:UserClasse = new UserClasse();

        if(schools&&schools.length>0){

            Object.keys(schools).map(function(key) {
                if(schools[key].id_user===id){
                    school=schools[key];
                }
            });
        }else{
            if(schools.id_user===id) {
                school = schools;
            }
        }
        return school;
    }
}
