import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { UserProvider } from "../../providers/user/user";
import { InviteUserPage } from "../invite-user/invite-user";
import { TranslateService } from "@ngx-translate/core";
import { ChallengeProvider } from "../../providers/challenge/challenge";
import { MyChallengePage } from "../my-challenge/my-challenge";
import {ApiProvider} from "../../providers/api/api";

/**
 * Generated class for the ChallengeUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
    selector: 'page-challenge-user',
    templateUrl: 'challenge-user.html',
})
export class ChallengeUserPage {

    challenge:any;
    participants:any;
    userData:any;
    userClasse:any;
    userSchool:any;
    userFormData:any;
    serveurReponse:boolean;
    loader:any;
    backgroundColors:string[];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public storage:Storage,
        public user:UserProvider,
        public loading:LoadingController,
        public translate:TranslateService,
        public challengeService:ChallengeProvider,
        public api:ApiProvider
    ) {

        this.challenge=this.navParams.get('challenge');
        this.participants=this.navParams.get('users');
        this.userFormData=[];
        this.userData=[];
        this.userClasse={};
        this.userSchool={};
        this.serveurReponse=false;
        this.backgroundColors=["rgba(128, 128, 128,0.2)","rgba(60, 180, 75,0.2)","rgba(255, 215, 180,0.2)","rgba(70, 240, 240,0.2)"];
    }

    ionViewDidLoad() {
        this.getUserChild(0,false);
    }


    initUserForm(data){
        if(data&&data.length>0){
            for(let i=0;i<data.length;i++){
                this.userFormData.push({choix:false,id:data[i].id});
            }
        }
    }


    //Get user from server
    getStudents(){
        this.storage.get('token').then(token=>{
            this.user.getAllFriends(token)
                .then(data=>{

                    this.serveurReponse=true;
                    if(data&&data.reponse=="success"){
                        this.userData=this.api.userParticpantsFilter(data.friends,this.participants);
                        this.initUserForm(this.userData);
                    }
                }).catch(err=>{
                    this.serveurReponse=true;
                    if(err&&err.reponse=="failure"){
                        this.getUserLocal();
                    }
                });
        });
    }


    //Get frined from local
    getUserLocal(){
        this.storage.get('user-friends').then(user=>{

            if(user){
                this.userData=this.api.userParticpantsFilter(user,this.participants);
                this.initUserForm(this.userData);
            }else{
                this.userData=[];
            }
            this.serveurReponse=true;
        });
    }

    //Get the list of friends
    getUserChild(refresher,refresh){

        if(refresh){
            this.getStudents();
        }else{
            this.getUserLocal();
            if(!this.userData||this.userData.length===0){
                this.getStudents();
            }
        }

        if(refresher!=0)
            refresher.complete();
    }


    //invite a user
    addUser(){
            this.storage.get('user-friends').then(data=>{
                if(data){
                    this.navCtrl.push(InviteUserPage,{users:data,matiere:null});
                }else{
                    this.navCtrl.push(InviteUserPage,{users:[],matiere:null});
                }
            });
    }


    //Verify is a user is checked
    userSelected(users:any):boolean{

        let isSelected=false,i=0;

        if(users&&users.length>0){

            while(i<users.length&&!isSelected){
                    if(users[i].choix){
                        isSelected=true;
                    }
                 i++;
            }
        }

        return isSelected;
    }

    //Invite user to the challenge

    inviteUser(){
        let users=[];
        for(let j=0;j<this.userFormData.length;j++){
            if(this.userFormData[j].choix){
                users.push(this.userFormData[j].id)
            }
        }

        let param = {"id_challenge":this.challenge.id,"user_id":users};

        this.storage.get('token').then(token=>{

            this.loader=this.loading.create({
                content:'',
                dismissOnPageChange:true,
                duration: 60000
            });

            this.loader.present();

            this.challengeService.inviteFriends(param,token)
                .then(reponse=>{

                    this.loader.dismiss();
                    if(reponse&&reponse.reponse=="success"){

                      this.initUserForm(this.userData);

                        this.translate.get(['MESSAGE_INVITATION_SEND']).subscribe(value=>{
                            this.user.toastAlert(value.MESSAGE_INVITATION_SEND);
                        });

                        this.navCtrl.setRoot(MyChallengePage);
                     }

                }).catch(err=>{

                    this.loader.dismiss();

                    if(err&&err.reponse=="failure"){

                        this.translate.get(['CHALLENGES_ERROR_INVITATION']).subscribe(value=>{
                            this.user.toastAlert(value.CHALLENGES_ERROR_INVITATION);
                        });
                    }
                });
        });
    }

}
