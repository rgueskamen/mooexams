import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content, AlertController, LoadingController} from 'ionic-angular';
import { FormControl } from "@angular/forms";
import { ProfilPage  } from "../profil/profil";
import { Storage } from "@ionic/storage";
import { MatiereProvider } from "../../providers/matiere/matiere";
import { TranslateService } from "@ngx-translate/core";
import { ChallengeProvider } from "../../providers/challenge/challenge";
import { UserProvider} from "../../providers/user/user";
import { ChallengeResultPage } from "../challenge-result/challenge-result";
import { QuestionChallengePage } from "../question-challenge/question-challenge";
import { ChallengeParticipantsPage } from "../challenge-participants/challenge-participants";
import { CreateChallengePage } from "../create-challenge/create-challenge";


/**
 * Generated class for the ChallengePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
    selector: 'page-challenge',
    templateUrl: 'challenge.html',
})
export class ChallengePage {

    @ViewChild(Content) content:Content;

    showFilter:boolean;
    testRadioResult:string;
    listMatiere:any;
    searching:boolean;
    pattern:string;
    searchControl: FormControl;
    backgroundColors:string[];
    showDetailChallenge:boolean[];
    challenges:any;
    schools:any;
    users:any;
    participants:any;
    matieres:any;
    classe:any;
    criteres_classe:any;
    days:any;
    months:any;
    userInfos:any;
    serveurReponse:boolean;
    loader:any;
    url:string;


    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public storage:Storage,
        public matiere:MatiereProvider,
        public translate:TranslateService,
        public alertCtrl:AlertController,
        public challenge:ChallengeProvider,
        public userService:UserProvider,
        public loading:LoadingController
    ) {

        this.showFilter=false;
        this.testRadioResult="";
        this.listMatiere=[];
        this.searching=false;
        this.searchControl = new FormControl();
        this.pattern="";
       // this.backgroundColors=["#d7efbf","#ffe2c4","#aaf9ff","#c2c6c6"];
        this.backgroundColors=["#e0d2c3","rgba(128, 128, 128,0.2)","rgba(60, 180, 75,0.2)","rgba(255, 215, 180,0.2)","rgba(70, 240, 240,0.2)"];
        this.showDetailChallenge=[];
        this.challenges=[];
        this.users=[];
        this.schools=[];
        this.userInfos={};
        this.matieres=[];
        this.classe=[];
        this.criteres_classe=[];
        this.participants=[];
        this.serveurReponse=false;
        this.url="http://mooexams.sdkgames.com/";
        this.getDays();
        this.getMonths();
    }

    ionViewDidLoad() {

        //Wait until the search data are ready
        this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
            this.searching = false;
            //Get the list of challenges filtered
            this.getItems();
        });

        this.getChallenges(0,true);
    }

    //Get the months format
    getMonths(){

        this.months=[];
        this.translate.get(['MONTH1','MONTH2','MONTH3','MONTH4','MONTH5','MONTH6','MONTH7','MONTH8','MONTH9','MONTH10','MONTH11','MONTH12'])
            .subscribe(months=>{
                this.months.push(months.MONTH1);
                this.months.push(months.MONTH2);
                this.months.push(months.MONTH3);
                this.months.push(months.MONTH4);
                this.months.push(months.MONTH5);
                this.months.push(months.MONTH6);
                this.months.push(months.MONTH7);
                this.months.push(months.MONTH8);
                this.months.push(months.MONTH9);
                this.months.push(months.MONTH10);
                this.months.push(months.MONTH11);
                this.months.push(months.MONTH12);
            });
    }

    //Get the days format
    getDays(){
        this.days=[];
        this.translate.get(['SHORTDAY1','SHORTDAY2','SHORTDAY3','SHORTDAY4','SHORTDAY5','SHORTDAY6','SHORTDAY7'])
            .subscribe(days=>{
                this.days.push(days.SHORTDAY7);
                this.days.push(days.SHORTDAY1);
                this.days.push(days.SHORTDAY2);
                this.days.push(days.SHORTDAY3);
                this.days.push(days.SHORTDAY4);
                this.days.push(days.SHORTDAY5);
                this.days.push(days.SHORTDAY6);
                
            });

    }



    //Get the list of matiere
    getMatieres(){

        let matiere=[];

        this.storage.get('token').then(token=>{
            this.matiere.classCourses(token)
                .then(data=>{

                Object.keys(data).map(function(key) {
                    matiere.push(data[key]);
                });

                this.listMatiere=matiere;

            });
        });
    }


    //Hide filter button and show the filter bar
    setFilter(){

        this.showFilter=true;
        //Get all the matiere
        this.storage.get('classe_matieres').then(data=>{

            if(data&&data.length>0){
                this.listMatiere=data;
            }else{
                this.getMatieres();
            }
        });

        this.content.resize();
    }

    //Hide the filter
    hideFilter(){
        this.showFilter=false;

        this.translate.get(['FORUM_FILTER_MESSAGE']).subscribe(value=>{
            this.testRadioResult=value.FORUM_FILTER_MESSAGE;
        });

        //Get the list of challenges from local
        this.content.resize();
    }


    //show the loading
    onSearchInput(){
        this.searching = true;
    }

    //Format the list of courses of the forum
    radioList(){

        let alert = this.alertCtrl.create();

        this.translate.get(['FORUM_SELECT_TEXT']).subscribe(value=>{
            alert.setTitle(value.FORUM_SELECT_TEXT);
        });

        for (let i = 0; i < this.listMatiere.length; i++) {

            if(this.listMatiere[i]){
                alert.addInput({
                    type: 'radio',
                    label: this.listMatiere[i].NOM_MATIERE.toUpperCase(),
                    value: this.listMatiere[i],
                    checked: false
                });
            }
        }

        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: data => {

                if(data){
                    this.testRadioResult=data.NOM_MATIERE;
                    //Get challenge by course
                    this.challengeByCourse(data.id);
                    this.showFilter=false;
                    this.content.resize();
                }
            }
        });

        alert.present();
    }


    // Go to the user profile
    goProfile(){
        this.navCtrl.push(ProfilPage);
    }

    //Add a Challenge
    addChallenge(){

        this.storage.get('user').then(user=>{

            let param={"user":user};
            this.storage.get('user-classes').then(classe=>{
                this.navCtrl.push(CreateChallengePage,{user:param.user,classe:classe});
            });

        });
    }

    //Show and hide the detail of the Challenge
    showDetail(index){
        this.showDetailChallenge[index]=!this.showDetailChallenge[index];
    }


    //Get the list of challenges
    getChallengesFromServer(){

        this.storage.get('token').then(token=>{

            let param={"token":token};

            this.storage.get('user').then(user=>{

                param["user_id"]=user.id;

                this.challenge.getChallengeAccepted(param,param.token)
                    .then(data=>{

                        this.serveurReponse=true;
                        if(data&&data.reponse=="success"){
                            if(data.challenge) {
                                this.challenges = data.challenge;
                                for (let j=0;j<this.challenges.length;j++){
                                    this.showDetailChallenge.push(false);
                                }
                            }
                            if(data.users)
                                this.users=data.users;
                            if(data.classes_criteres)
                                this.criteres_classe=data.classes_criteres;
                            if(data.matieres)
                                this.matieres=data.matieres;
                            if(data.classes)
                                this.classe=data.classes;
                            if(data.participants)
                                this.participants=data.participants;
                            if(data.userClasse)
                                this.schools=data.userClasse;
                        }

                    }).catch(err=>{
                            this.serveurReponse=true;
                            this.getChallengesFromLocal();
                    });
            });

        });
    }

    //Get the list form local
    getChallengesFromLocal() {

        this.storage.get('challenges-accepted').then(data=>{
            if(data){
                if(data.challenge) {
                    this.challenges = data.challenge;
                    for (let j=0;j<this.challenges.length;j++){
                        this.showDetailChallenge.push(false);
                    }
                }
                if(data.users)
                    this.users=data.users;
                if(data.classes_criteres)
                    this.criteres_classe=data.classes_criteres;
                if(data.matieres)
                    this.matieres=data.matieres;
                if(data.classes)
                    this.classe=data.classes;
                if(data.participants)
                    this.participants=data.participants;
            }
            this.serveurReponse=true;
        });
    }


    //Get the list of challenges
    getChallenges(refresher,refresh){

        if(refresh){
            this.getChallengesFromServer();
        }else{
            this.getChallengesFromLocal();
            if(!this.challenges||this.challenges.length==0){
                this.getChallengesFromServer();
            }
        }

        if(refresher!=0)
            refresher.complete();

    }

    //Filter the challenge with a pattern
    filterItems(searchTerm){
        return this.challenges.filter((item) => {
            return item.titre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });
    }


    //Get the list which match the pattern
    getItems(){
        this.challenges=this.filterItems(this.pattern);
        this.content.resize();
    }


    //Get the Challenge list of a course
    challengeByCourse(matiereId:number){

        let dataPost=[];

        if(this.challenges&&this.challenges.length>0){

            for(let i=0;i<this.challenges.length;i++) {

                if(this.challenges[i].id_matiere===matiereId){
                    dataPost.push(this.challenges[i]);
                }
            }

            this.challenges=dataPost;
            this.content.resize();
        }

    }


    formatDate(date:Date) : string{

        if(date){

            let dateFormat=new Date(date);
            return this.days[dateFormat.getDay()]+". "+dateFormat.getDate()+" "+this.months[dateFormat.getMonth()]+". "+dateFormat.getFullYear();
        }else{
            return "";
        }
    }

    //Decline the invitation to a challenge
    declineInvitation(index){

        this.storage.get('user').then(user=>{

            this.loader = this.loading.create({
                content:'',
                dismissOnPageChange:true,
                duration: 60000
            });

            let data={"user_id":user.id,"challenge_id":this.challenges[index].id};
            this.loader.present();

            this.storage.get('token').then(token=>{

                this.challenge.refusedInvitations(data,token)
                    .then(data=>{

                        this.loader.dismiss();
                        if(data&&data.reponse=="success"){
                            this.challenges.splice(1,index) ;
                            this.content.resize();
                            this.getChallengesFromServer();
                        }

                    }).catch(err=>{

                        this.loader.dismiss();
                        if(err&&err.reponse=="failure"){

                            this.translate.get(['CHALLENGES_ERROR_DECILNE']).subscribe(value=>{
                                this.userService.toastAlert(value.CHALLENGES_ERROR_DECILNE);
                            });
                        }
                    });
            });

        });

    }




    //show the result of a challenge
    ResultsOfchallenge(challenge,participants,users,matieres,classe,school){
        this.navCtrl.push(ChallengeResultPage,
            {
                participants:participants,
                challenge:challenge,
                users:users,
                matieres:matieres,
                classe:classe,
                school:school
            });
    }



    //show the list of the challenges of a challenge
    participantsChallenge(challenge,participants,users,matieres,classe,school){
        this.navCtrl.push(ChallengeParticipantsPage,
            {
                participants:participants,
                challenge:challenge,
                users:users,
                matieres:matieres,
                classe:classe,
                school:school
            });
    }

    //Participate to a challenge
    participedToChallenge(index){

        this.storage.get('user').then(user=>{

            this.loader = this.loading.create({
                content:'',
                dismissOnPageChange:true,
                duration: 60000
            });

            let data={"user_id":user.id,"challenge_id":this.challenges[index].id};
            this.loader.present();

            this.storage.get('token').then(token=>{


                this.challenge.acceptedInvitations(data,token)
                    .then(reponse=>{

                        this.loader.dismiss();
                        if(reponse&&reponse.reponse=="success"){
                            this.challenges[index].state=1;
                            this.getChallengesFromServer();
                        }

                    }).catch(err=>{

                        this.loader.dismiss();
                        if(err&&err.reponse=="failure"){

                            this.translate.get(['CHALLENGES_ERROR_DECILNE']).subscribe(value=>{
                                this.userService.toastAlert(value.CHALLENGES_ERROR_DECILNE);
                            });
                        }
                    });
            });

        });
    }




    //answer the challenges Questions
    answerChallenge(data){
        this.storage.get('user').then(user=>{
            let param={"id_challenge":data.id_challenge,"id_user":user.id};
            this.navCtrl.setRoot(QuestionChallengePage,{params:param});
        });
    }
}
