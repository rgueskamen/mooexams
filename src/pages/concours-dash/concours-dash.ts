import { UserProvider } from './../../providers/user/user';
import { Storage } from '@ionic/storage';
import { ConcoursProvider } from './../../providers/concours/concours';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ProfilPage } from './../profil/profil';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content, LoadingController } from 'ionic-angular';
import { ConcoursQuestionsPage } from '../concours-questions/concours-questions';

/**
 * Generated class for the ConcoursDashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 interface Concours{
  active:number;
  baniere_sponsors:string;
  created_at:Date;
  date_debut:string;
  date_fin:string;
  description:string;
  id:number;
  image_concours:string;
  last_concours:string;
  nb_questions_jour:number;
  title:string;
  updated_at:Date;
 }


/*interface Sponsors{
  banniere_pub:string;
  categorie:string;
  concours_id:number;
  created_at:Date;
  debut_visibilite:string;
  fin_visibilite:string;
  id:number;
  lien:string;
  logo:string;
  nom:string;
  updated_at:Date;
}*/


@Component({
  selector: 'page-concours-dash',
  templateUrl: 'concours-dash.html',
})
export class ConcoursDashPage {

  @ViewChild(Content) content : Content;
 
  choix : string;
  months:Array<string>;
  days:Array<string>;
  members:any[];
  showAddMemberContain:boolean;
  url:string;
  concours:Concours;
  sponsors:Array<string[]>;
  nbParticipants:number;
  urlImage:string;
  searching:boolean;
  team:any;
  hasteamVerify:boolean;
  showCreateContain:boolean;
  typeButtonShow:number;
  currentMember:any;
  showUpdateContain:boolean;
  user:any;
  classe:any;
  loader:any;


  constructor(
    public navCtrl: NavController, 
    public translate : TranslateService,
    public storage : Storage,
    public concour : ConcoursProvider,
    public userService : UserProvider,
    public loading : LoadingController,
    public fb : FormBuilder,
    public navParams: NavParams
  ) {
    this.choix='concours';
    this.getMonths();
    this.getDays();

    this.members=[];
    this.urlImage="http://mooexams.sdkgames.com/";
    this.concours ={
      active:0,
      baniere_sponsors:"",
      created_at:new Date(),
      date_debut:"",
      date_fin:"",
      description:"",
      id:0,
      image_concours:"",
      last_concours:null,
      nb_questions_jour:5,
      title:"",
      updated_at:new Date()
    };
    this.sponsors=[];
    this.nbParticipants=0;
    this.showAddMemberContain=false;
    this.url="assets/img/paillettes.png";
    this.searching=false;
    this.team={};
    this.hasteamVerify=false;
    this.showCreateContain=false;
    this.typeButtonShow=1;
    this.currentMember={};
    this.showUpdateContain=false;
    this.storage.get('user').then(user=>{
          this.user=user;
    });

    this.storage.get('user-classes').then(classe=>{
      this.classe=classe;
    });
  }

  ionViewDidLoad() {
   this.content.resize();
   this.members=[];
   this.getconcours();
 
  }

   
  //Members form
  public  memberForm = this.fb.group({
    numero: ["",Validators.compose([Validators.required,Validators.pattern('^[0-9]{9,13}')])],
    nom: ["", Validators.required]
 });

   //Friends form
   public  friendForm = this.fb.group({
    numero: ["",Validators.compose([Validators.required,Validators.pattern('^[0-9]{9,13}')])],
    nom: ["", Validators.required]
 });

   //Return form instance control fields for login
   get username() { return this.memberForm.get('nom'); }
   get phone() { return this.memberForm.get('numero'); }

   //Init the Form member control
   initMember() {
    this.memberForm = this.fb.group({
      numero: ["", Validators.compose([Validators.required,Validators.pattern('^[0-9]{9,13}')])],
      nom: ["", Validators.required]
    });
  }



   //Init the Form friends control
   initFriends() {
    this.friendForm = this.fb.group({
      numero: ["", Validators.compose([Validators.required,Validators.pattern('^[0-9]{9,13}')])],
      nom: ["", Validators.required]
    });
  }

  //init form member with data
  initMemberData(data:any) {
    //Construction du controleur du formulaire de login
    this.memberForm = this.fb.group({
      numero: [data.numero, Validators.compose([Validators.required,Validators.pattern('^[0-9]{9,13}')])],
      nom: [data.nom, Validators.required]
    });
  }

  //team Form 
 public  teamForm = this.fb.group({
   nom_equipe: ["", Validators.required]
 });

 //Get the team form control
 get teamName() { return this.teamForm.get('nom_equipe'); }

 //Init the team form
 initTeam(){
  this.teamForm = this.fb.group({
    nom_equipe: ["", Validators.required]
  });
 }


  //Go to the concours page
  goProfile(){
    this.navCtrl.push(ProfilPage);
  }


  //Select the tab to show
  updateChoix(choix:string){
    this.choix=choix;
    this.content.resize();

    if(choix=='equipe'){
      this.showAddMemberContain=false;
      this.showUpdateContain=false;
      this.showCreateContain=false;
      this.getTeam();
      this.initMember();
      this.initTeam();
    }
  }

  //Answers the questions of concours.
  takeConcours(){
    this.storage.get('user').then(user=>{
      this.navCtrl.push(ConcoursQuestionsPage,{concoursId: this.concours.id,userId:user.id,numero:user.tel});
    });
   
  }

      //Get the months format
      getMonths(){

        this.months=[];
        this.translate.get(['MONTH1','MONTH2','MONTH3','MONTH4','MONTH5','MONTH6','MONTH7','MONTH8','MONTH9','MONTH10','MONTH11','MONTH12'])
            .subscribe(months=>{
                this.months.push(months.MONTH1);
                this.months.push(months.MONTH2);
                this.months.push(months.MONTH3);
                this.months.push(months.MONTH4);
                this.months.push(months.MONTH5);
                this.months.push(months.MONTH6);
                this.months.push(months.MONTH7);
                this.months.push(months.MONTH8);
                this.months.push(months.MONTH9);
                this.months.push(months.MONTH10);
                this.months.push(months.MONTH11);
                this.months.push(months.MONTH12);
            });
    }

    //Get the days format
    getDays(){
        this.days=[];
        this.translate.get(['SHORTDAY7','SHORTDAY1','SHORTDAY2','SHORTDAY3','SHORTDAY4','SHORTDAY5','SHORTDAY6'])
            .subscribe(days=>{
                this.days.push(days.SHORTDAY7);
                this.days.push(days.SHORTDAY1);
                this.days.push(days.SHORTDAY2);
                this.days.push(days.SHORTDAY3);
                this.days.push(days.SHORTDAY4);
                this.days.push(days.SHORTDAY5);
                this.days.push(days.SHORTDAY6);
          
            });

    }


    //Foramt the date of competition
  formatDate(dateParam:string) : string{
    let date = new Date(dateParam);
    return " "+this.days[date.getDay()]+". "+date.getDate()+" "+this.months[date.getMonth()]+". "+date.getFullYear();
  }

  //show add member Form
  addMember(){
    this.showAddMemberContain=true;
    this.initMember();
    this.content.resize();
  }


  //show add member Form
  createMyTeam(){
    this.showCreateContain=true;
    this.initTeam();
    this.content.resize();
  }

   //Hide add member Form
  cancelMember(){
    this.showAddMemberContain=false;
    this.showUpdateContain=false;
    this.initMember();
    this.content.resize();  
  }


  //Cancel the team creation Form
  cancelMyTeam(){
    this.showCreateContain=false;
    this.initTeam();
    this.content.resize(); 
  }


  //buid slide data. 
  getSlides(sponsors:any){
    let slides : Array<string[]> = [],k=0,taille=0;

   /* let sponsors=[
    {logo:"assets/img/MooExams.png"},
    {logo:"assets/img/MooExams.png"},
    {logo:"assets/img/MooExams.png"},
    {logo:"assets/img/MooExams.png"},
    {logo:"assets/img/MooExams.png"},
    {logo:"assets/img/MooExams.png"},
    {logo:"assets/img/MooExams.png"}
    ]*/;

    k=sponsors.length;
    if(k>0){
     
      taille=Math.ceil(k/3);

      for(let j=0;j<taille;j++){
        slides[j]=[];
      }

    
      for(let j=0;j<taille;j++){
        k=j*3;
       
        if(k<sponsors.length&&slides[j]){
          slides[j].push(this.urlImage+sponsors[k].logo);
        }else{
          slides[j].push('assets/img/MooExams.png');
        }


        k=j*3+1;
        if(k<sponsors.length&&slides[j]){
          slides[j].push(this.urlImage+sponsors[k].logo);
        }else{
          slides[j].push('assets/img/MooExams.png');
        }

        k=j*3+2;
        if(k <sponsors.length&&slides[j]){
          slides[j].push(this.urlImage+sponsors[k].logo);
        }else{
          slides[j].push('assets/img/MooExams.png');
        }

      }

      
    }else{

      for(let j=0;j<3;j++){
        slides[j]=[];
      }
     
      slides[0].push('assets/img/MooExams.png');
      slides[0].push('assets/img/MooExams.png');
      slides[0].push('assets/img/MooExams.png');
    }

    return slides;

  }


  //Get the list of concours

  getconcours(){

     this.storage.get('concours').then(data=>{
      if(data&&data.reponse=="success"){
        this.concours=data.concours;
        this.sponsors=this.getSlides(data.sponsors);
        this.nbParticipants=data.participants;
        this.searching=true;
      }
    });

      this.storage.get('token').then(token=>{
          this.concour.getconcours(token).then(concours=>{

            if(concours&&concours.reponse=="success"){
                this.concours=concours.concours;
                this.sponsors=this.getSlides(concours.sponsors);
                this.nbParticipants=concours.participant;
                this.content.resize(); 
               // this.getTeam();
            }else{
                this.storage.get('concours').then(data=>{
                  if(data&&data.reponse=="success"){
                    this.concours=data.concours;
                    this.sponsors=this.getSlides(data.sponsors);
                    this.nbParticipants=data.participants;
                  }
              });
            }
            this.searching=true;

          }).catch(err=>{

            this.searching=true;

            if(err&&err.reponse=="failure"){
              this.translate.get(['CONCOURS_SERVER_ERROR']).subscribe(value => {
                this.userService.toastAlert(value.CONCOURS_SERVER_ERROR);
              });
              this.concours=err.concours;
              this.sponsors=this.getSlides(err.sponsors);
              this.nbParticipants=err.participants;

            }else{
                this.storage.get('concours').then(data=>{
                  if(data&&data.reponse=="success"){
                    this.concours=data.concours;
                    this.sponsors=this.getSlides(data.sponsors);
                    this.nbParticipants=data.participants;
                  }
              });
            }
            
          });
      });
  }


  refreshData(refreseher,choix:string){
    
      switch(choix){

        case 'concours' : 
        this.getconcours();
        setTimeout(()=>{
          refreseher.complete();
        },3000);
        break;


        case 'equipe' : 
        this.getTeam();
        setTimeout(()=>{
          refreseher.complete();
        },3000);
        break;

        default : 
           break;
         
      }
  }


  //Find a user team and member for concours
  getTeam(){
        this.storage.get('token').then(token=>{
            this.storage.get('user').then(user=>{

              this.storage.get('concours').then(data=>{

                let param = {"numero":"", "concours_id":""};

                if(data&&data.reponse=="success"){
                    param = {
                       "numero" :user.tel,
                       "concours_id":data.concours.id
                    }
                }
            
                if(param.concours_id){

                this.concour.findteam(param,token).then(team=>{

                      if(team&&team.reponse=="success"){
                          this.members=team.membres;
                          this.team=team.equipe;
                          this.content.resize(); 
                          
                      }else{
                          this.storage.get('team'+this.concours.id).then(data=>{

                            if(data&&data.reponse=="success"){
                                this.members=data.membres;
                                this.team=data.equipe;
                                this.content.resize()
                            }
                        });
                      }
                      this.hasteamVerify=true;

                }).catch(err=>{

                    this.hasteamVerify=true;

                    if(err&&err.reponse=="failure"){
                      this.translate.get(['CONCOURS_SERVER_ERROR']).subscribe(value => {
                        this.userService.toastAlert(value.CONCOURS_SERVER_ERROR);
                      });
                        this.members=[];
                        this.team={};
                    }else{
                      this.storage.get('team'+this.concours.id).then(data=>{

                          if(data&&data.reponse=="success"){
                              this.members=data.membres;
                              this.team=data.equipe;
                              this.content.resize()
                          }
                      });
                    }
                });
              }else{
                this.members=[];
                this.team={};
                this.hasteamVerify=true;
              }

            });
          });
        });
  }


  //create a team 
  createTeam(data:any){
    this.storage.get('token').then(token=>{
        this.storage.get('user').then(user=>{

            let param = {
              "numero" :user.tel,
              "nom_equipe":data.nom_equipe,
              "user_id":user.id,
              "concours_id":this.concours.id,
              "date_creation":new Date(),
              "membres":[{"nom":user.username,"numero":user.tel}]
            }

            this.loader = this.loading.create({
              content: '',
              dismissOnPageChange: true,
              duration: 60000
            });

           this.loader.present();

            this.concour.createteam(param,token).then(team=>{

                  if(team&&team.reponse=="success"){
                      this.members=team.membres;
                      this.team=team.equipe;
                      this.content.resize(); 
                      this.initTeam();
                      this.showCreateContain=false;
                  }else if(team&&team.reponse=="user_not_exist"){
                      this.members=[];
                      this.team={};
                      this.translate.get(['CONCOURS_USER_NOT_PATICIPATE']).subscribe(value => {
                        this.userService.toastAlert(value.CONCOURS_USER_NOT_PATICIPATE);
                      });

                  }else if(team&&team.reponse=="nom_equipe_already_exist"){
                      this.members=[];
                      this.team={};
                      this.translate.get(['CONCOURS_TEAM_EXIST']).subscribe(value => {
                        this.userService.toastAlert(value.CONCOURS_TEAM_EXIST);
                      });

                  }else if(team&&team.reponse=="user_has_team"){
       
                      this.members=[];
                      this.team={};
                      this.translate.get(['CONCOURS_USER_HAS_TEAM']).subscribe(value => {
                        this.userService.toastAlert(value.CONCOURS_USER_HAS_TEAM);
                      });

                      this.getTeam();

                  }else{
                      this.members=[];
                      this.team={};

                      this.translate.get(['CONCOURS_CREATE_TEAM_ERROR']).subscribe(value => {
                        this.userService.toastAlert(value.CONCOURS_CREATE_TEAM_ERROR);
                      });
                  }
                  if(this.loader) this.loader.dismiss();
               
            }).catch(err=>{
              if(this.loader) this.loader.dismiss();
                  this.members=[];
                  this.team={};

                  if(err&&err.reponse=="failure"){
                  this.translate.get(['CONCOURS_CREATE_TEAM_ERROR']).subscribe(value => {
                    this.userService.toastAlert(value.CONCOURS_CREATE_TEAM_ERROR);
                  });
                }
            });

        });
    });
 }




  //invite friend 
  inviteFriend(data:any){
    this.storage.get('token').then(token=>{
      
      this.storage.get('user').then(user=>{

            let param = {
              "user_id":user.id,
              "numero":data.numero,
              "nom":data.nom
            };

            this.loader = this.loading.create({
              content: '',
              dismissOnPageChange: true,
              duration: 60000
            });

           this.loader.present();

            this.concour.inviteFriend(param,token).then(data=>{

                  if(data&&data.reponse=="success"){
                     
                      this.translate.get(['CONCOURS_INVITATION_SUCCESS']).subscribe(value => {
                        this.userService.toastAlert(value.CONCOURS_INVITATION_SUCCESS);
                      });
                    

                      this.initFriends();

                  }else if(data&&data.reponse=="number_exist"){
        
                      this.translate.get(['CONCOURS_INVITATION_NUMBER_EXIST']).subscribe(value => {
                        this.userService.toastAlert(value.CONCOURS_INVITATION_NUMBER_EXIST);
                      });
                  }
                  if(this.loader) this.loader.dismiss();
               
            }).catch(err=>{
              if(this.loader) this.loader.dismiss();
                  if(err&&err.reponse=="failure"){
                    this.translate.get(['CONCOURS_INVITATION_NUMBER_EXIST']).subscribe(value => {
                      this.userService.toastAlert(value.CONCOURS_INVITATION_NUMBER_EXIST);
                    });
                }
            });

          });

    });
 }



    //Add  a member to concours team
   AddTeamMember(members:any){
      this.storage.get('token').then(token=>{
          this.storage.get('user').then(user=>{

              let param = {
                "equipe_id":this.team.id,
                "user_id" :user.id,
                "concours_id":this.concours.id,
                "date_creation":new Date(),
                "membres":{"nom":members.nom,"numero":members.numero}
              }

              this.loader = this.loading.create({
                content: '',
                dismissOnPageChange: true,
                duration: 60000
              });
  
             this.loader.present();

              this.concour.addmemberteam(param,token).then(team=>{

                    if(team&&team.reponse=="success"){
                        this.members=team.membres;
                        this.team=team.equipe;
                        this.content.resize(); 
                        this.initMember();
                        if(this.members.length==15){
                          this.showAddMemberContain=false;
                        }
                
                    }else if(team&&team.reponse=="user_has_team"){
                        
                        this.translate.get(['CONCOURS_USER_HAS_TEAM']).subscribe(value => {
                          this.userService.toastAlert(value.CONCOURS_USER_HAS_TEAM);
                        });

                        this.getTeam();
  
                    }else if(team&&team.reponse=="user_not_exist1"){
              
                      this.translate.get(['CONCOURS_USER_NOT_EXIST']).subscribe(value => {
                        this.userService.toastAlert(value.CONCOURS_USER_NOT_EXIST);
                      });
                    }else if(team&&team.reponse=="user_dont_have_classe"){
                  
                      this.translate.get(['CONCOURS_USER_CLASS_NOT_EXIST']).subscribe(value => {
                        this.userService.toastAlert(value.CONCOURS_USER_CLASS_NOT_EXIST);
                      });
                    }
                    else if(team&&team.reponse=="user_dont_have_team_classe"){
                  
                      this.translate.get(['CONCOURS_USER_NOT_CLASS_TEAM']).subscribe(value => {
                        this.userService.toastAlert(value.CONCOURS_USER_NOT_CLASS_TEAM);
                      });
                    }
                    else if(team&&team.reponse=="team_aleardy_have_15_member"){
                 
                      this.translate.get(['CONCOURS_TEAM_SIZE_LIMIT']).subscribe(value => {
                        this.userService.toastAlert(value.CONCOURS_TEAM_SIZE_LIMIT);
                      });

                    }
                    else{
                        this.translate.get(['CONCOURS_SERVER_ERROR']).subscribe(value => {
                          this.userService.toastAlert(value.CONCOURS_SERVER_ERROR);
                        });
                    }
                    this.loader.dismiss();
              }).catch(err=>{
                this.loader.dismiss();
                 
                    if(err&&err.reponse=="failure"){
                      this.translate.get(['CONCOURS_SERVER_ERROR']).subscribe(value => {
                        this.userService.toastAlert(value.CONCOURS_SERVER_ERROR);
                      });
                    }
              });

          });
      });
    }

          //Add  a member to concours team
   DeleteTeamMember(index:number,member:any){
    this.storage.get('token').then(token=>{
        this.storage.get('user').then(user=>{

            let param = {
              "equipe_id":member.equipe_id,
              "user_id" :user.id,
              "numero":user.tel,
              "concours_id":this.team.concours_id,
              "member_id":member.id,
              "date_creation":new Date(),
              "membres":{"nom":member.nom,"numero": member.numero,"member_id":member.id}
            }

            this.loader = this.loading.create({
              content: '',
              dismissOnPageChange: true,
              duration: 60000
            });

           this.loader.present();

            this.concour.deletememberteam(param,token).then(team=>{

                  this.members.splice(index,1);
                  if(team&&team.reponse=="success"){
                      this.members=team.membres;
                      this.team=team.equipe;
                      this.content.resize(); 
                      
                  }
                 if(this.loader) this.loader.dismiss();
               
            }).catch(err=>{
              
                  if(this.loader) this.loader.dismiss();
            });

        });
    });
  }

  UpdateTeamMemberForm(members:any,member:any){
    this.typeButtonShow=2;
    this.showUpdateContain=true;
    this.initMemberData(member);
    this.currentMember=member;
    this.content.resize(); 
  }

        //Add  a member to concours team
   UpdateTeamMember(membersForm:any,member:any){
    this.storage.get('token').then(token=>{
        this.storage.get('user').then(user=>{

            let param = {
              "equipe_id":member.equipe_id,
              "user_id" :user.id,
              "numero":user.tel,
              "concours_id":this.team.concours_id,
              "member_id":member.id,
              "date_creation":new Date(),
              "membres":{"nom":membersForm.nom,"numero": membersForm.numero,"member_id":member.id}
            }

            this.loader = this.loading.create({
              content: '',
              dismissOnPageChange: true,
              duration: 60000
            });

           this.loader.present();

            this.concour.updatememberteam(param,token).then(team=>{

                  if(team&&team.reponse=="success"){
                      this.members=team.membres;
                      this.team=team.equipe;
                      this.initMember();
                      this.showUpdateContain=false;
                      this.content.resize(); 
                  }
                  if(this.loader) this.loader.dismiss();
            }).catch(err=>{
        
                  if(this.loader) this.loader.dismiss();
            });

        });
    });
 }


 
          //Cancel the team member to concours team
          CancelTeamMemberForm(index:number,member:any){
            this.storage.get('token').then(token=>{
         
                    let param = {
                      "numero":member.numero,
                      "concours_id":this.team.concours_id
                    };
        
                    this.loader = this.loading.create({
                      content: '',
                      dismissOnPageChange: true,
                      duration: 60000
                    });
        
                    this.loader.present();
        
                    this.concour.leaveTeam(param,token).then(team=>{
      
                          if(team&&team.reponse=="success"){
                              this.members.splice(index,1);
                              this.members=[];
                              this.team={};
                              this.storage.set('team'+param.concours_id,null);
                              this.getTeam();
                              this.content.resize();  
                          }else{

                            this.translate.get(['CONCOURS_FAILED_EXIT']).subscribe(value => {
                              this.userService.toastAlert(value.CONCOURS_FAILED_EXIT);
                            });
                          }
                          if(this.loader) this.loader.dismiss();
                       
                    }).catch(err=>{
                         if(this.loader) this.loader.dismiss();
                    });
      
            });
          }

}
