import { HomePage } from './../home/home';
import { Component, ElementRef, ViewChild} from '@angular/core';
import { LoadingController, ModalController,Events, ToastController, NavController, NavParams, Content } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { ApiProvider } from "../../providers/api/api";
import { Question } from '../../models/question';
import { ScoreProvider } from "../../providers/score/score";
import { TranslateService } from "@ngx-translate/core";

import { ConcoursProvider } from './../../providers/concours/concours';
import { ModalSuccessPage } from '../modal-success/modal-success';
import { ModalFailedPage } from './../modal-failed/modal-failed';
import { ModalConcoursPage } from './../modal-concours/modal-concours';

/**
 * Generated class for the ConcoursQuestionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

interface QuestionTimer{

  value:number;
  points:number;
  border:string;
  color:string;
  background:string;
  time:number;
  percent:number
}

@Component({
  selector: 'page-concours-questions',
  templateUrl: 'concours-questions.html',
})
export class ConcoursQuestionsPage {


      picture_logo: string;
      picture_revision: string;
      picture_light: string;
      options: any;
      questionnaires: Array<Question>;
      answers: number[];
      confirmAnswer: boolean[];
      slideShow: number;
      score: { failed: number, success: number, points: number };
      progressPercent: number;
      interval: any;
      labels: string[];
      url: string;
      answersTimer: Array<QuestionTimer>;
      timerPosition: number;
      serveurReponse: boolean;
      currentsPoints:number;
      loader:any;
      concoursId:number;
      userId:number;
      currentReponse:number;
      myPoints:number;
      numero:string;
      timeout:any;
      time:number;
      formatTime:string;
      intervalTime:any;

      @ViewChild(Content) content : Content;


      constructor(public navCtrl: NavController,
                  public navParams: NavParams,
                  public storage: Storage,
                  public api: ApiProvider,
                  public events: Events,
                  public  toastCtrl:ToastController,
                  public concours: ConcoursProvider,
                  public resultat: ScoreProvider,
                  public modalCtrl: ModalController,
                  public translate: TranslateService,
                  public loading: LoadingController,
                  public element: ElementRef) {
          this.picture_logo = "assets/img/MooExams_pre.png";
          this.picture_revision = "assets/img/mode_revision.png";
          this.picture_light = "assets/img/light.png";

          this.concoursId = this.navParams.get('concoursId');
          this.userId = this.navParams.get('userId');
          this.numero = this.navParams.get('numero');

          this.questionnaires = [];
          this.slideShow = -1;
          this.answers = [];
          this.answersTimer = [];
          this.confirmAnswer = [];
          this.score = {failed: 0, success: 0, points: 0};
          this.progressPercent = 53;
          this.interval = null;
          this.labels = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];
          this.url = "http://mooexams.sdkgames.com/";
          this.timerPosition = 0;
          this.serveurReponse = false;
          this.currentsPoints=0;
          this.currentReponse=-1;
          this.myPoints=0;
          this.formatTime="00:00";
          this.intervalTime=null;
          this.time=0;
      }


      ionViewDidLoad() {
          this.getQuestions();
          this.initTimer();
          this.content.resize();
      }

      ionViewWillLeave(){
          this.exitTimer();
         // this.exitUserTimer();
      }


      ionViewDidLeave(){
          this.exitTimer();
          //this.exitUserTimer();
      }


      //Clear the timer
      clearTimer(){
          clearInterval(this.interval);
          this.interval=null;
      }

      exitTimer(){

        clearInterval(this.interval);
        this.interval=null;

       for(let i=0;i<1000;i++){
            clearInterval(i);
        }
      }


      initTimer() {

          this.answersTimer.push({
              value: 5,
              points: 5,
              border: "#3cb44b",
              color: "#fff",
              background: "rgba(60, 180, 75,0.2)",
              time: 30000,
              percent: 100
          });
          this.answersTimer.push({
              value: 4,
              points: 4,
              border: "#3cb44b",
              color: "#fff",
              background: "rgba(60, 180, 75,0.2)",
              time: 30000,
              percent: 80
          });
          this.answersTimer.push({
              value: 3,
              points: 3,
              border: "#f58231",
              color: "#fff",
              background: "rgba(245, 130, 48,0.2)",
              time: 30000,
              percent: 60
          });
          this.answersTimer.push({
              value: 2,
              points: 2,
              border: "#f58231",
              color: "#fff",
              background: "rgba(245, 130, 48,0.2)",
              time: 30000,
              percent: 40
          });
          this.answersTimer.push({
              value: 1,
              points: 1,
              border: "#e6194b",
              color: "#fff",
              background: "rgba(230, 25, 75,0.2)",
              time: 30000,
              percent: 20
          });
          this.answersTimer.push({
              value: 0,
              points: 1,
              border: "#e6194b",
              color: "#fff",
              background: "rgba(230, 25, 75,0.2)",
              time: 100,
              percent: 0
          });
      }

      sendNotificationCloseModal() {
        this.events.publish('modal:close',Date.now());
     }



      formatTimer(time:number) :string{
            let minute = Math.floor(time/60);
            let seconde = time%60;

            return ""+(minute<10?'0'+minute:minute)+ ':'+(seconde<10?'0'+seconde:seconde)+"";
      }

      //set the user end of game timer
      endusertimer(){

        this.intervalTime = setInterval(() => {

            if(this.time<360) {this.time++;}
            else{

                this.exitTimer();
                this.exitUserTimer();
                this.sendNotificationCloseModal();

                let resultats={
                    points:this.score.points,
                    success:this.score.success,
                    failed:this.score.failed
                };

                this.navCtrl.push(ModalConcoursPage,{resultats:resultats});
            }

            this.formatTime=this.formatTimer(this.time);

        },1000);
      }

//Clear the user Timer
      clearUserTimer(){
        clearInterval(this.intervalTime);
        this.intervalTime=null;
    }


    exitUserTimer(){

        clearInterval(this.intervalTime);
        this.intervalTime=null;

        for(let i=0;i<1000;i++){
            clearInterval(i);
        }
    }




      setTimer(index) {

          this.currentsPoints = this.answersTimer[0].points;

          this.interval = setInterval(() => {

              this.timerPosition++;
              if (this.timerPosition < 6) {
                  this.currentsPoints = this.answersTimer[this.timerPosition].points;
              }
              if (this.timerPosition == 6) {

                  this.timerPosition = 0;
                  if (!this.questionnaires[index].choix) {
                      this.answers[index] = 0;
                  }
                  this.confirm(index);

                  clearInterval(this.interval);
              }

          }, this.answersTimer[this.timerPosition].time);

      }


      //Start to answer question
      begin() {

          this.slideShow = 0;
          this.score = {failed: 0, success: 0, points: 0};
          this.setTimer(0);
          //this.endusertimer();
          //init array of id answers
          for (let i = 0; i < this.questionnaires.length; i++) {
              this.answers.push(0);
              this.confirmAnswer.push(false);
          }
          this.content.resize();

      }


      //Next question
      next(index: number) {

          if(this.questionnaires[index].bonne_reponse.id==this.answers[index]){
            this.score.points = this.score.points + this.currentsPoints;
            this.score.success++
          }else{
           this.score.failed++;
           this.currentsPoints=0;
          }

          this.currentReponse=this.questionnaires[index].id;
          this.myPoints=this.currentsPoints;

          //Destroy the time
          this.currentsPoints=0;
          this.timerPosition = 0;
          clearInterval(this.interval);

          this.interval=null;
        /*  for(let i=0;i<1000;i++){
            clearInterval(i);
          }*/

          if (this.slideShow !== this.questionnaires.length) {
              this.slideShow++;
              this.content.resize();
              this.setTimer(index+1);
          }

          this.save(this.questionnaires);
      }

      //the choice make by a user
      selectProposition(index: number, propo: number) {
          this.questionnaires[index].choix = true;

          if (this.questionnaires[index].bonne_reponse.id == propo) {
              this.answers[index] = this.questionnaires[index].bonne_reponse.id;
          } else {
              this.answers[index] = 0;
          }
      }

      //confirm the user choice
      confirm(index) {
          this.confirmAnswer[index] = true;
          //Add the sound corresponding to the good or bad answers

          if(this.answers[index]==0){
              this.openModalEchec(index);
          }else{
              this.openModalSuccess(index);
          }

      }



    //Open the modal  success
    openModalSuccess(index){

        //Play success sound
        let modal = this.modalCtrl.create(ModalSuccessPage,{question:this.questionnaires[index]});

        modal.onDidDismiss(data => {
            this.next(index);
        });
        modal.present();
    }



    //Open the modal failed
    openModalEchec(index){

         //Play Failure sound
        let modal = this.modalCtrl.create(ModalFailedPage,{question:this.questionnaires[index]});

        modal.onDidDismiss(data => {
            this.next(index);
        });

        modal.present();
    }


      //Select the number of questions
      getQuestions() {

          this.storage.get('token').then(token => {

               let param = {
                  "concours_id":this.concoursId,
                  "user_id":this.userId,
                  "numero" : this.numero,
                  "matiere_id":""
                };

              this.concours.getconcoursquestions(param, token)

                  .then(data => {

                      this.serveurReponse = true;

                      if (data && data.reponse=="success") {
                        this.questionnaires = data.questions;
                        this.content.resize();
                      }else if(data && data.reponse=="already_answer"){

                        this.questionnaires = [];
                        this.translate.get(['CONCOURS_QUESTIONS_QUOTA']).subscribe(value => {
                            this.toastCtrl.create({
                                message: value.CONCOURS_QUESTIONS_QUOTA,
                                duration: 3000
                            }).present();
                        });
                        this.navCtrl.pop();
                      }else if(data && data.reponse=="questions_not_found"){
                        this.questionnaires = [];
                        this.translate.get(['CONCOURS_QUESTIONS_MESSAGE']).subscribe(value => {
                            this.toastCtrl.create({
                                message: value.CONCOURS_QUESTIONS_MESSAGE,
                                duration: 3000
                            }).present();
                        });
                      }else if(data && data.reponse=="same_device"){
                        this.questionnaires = [];
                        this.translate.get(['CONCOURS_QUESTIONS_SAME_DEVICE']).subscribe(value => {
                            this.toastCtrl.create({
                                message: value.CONCOURS_QUESTIONS_SAME_DEVICE,
                                duration: 3000
                            }).present();
                        });

                      }else{
                        this.questionnaires = [];
                      }
                  }).catch(err => {

                      this.serveurReponse = true;
                      if (err && err.reponse == "failure") {
                          this.translate.get(['CONCOURS_QUESTIONS_MESSAGE']).subscribe(value => {
                              this.toastCtrl.create({
                                message: value.CONCOURS_QUESTIONS_MESSAGE,
                                duration: 3000
                            }).present();
                          });
                      }
                  });

          });
      }


      //Format the data
      format(chaine: string) {

          let ch = String(chaine);
          let resultat = ch.replace(/\\'/g, "'");
          resultat = resultat.replace(/\\"/g, '"');
          return resultat;
      }


      //save the result of the challenge
      save(data: any) {

          let param = {
              "concours_id":0,
              "user_id":0,
              "reponse":0,
              "numero":"",
              "nbrePoints":0
          };

         param.user_id = this.userId;
         param.numero = this.numero;
         param.concours_id = this.concoursId;
         param.reponse = this.currentReponse;
         param.nbrePoints = this.myPoints;
         this.myPoints=0;

         this.storage.get('token').then(token => {

        if(this.slideShow==this.questionnaires.length){
            this.loader = this.loading.create({
                content: '',
                dismissOnPageChange: true,
                duration: 60000
            });

            this.loader.present();

         }

            this.concours.saveconcoursquestions(param, token)
            .then(data => {


                    if (data && data.reponse == "success") {

                        if(this.slideShow==this.questionnaires.length){

                           /* this.translate.get(['CONCOURS_QUESTIONS_QUOTA']).subscribe(value => {
                                this.toastAlert(value.CONCOURS_QUESTIONS_QUOTA);
                            });*/

                            this.loader.dismiss();

                            let resultats={
                                points:this.score.points,
                                success:this.score.success,
                                failed:this.score.failed
                            };

                            this.navCtrl.push(ModalConcoursPage,{resultats:resultats});
                        }
                    }

            }).catch( err => {


                    if (data && data.reponse == "failure") {
                              this.translate.get(['CONCOURS_SAVE_RESULT_FAILED']).subscribe(value => {

                                  this.toastCtrl.create({
                                    message: value.CONCOURS_SAVE_RESULT_FAILED,
                                    duration: 3000
                                }).present();
                        });
                    }

                    if(this.slideShow==this.questionnaires.length){
                        this.loader.dismiss();
                        this.navCtrl.pop();
                    }

             });

            });

      }


      //Go to the DashBoard
      gotoDash(){
          this.navCtrl.setRoot(HomePage);
      }


  }



