import { Component, ElementRef, ViewChild} from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators} from "@angular/forms";
import { EmailComposer } from '@ionic-native/email-composer';
import { UserProvider} from "../../providers/user/user";
import { TranslateService} from "@ngx-translate/core";
import { ProfilPage} from "../profil/profil";
import { Storage } from "@ionic/storage";
/**
 * Generated class for the ContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {

    @ViewChild('texte') texte : ElementRef;

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public fb:FormBuilder,
      private emailComposer: EmailComposer,
      public user:UserProvider,
      public translate:TranslateService,
      public storage:Storage

  ) {

  }

  ionViewDidLoad() {
        this.initForm();
  }

    public contactForm=this.fb.group({
        username:["",Validators.compose([Validators.required])],
        objet:["",Validators.compose([Validators.required])],
        message:["",Validators.compose([Validators.required])],
        email:["",Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z0-9_.]+@[a-zA-Z0-9_.]+\\.[a-zA-Z]{2,5}')])]
    });


   //Init the form
    initForm(){

        this.storage.get('user').then(user=>{

            this.contactForm=this.fb.group({
                username:[user.username,Validators.compose([Validators.required])],
                objet:["",Validators.compose([Validators.required])],
                message:["",Validators.compose([Validators.required])],
                email:[user.email,Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z0-9_.]+@[a-zA-Z0-9_.]+\\.[a-zA-Z]{2,5}')])]
            });

        });

    }

    //Show the error message
    showMessage(){
        this.translate.get(['ERROR_MESSAGE']).subscribe(value=>{
            this.user.toastAlert(value.ERROR_MESSAGE);
        });
    }


    //Resize the textarea
    resize(){

        if(this.texte['_elementRef']) {
            let element = this.texte['_elementRef'].nativeElement.getElementsByClassName("text-input")[0];
            element.style.overflow = 'hidden';
            element.style.height = 'auto';
            element.style.height = element.scrollHeight + 'px';
        }
    }

    //Send the email to the administrator
    sendEmail(data){

        let email = {
            to: 'info@sdkgames.com',
            cc: 'kamenrodrigues@sdkgames.com',
            bcc: ['christian@sdkgames.com'],
            attachments: [],
            subject: data.objet,
            body: data.message+'\n\n'+data.username,
            isHtml: false
        };

        this.emailComposer.isAvailable().then((available: boolean) =>{
            if(available) {
                // Send a text message using default options
                this.emailComposer.open(email).then(data=>{
                    this.initForm();
                },err=>{
                    this.showMessage();
                });
            }else{
                this.showMessage();
            }
        });


    }

    //Go to the profile page
    goProfile(){
        this.navCtrl.push(ProfilPage);
    }
}
