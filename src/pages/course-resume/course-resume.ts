import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams } from 'ionic-angular';
import { Storage } from "@ionic/storage";

import { QuestionRevisionPage } from "../question-revision/question-revision";

import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { UserProvider } from "../../providers/user/user";
import { TranslateService } from "@ngx-translate/core";
import { LeconsProvider } from "../../providers/lecons/lecons";
import { HomePage } from "../home/home";
/**
 * Generated class for the CourseResumePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-course-resume',
  templateUrl: 'course-resume.html',
})
export class CourseResumePage {

  course:any;
  lesson:any;
  url: string;
  param:any;
  loader:any;

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public storage:Storage,
      public loaderCtrl : LoadingController,
      private youtube: YoutubeVideoPlayer,
      public userService:UserProvider,
      public translate:TranslateService,
      public lessonService:LeconsProvider

  ) {

    this.course=this.navParams.get('subject');
    this.lesson=this.navParams.get('lessons');
    this.url="http://mooexams.sdkgames.com/";
    this.param={id_matiere:0, id_user:0, id_lecon:0};

  }




  ionViewDidLoad() {
      this.updateLevels();
  }


  //Go to the page of lesson questions
    answerQuestion(course,lecon){


        this.param.id_matiere=course.id;
        this.param.id_lecon=lecon.id;

        this.loader = this.loaderCtrl.create({
            content:'',
            dismissOnPageChange:true
        });

        this.loader.present();

        this.storage.get('user').then(user=>{

            this.loader.dismiss();
            this.param.id_user=user.id;

            this.navCtrl.push(QuestionRevisionPage,{params :this.param});
        });
    }


    //Format the data
    format(chaine:string){

        let ch=String(chaine);
        let resultat=ch.replace(/\\'/g,"'");
        resultat=resultat.replace(/\\"/g,'"');
        return resultat;
    }


    //Play the youtube Video
    openVideo(videoId:string){
        if(videoId&&videoId!=""){
            this.youtube.openVideo(videoId);
        }else{
            this.translate.get(['VIDEO_EMPTY_MESSAGE']).subscribe(value=>{
                this.userService.toastAlert(value.VIDEO_EMPTY_MESSAGE);
            });
        }

    }

    //Play the summary son
    openAudio(audioLink:string){

        if(audioLink&&audioLink!=""){
            //lecteur audio
        }else{
            this.translate.get(['AUDIO_EMPTY_MESSAGE']).subscribe(value=>{
                this.userService.toastAlert(value.AUDIO_EMPTY_MESSAGE);
            });
        }
    }

    //Open the pdf
    openPdf(pdfLink:string){
        if(pdfLink&&pdfLink!=""){
           //lecteur pdf
        }else{
            this.translate.get(['PDF_EMPTY_MESSAGE']).subscribe(value=>{
                this.userService.toastAlert(value.PDF_EMPTY_MESSAGE);
            });
        }
    }


    //GO TO the user profile
    gotoDash(){
        this.navCtrl.setRoot(HomePage);
    }

    //update the level of the user in the subjets
    updateLevels(){
        this.storage.get('token').then(token=> {

            this.storage.get('user').then(user => {

                this.storage.get('user-school').then(school=>{

                    this.storage.get('user-classes').then(classe=>{
                        let param = {"user_id":user.id,"classe_id":classe.id,"matiere_id":this.course.id,"lecon_id":this.lesson.id,"annee":school.annee};
                        this.lessonService.lessonsLevels(param,token);
                    });

                });

            });
        });
    }

    //Get the user course Level and show
    getUserCourseLevels(){

    }

}
