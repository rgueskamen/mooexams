import {Component, ElementRef, ViewChild} from '@angular/core';
import { Storage } from "@ionic/storage";
import { AlertController, LoadingController, NavController, NavParams} from 'ionic-angular';
import { MatiereProvider} from "../../providers/matiere/matiere";
import { FormBuilder, Validators} from "@angular/forms";
import { ChallengeProvider} from "../../providers/challenge/challenge";
import { ApiProvider} from "../../providers/api/api";
import { TranslateService} from "@ngx-translate/core";
import { DatePicker} from "@ionic-native/date-picker";
import { ProfilPage} from "../profil/profil";
import { UserProvider} from "../../providers/user/user";
import { LeconsProvider} from "../../providers/lecons/lecons";
import { MyChallengePage} from "../my-challenge/my-challenge";



/**
 * Generated class for the MyChallengePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
    selector: 'page-create-challenge',
    templateUrl: 'create-challenge.html',
})
export class CreateChallengePage {

    @ViewChild('texte') texte:ElementRef;

    user:any;
    classe:any;
    listMatiere:any;
    testRadioResult:string;
    nbre_question:string;
    defaultTime:Date[];
    shortDays:string[];
    loading:any;

    
    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public matiere:MatiereProvider,
        public storage:Storage,
        public fb:FormBuilder,
        public challenge:ChallengeProvider,
        public loader:LoadingController,
        public api:ApiProvider,
        public translate:TranslateService,
        public alertCtrl:AlertController,
        private datePicker:DatePicker,
        public userService:UserProvider,
        public lesson:LeconsProvider
    ) {

        this.user=this.navParams.get('user');
        this.classe=this.navParams.get('classe');
        this.listMatiere=[];
        this.testRadioResult="";
        this.nbre_question="";
        let date =new Date();
        this.defaultTime=[];
        this.defaultTime.push(date);
        this.defaultTime.push(date);

        this.shortDays=[];
        this.translate.get(['SHORTDAY1','SHORTDAY2','SHORTDAY3','SHORTDAY4','SHORTDAY5','SHORTDAY6','SHORTDAY7'])
            .subscribe(days=>{
                this.shortDays.push(days.SHORTDAY1);
                this.shortDays.push(days.SHORTDAY2);
                this.shortDays.push(days.SHORTDAY3);
                this.shortDays.push(days.SHORTDAY4);
                this.shortDays.push(days.SHORTDAY5);
                this.shortDays.push(days.SHORTDAY6);
                this.shortDays.push(days.SHORTDAY7);
            });

    }

    ionViewDidLoad() {
        this.initForm();
        this.getMatieres();
    }


    //Form data
    public challengeForm=this.fb.group({

        "id_matiere":[""],
        "user_id":[""],
        "id_niveau":[""], // le tab d’ids des niveaux des challenge
        "nbre_question":[100,Validators.compose([Validators.required])], // nombre de questions du challenge
        "titre":["",Validators.compose([Validators.required])],
        "description":["",Validators.compose([Validators.required])],
        "image":[""],
        "video":[""],
        "active" : 1,
        "date_debut": ["",Validators.compose([Validators.required])],
        "date_fin": ["",Validators.compose([Validators.required])],
        "date_modification":[""]

    });


    initForm(){

        let date=new Date();
        this.challengeForm=this.fb.group({
            "id_matiere":[""],
            "user_id":[this.user.id],
            "id_niveau":[""], // le tab d’ids des niveaux des challenge
            "nbre_question":[100,Validators.compose([Validators.required])], // nombre de questions du challenge
            "titre":["",Validators.compose([Validators.required])],
            "description":["",Validators.compose([Validators.required])],
            "image":[""],
            "video":[""],
            "active":1,
            "date_debut": [date,Validators.compose([Validators.required])],
            "date_fin": [date,Validators.compose([Validators.required])],
            "date_modification":[date]
        });

        this.getLevels();
    }


    // Select the picture or select on the gallery
    getPicture(){

        this.api.selectPicture().then(image=>{
            this.challengeForm.controls['image'].setValue(image);
        }).catch(err=>{

            if(err==="none"){
                this.translate.get(['PICTURE_ERROR_MESSAGE']).subscribe(value=>{
                    this.userService.toastAlert(value.PICTURE_ERROR_MESSAGE);
                });
            }
        });
    }


    //Get the liste of levels
    getLevels(){

        let levels="";
        this.storage.get('niveaux-matieres').then(niveaux=>{

            if(niveaux&&niveaux.length>0){
                for(let i=0;i<niveaux.length;i++){

                    if(levels==""){
                        levels=niveaux[i].id;
                    }else{
                        levels+=";"+niveaux[i].id;
                    }
                }

                this.challengeForm.controls['id_niveau'].setValue(levels);
            }else{

                this.storage.get('token').then(token=>{

                    this.matiere.getLevels(token)
                        .then(data=>{

                        if(data.niveaux&&data.niveaux.length>0){
                            for(let i=0;i<data.niveaux.length;i++){
                                if(levels==""){
                                    levels=data.niveaux[i].id;
                                }else{
                                    levels+=";"+data.niveaux[i].id;
                                }
                            }
                            this.challengeForm.controls['id_niveau'].setValue(levels);
                        }

                    }).catch(err=>{

                    });

                });

            }

        });
    }


    //Add criteria to the challenge

    addCriteriaChallenge(data){

        let param={
            "challenge_id":data.id,
            "criteres_countries_code":"",
            "criteres_town":"",
            "criteres_gender":"F#M",
            "criteres_id_classe":"",
            "criteres_langue":"en#fr",
            "criteres_nbrePoints":50
        };

        this.storage.get('user-classes').then(classe=>{
            param["criteres_id_classe"]=classe.id;
            this.storage.get('token').then(token=>{
                this.challenge.addCriteria(param,token);
            });
        });
    }


    //SAVE THE CHALLENGES
    saveChallenge(data:any){

        this.storage.get('token').then(token=>{

            this.loading=this.loader.create({
                content:'',
                dismissOnPageChange:true,
                duration: 60000
            });

            this.loading.present();
            this.challenge.createChallenge(data,token)
                .then(data=>{
                    this.loading.dismiss();
                    if(data&&data.reponse=="success"){
                        //we ask him to add criteria to the challenge
                        this.initForm();
                        if(data&&data.challenge){
                            this.addCriteriaChallenge(data.challenge);
                        }
                        this.navCtrl.setRoot(MyChallengePage);
                    }

                }).catch(err=>{

                    this.loading.dismiss();
                    if(err&&err.reponse=="failure"){
                        //we ask him to we show the error message
                        this.translate.get(['PICTURE_ERROR_MESSAGE']).subscribe(value=>{
                            this.userService.toastAlert(value.PICTURE_ERROR_MESSAGE);
                        });
                    }
                });
        });

    }


    //Get the list of matiere
    getMatieres(){

        let matiere=[];

        this.storage.get('token').then(token=>{
            this.matiere.classCourses(token).then(data=>{

                if(data){
                    Object.keys(data).map(function(key) {
                        matiere.push(data[key]);
                    });
                }
                this.listMatiere=matiere;

            });
        });
    }


    //Format the list of courses of the forum
    radioList(){

        let alert = this.alertCtrl.create();

        this.translate.get(['FORUM_SELECT_TEXT']).subscribe(value=>{
            alert.setTitle(value.FORUM_SELECT_TEXT);
        });

        for (let i = 0; i < this.listMatiere.length; i++) {

            if(this.listMatiere[i]){
                alert.addInput({
                    type: 'radio',
                    label: this.listMatiere[i].NOM_MATIERE.toUpperCase(),
                    value: this.listMatiere[i],
                    checked: false
                });
            }
        }

        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: data => {

                if(data){
                    this.testRadioResult=data.NOM_MATIERE;
                    this.challengeForm.controls['id_matiere'].setValue(data.id);
                }
            }
        });

        alert.present();
    }



    //SHOW THE DATEPICKER
    showClock(index){

        this.datePicker.show({
            date: new Date(),
            mode: 'date',
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
        }).then(
            date =>{

                switch(index){
                    case 0:
                        this.challengeForm.controls['date_debut'].setValue(date);
                        this.defaultTime[0]=date;
                        break;
                    case 1:
                        this.challengeForm.controls['date_fin'].setValue(date);
                        this.defaultTime[1]=date;
                        break;
                    default:
                        break;
                }
            },
            err => {
                this.translate.get(['PERIODE_ERROR']).subscribe(value=>{
                    this.userService.toastAlert(value.PERIODE_ERROR);
                });
            }
        );
    }

    //FORMAT THE DATE
    formatDate(date:Date):string{

        if(date) {
            let dateFormat=new Date(date);
            return " " + this.shortDays[dateFormat.getDay()] + " " + (dateFormat.getDate() <= 9 ? "0" + dateFormat.getDate() : dateFormat.getDate()) + " | " + ((dateFormat.getMonth() + 1) <= 9 ? "0" + (dateFormat.getMonth() + 1) : (dateFormat.getMonth() + 1)) + " | " + dateFormat.getFullYear();
        }else{
            return "";
        }
    }

    //Go to the profil Page
    goProfile(){
        this.navCtrl.push(ProfilPage);
    }


    //show number of questions
    showNbQuestions(question){

        //Create alert and set Title
        let alert = this.alertCtrl.create();

        alert.setTitle("Choisir un nombre");

        for(let j=0;j<question.length;j++) {
            //Prepare to challenge input
            alert.addInput({
                type: 'radio',
                label: question[j],
                value: question[j],
                checked: false
            });
        }

        //The CANCEL button
        alert.addButton('Cancel');

        //The OK button
        alert.addButton({
            text: 'Ok',
            handler: data => {
                if(data){
                    this.challengeForm.controls['nbre_question'].setValue(data);
                    this.nbre_question=String(data);
                }
            }
        });

        //Show the alert
        alert.present();
    }


    //Select the number of questions
    selectQuestion()
    {
        this.storage.get('token').then(token => {

            let data = {"id_matiere": this.challengeForm.value.id_matiere};

            this.lesson.lessons_with_nb_questions(data,token).then(data => {

                if (data&&data.rubriques) {
                    let nb_questions = 0;

                    for (let rubric of data.rubriques) {
                        nb_questions += parseInt(rubric.nbre_question);
                    }
                    this.showNbQuestions(this.api.numberToArray(nb_questions));
                }
            });

        });

    }


    //Resize the textarea
    resize(){

        if(this.texte['_elementRef']) {
            let element = this.texte['_elementRef'].nativeElement.getElementsByClassName("text-input")[0];
            element.style.overflow = 'hidden';
            element.style.height = 'auto';
            element.style.height = element.scrollHeight + 'px';
        }
    }

}
