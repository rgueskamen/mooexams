import { Component } from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';


/**
 * Generated class for the DetailUserPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-detail-user',
  templateUrl: 'detail-user.html',
})
export class DetailUserPage {

  userInfos:any;
  userClasse:any;
  userSchool:any;

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public viewCtrl:ViewController

  ) {

      this.userInfos=this.navParams.get('user');
      this.userClasse=this.navParams.get('classe');
      this.userSchool=this.navParams.get('school');

  }

  // Lifecycle
  ionViewDidLoad() {

  }

    //Close the page
    close(){
        this.viewCtrl.dismiss();
    }



}
