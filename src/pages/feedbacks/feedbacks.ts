import {Component, ElementRef, ViewChild } from '@angular/core';
import {LoadingController, NavController, NavParams} from 'ionic-angular';
import { FormBuilder, Validators } from "@angular/forms";
import { UserProvider } from "../../providers/user/user";
import { TranslateService } from "@ngx-translate/core";
import { ProfilPage } from "../profil/profil";
import { FeedbacksProvider } from "../../providers/feedbacks/feedbacks";
import { Storage } from "@ionic/storage";

/**
 * Generated class for the FeedbacksPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-feedbacks',
  templateUrl: 'feedbacks.html',
})
export class FeedbacksPage {

  @ViewChild('texte') texte : ElementRef;

    loader:any;

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public fb:FormBuilder,
      public user:UserProvider,
      public translate:TranslateService,
      public storage:Storage,
      public feeds:FeedbacksProvider,
      public loadingCtrl:LoadingController
  ) {

  }


    ionViewDidLoad() {
        this.initForm();
    }

    public feedbackForm=this.fb.group({
        username:["",Validators.compose([Validators.required])],
        application:["MOOEXAMS",Validators.compose([Validators.required])],
        email_tel:["",Validators.compose([Validators.required])],
        note:[1,Validators.compose([Validators.required])],
        suggestions:["",Validators.compose([Validators.required])],
        appreciations:["",Validators.compose([Validators.required])],
        autres:[""],
        rapport_bug:[""],
    });



      initForm(){
      this.storage.get('user').then(user=>{

          this.feedbackForm=this.fb.group({
              username:[user.username,Validators.compose([Validators.required])],
              email_tel:[user.email||user.tel,Validators.compose([Validators.required])],
              application:["MOOEXAMS",Validators.compose([Validators.required])],
              note:[1,Validators.compose([Validators.required])],
              suggestions:["",Validators.compose([Validators.required])],
              appreciations:["",Validators.compose([Validators.required])],
              autres:[""],
              rapport_bug:[""],
          });

      });


    }

    //Listen to the change
    onModelChange(event){

    }

    //Show the error message
    showMessage(){
        this.translate.get(['ERROR_MESSAGE']).subscribe(value=>{
            this.user.toastAlert(value.ERROR_MESSAGE);
        });
    }


    //Resize the textarea
    resize(){

        if(this.texte['_elementRef']) {
            let element = this.texte['_elementRef'].nativeElement.getElementsByClassName("text-input")[0];
            element.style.overflow = 'hidden';
            element.style.height = 'auto';
            element.style.height = element.scrollHeight + 'px';
        }
    }

    //Send the email to the administrator
    sendEmail(data){

      this.storage.get('token').then(token=>{

            this.loader = this.loadingCtrl.create({
                content: '',
                dismissOnPageChange:true,
                duration: 60000
            });

           this.loader.present();
          this.feeds.sendMessage(data,token)
          .then(data=>{

              this.loader.dismiss();

              if(data&&data.reponse=="success"){
                  this.initForm();
                  this.translate.get(['PERIODE_SUCCES']).subscribe(value=> {
                      this.user.toastAlert(value.PERIODE_SUCCES);
                  });

              }
          }).catch(err=>{

              this.loader.dismiss();
               if(data&&data.reponse=="failure"){

                   this.translate.get(['FAILURE']).subscribe(value=> {
                       this.user.toastAlert(value.FAILURE);
                   });

               }
          });
          
      });

    }

    //Go to the profile page
    goProfile(){
        this.navCtrl.push(ProfilPage);
    }

}
