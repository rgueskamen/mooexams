import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {NavController, LoadingController, NavParams, Events} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from "@ionic/storage";

import { UserProvider } from '../../providers/user/user';
import { HomePage } from '../home/home';
import { HomeParentPage } from '../home-parent/home-parent';
import {OneSignal} from "@ionic-native/onesignal";

/**
 * Generated class for the ForgotPassPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-forgot-pass',
    templateUrl: 'forgot-pass.html',
})

export class ForgotPassPage {

    loadingKeys:string[];
    loadingMessage:string[];
    title:string;
    hasCode:boolean;
    choix:string;
    logo:string;
    mode:any;
    body:any;
    isActive:boolean;
    loaderCode:any;


    constructor(

        public navCtrl: NavController,
        public fb:FormBuilder,
        public loadingCtrl:LoadingController,
        public translate:TranslateService,
        public navParams: NavParams,
        public user:UserProvider,
        private storage:Storage,
        public onesignal:OneSignal,
        public events:Events

    ) {

        this.isActive=false;
        this.storage.get('hasCode').then(hascode=>{
            if(hascode){
                this.hasCode=true;
            }else{
                this.hasCode=false;
            }
        });

        this.mode=[{mode:"sms",choix:false},{mode:"email",choix:false}];

        this.storage.get('CHOIX_PASS').then(value=>{

            this.choix=value||"sms";

            if(this.choix==='sms'){
                this.mode=[{mode:"sms",choix:true},{mode:"email",choix:false}];
            }else {
                this.mode=[{mode:"sms",choix:false},{mode:"email",choix:true}];
            }
        });


        this.logo="assets/img/MooExams.png";
        this.loadingKeys=["LOADER_LOGIN","LOADER_REGISTRATION","LOADER_CODE","LOADER_RESET_PASS","LOADER_TEXT","TITLE_GET_CODE","TITLE_GET_FORGOT"];
        this.translate.get(this.loadingKeys).subscribe((value) => {
            this.loadingMessage=[value.LOADER_LOGIN,value.LOADER_REGISTRATION,value.LOADER_CODE,value.LOADER_RESET_PASS,value.LOADER_TEXT,value.TITLE_GET_CODE,value.TITLE_GET_FORGOT];
            this.title=value.TITLE_GET_CODE;
        });

    }



    //Construction du controleur du formulaire de login
    public forgotPassForm = this.fb.group({

        email: ["", Validators.compose([Validators.pattern('^[a-zA-Z0-9_.]+@[a-zA-Z0-9_.]+\\.[a-zA-Z]{2,5}')])],
        phone: ["", Validators.compose([Validators.required,Validators.pattern('^[0-9]{9,13}')])],
        code: [""],
        token_notification: [""],
        password: [""]

    });


    initForm(){
        this.forgotPassForm = this.fb.group({

            email: ["", Validators.compose([Validators.pattern('^[a-zA-Z0-9_.]+@[a-zA-Z0-9_.]+\\.[a-zA-Z]{2,5}')])],
            phone: ["", Validators.compose([Validators.required,Validators.pattern('^[0-9]{9,13}')])],
            code: [""],
            token_notification: [""],
            password: [""]

        });

        this.registerPush();
    }



    //Return form instance control fields for login
    get code() { return this.forgotPassForm.get('code'); }
    get email() { return this.forgotPassForm.get('email'); }
    get phone(){ return this.forgotPassForm.get('phone'); }
    get passwordLogin() { return this.forgotPassForm.get('password'); }


    //Create event to send user data
    createUser(user) {
        this.events.publish('user:created', user, Date.now());
    }

    //Allow user to get a new password
    getCode(data:any){

        if(data.email===""){
            data.email="null";
        }

        if(data.phone===""){
            data.phone="null";
        }

        if(data&&(data.email!="null"||data.phone!="null")) {
            this.loaderCode = this.loadingCtrl.create({
                content: "",
                dismissOnPageChange: true,
                duration: 60000
            });
            this.loaderCode.present();

            this.user.getcode(data)
                .then(data => {
                this.loaderCode.dismiss();
                if (data && data.reponse == "success") {
                    this.hasCode = true;
                    this.storage.set('hasCode', true);
                }

            }).catch(error => {
                this.loaderCode.dismiss();
            });
        }else{
            this.hasCode = false;
            this.storage.set('hasCode', false);
        }
    }


    //Show and hide the password
    showPass(){
        this.isActive=!this.isActive;
    }


    //choose the method to receive the verification code
    getMode(mode:string){

        this.choix=mode;
        this.storage.set('CHOIX_PASS',mode);

        switch (mode){
            case "sms":
                this.mode[0].choix=true;
                this.mode[1].choix=false;
                break;
            case "email":
                this.mode[1].choix=true;
                this.mode[0].choix=false;
                break;
            default:
                break;
        }
    }



    //Reset the password
    reset(data:any){

        if(data.email===""){
            data.email="null";
        }

        if(data.phone===""){
            data.phone="null";
        }
        this.loaderCode = this.loadingCtrl.create({
            content :  "",
            dismissOnPageChange:true,
            duration: 60000
        });
        this.loaderCode.present();
        this.user.resetPassword(data)
            .then(res=>{
            this.loaderCode.dismiss();


            if (res&&res.reponse==="success") {

                this.createUser(res.user);
                this.initForm();


                switch (res.user.role_id) {


                    case 4:
                        this.navCtrl.setRoot(HomePage);
                        break;

                    case 5:
                        this.navCtrl.setRoot(HomeParentPage);
                        break;

                    case 6:
                        this.navCtrl.setRoot(HomeParentPage);
                        break;

                    default:
                        this.navCtrl.setRoot(HomePage);
                        break;
                }
            }
        }).catch(error=>{
            this.loaderCode.dismiss();
            this.initForm();
        });
    }

    //Register a pushNotification
    registerPush() {
        this.onesignal.getIds().then( ids=> {
            if(ids&&ids.userId){
                this.storage.set('device_id',ids.userId);
                this.forgotPassForm.controls['token_notification'].setValue(ids.userId);
            }
        });
    }


    ionViewDidLoad() {
        this.initForm();
    }

}
