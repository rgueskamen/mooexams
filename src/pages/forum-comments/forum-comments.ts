import {Component, ElementRef, ViewChild} from '@angular/core';
import {LoadingController,Content, NavController, NavParams} from 'ionic-angular';
import { ProfilPage } from "../profil/profil";
import { FormBuilder,Validators } from "@angular/forms";
import { Storage } from "@ionic/storage";

import { User } from "../../models/user";
import { TranslateService } from "@ngx-translate/core";
import { ApiProvider } from "../../providers/api/api";
import { ForumProvider } from "../../providers/forum/forum";
import { UserProvider } from "../../providers/user/user";

/**
 * Generated class for the ForumCommentsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-forum-comments',
    templateUrl: 'forum-comments.html',
})

export class ForumCommentsPage {

    @ViewChild('myMessage') myMessage : ElementRef;
    @ViewChild(Content) content: Content;

    posts:any;
    matieres:any;
    commentaires:any;
    statut:string[];
    focus:boolean;
    classes:any;
    classesComments:any;
    users:any;
    usersComments:any;
    userId:number;
    commentLoading:any;
    url:string;
    sendComment:boolean;


    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public fb:FormBuilder,
        public translate:TranslateService,
        public api:ApiProvider,
        public storage:Storage,
        public forum:ForumProvider,
        public loader:LoadingController,
        public userService : UserProvider,
        public forums:ForumProvider
    ) {
        this.posts=this.navParams.get('posts');
        this.matieres=this.navParams.get('matieres');
        this.classes=this.navParams.get('classes');
        this.classesComments=this.navParams.get('classesComments');
        this.users=this.navParams.get('users');
        this.usersComments=this.navParams.get('usersComments');
        this.commentaires=this.navParams.get('commentaires');
        this.focus=false;
        this.sendComment=false;
        this.url="http://mooexams.sdkgames.com/";
        this.statut=[];
        this.translate.get(['PARENT','ENSEIGNANT']).subscribe(value=>{
            this.statut.push(value.PARENT);
            this.statut.push(value.ENSEIGNANT);
        });

        if(this.posts){
            this.commentForm.controls['id_post'].setValue(this.posts.id);
        }


        this.storage.get('user').then(user=>{

            if(user){
                this.commentForm.controls['user_id'].setValue(user.id);
                this.userId=user.id;
            }

        });

    }

    ionViewDidLoad() {
        this.initForm();
    }


    public commentForm =  this.fb.group({
        id_post:[""],
        message: ["", Validators.compose([Validators.required,Validators.maxLength(1000)])],
        image:[""],
        user_id:[""],
        video:[""],
        audio:[""]
    });


    resize(){
        this.sendComment=false;
        if(this.myMessage['_elementRef']) {
            let element = this.myMessage['_elementRef'].nativeElement.getElementsByClassName("text-input")[0];
            element.style.overflow = 'hidden';
            element.style.height = 'auto';
            element.style.height = element.scrollHeight + 'px';
        }
    }




    initForm(){
        this.commentForm =  this.fb.group({
            id_post:[""],
            message: ["", Validators.compose([Validators.required,Validators.maxLength(1000)])],
            image:[""],
            user_id:[""],
            video:[""],
            audio:[""]
        });

        if(this.posts){
            this.commentForm.controls['id_post'].setValue(this.posts.id);
        }



        this.storage.get('user').then(user=>{

            if(user){
                this.commentForm.controls['user_id'].setValue(user.id);
                this.userId=user.id;
            }

        });
    }

    //Go to the user profile
    goProfile(){
        this.navCtrl.push(ProfilPage);
    }

    //Get the class  of the user
    getclass(userId:number,classes:any):string{

        let classe ="";

        if(classes){

            Object.keys(classes).map(function(key) {

                if(key===String(userId)){

                    if(classes[key]&&classes[key].OPTION_CLASSE){
                        classe =classes[key].NOM_CLASSE +' '+classes[key].OPTION_CLASSE;
                    }else{
                        classe =classes[key].NOM_CLASSE;
                    }
                }
            });
        }

        return classe;
    }



    //Get the username
    getUserInfos(userId:number,user:any){

        let userInfo:User=new User();

        if(user){

            Object.keys(user).map(function(key) {
                if(user[key]&&user[key].id===userId){
                    userInfo=user[key];
                }
            });
        }

        return userInfo;
    }

    //Format the post Date
    formatDatePost(date:string) :string{

        let dateFormat="";
        let currentDate=new Date();

        if(date){
            let postDate=new Date(date);
            if(currentDate.getDay()===postDate.getDay()){
                this.translate.get(['TODAY']).subscribe(value=>{
                    dateFormat=value.TODAY;
                });
            }else{
                dateFormat=date;
            }
        }

        return dateFormat;
    }


    //Get the picture of the comments
    getPostPicture(){

        this.api.takePicture().then(imagedata=>{
            this.commentForm.controls['image'].setValue(imagedata);
        }).catch(err=>{

                if(err==="none"){
                    this.translate.get(['PICTURE_ERROR_MESSAGE']).subscribe(value=>{
                        this.userService.toastAlert(value.PICTURE_ERROR_MESSAGE);
                    });
                }
        });

    }

    //Send the post comment
    sendPost(data:any){

        this.commentLoading = this.loader.create({
            content: '',
            dismissOnPageChange:true,
            duration: 60000
        });
        this.commentLoading.present();

        this.storage.get('token').then(token=>{
            this.forum.commentPost(data,token)
                .then(data=>{
                    this.commentLoading.dismiss();
                    if(data&&data.reponse==="success"){

                        this.initForm();
                        this.content.resize();
                        this.sendComment=true;

                        this.translate.get(['FORUM_COMMENT_SUCCESS_MESAGE']).subscribe(value=>{
                            this.userService.toastAlert(value.FORUM_COMMENT_SUCCESS_MESAGE);
                        });
                        
                        if(data.commentaires)
                             this.commentaires=data.commentaires;
                        if(data.users)
                             this.usersComments=data.users;
                        if(data.classes)
                             this.classesComments=data.classes;
                    }

                }).catch(err=>{

                    this.commentLoading.dismiss();
                    if(err&&err.reponse==="failure"){

                        this.translate.get(['FORUM_COMMENT_ERROR_MESAGE']).subscribe(value=>{
                            this.userService.toastAlert(value.FORUM_COMMENT_SUCCESS_MESAGE);
                        });

                        this.initForm();
                        this.content.resize();
                        this.sendComment=true;

                    }

                });
        });


    }


    //think about event to make update
    //Like the Post
    likePost()
    {
        // add a view to the post
        this.storage.get('token').then(token=>{
            let data={"id_post":this.posts.id,"user_id":this.userId};
            this.forums.likePost(data,token)
                .then(data=>{

                    if(data&&data.reponse==="success"){
                        this.posts.nb_likes+=1;
                        this.forums.getPosts(token);
                    }
                }).catch(err=>{});
        });
    }

    //Like a comment

    likeComment(comment:any){
        // add a view to the post
        this.storage.get('token').then(token=>{
            let data={"comment_id":comment.id,"user_id":this.userId};
            this.forums.likePostComment(data,token)
                .then(data=>{

                    if(data&&data.reponse==="success"){
                        if(data.commentaires){
                            this.commentaires=data.commentaires;
                        }
                    }

                }).catch(err=>{

                });
        });
    }

    //coment post
    commentPost(){
        this.focus=true;
    }

    //Delete a post
    deleteComment(comment,index){

        this.storage.get('token').then(token=>{

            this.storage.get('user').then(user=>{

                this.commentLoading = this.loader.create({
                    content: '',
                    dismissOnPageChange:true,
                    duration: 60000
                });

                this.commentLoading.present();

                let param={"id_post":this.posts.id,"id_comment":comment.id,"user_id":user.id};
                this.forums.deletePostComment(param,token)
                    .then(data=>{

                        this.commentLoading.dismiss();
                        if(data&&data.reponse=="success"){
                            this.commentaires.splice(1,index);
                        }

                    }).catch(err=>{

                        this.commentLoading.dismiss();
                        if(err&&err.reponse=="failure"){
                            this.translate.get(['FORUM_POST_COMMENTS_DELETE_FAILED']).subscribe(value=>{
                                this.userService.toastAlert(value.FORUM_POST_COMMENTS_DELETE_FAILED);
                            });
                        }
                    });

            });
        });

    }

    //Delete a post
    NoteComment(comment,note){

        this.storage.get('token').then(token=>{

            this.storage.get('user').then(user=>{

                this.commentLoading = this.loader.create({
                    content: '',
                    dismissOnPageChange:true,
                    duration: 60000
                });

                this.commentLoading.present();

                let param={"id_post":this.posts.id,"comment_id":comment.id,"user_id":user.id,"note":note};
                this.forums.notePostComment(param,token)
                    .then(data=>{

                        this.commentLoading.dismiss();
                        if(data&&data.reponse=="success"){
                            if(data.commentaires&&data.commentaires.length>0){
                                this.commentaires=data.commentaires;
                            }
                        }

                    }).catch(err=>{

                        this.commentLoading.dismiss();
                        if(err&&err.reponse=="failure"){
                            this.translate.get(['FORUM_POST_COMMENTS_NOTE_FAILED']).subscribe(value=>{
                                this.userService.toastAlert(value.FORUM_POST_COMMENTS_DELETE_FAILED);
                            });
                        }
                    });

            });
        });

    }

}
