import { Component,ViewChild } from '@angular/core';
import { NavController, Content, NavParams, ModalController, AlertController, LoadingController } from 'ionic-angular';
import { TranslateService} from "@ngx-translate/core";
import { Storage } from "@ionic/storage";
import { FormControl } from "@angular/forms";
import { AddPostPage } from '../add-post/add-post';
import { ProfilPage } from "../profil/profil";
import { ForumCommentsPage } from "../forum-comments/forum-comments";
import { ForumProvider } from "../../providers/forum/forum";
import { User } from "../../models/user";
import { MatiereProvider } from "../../providers/matiere/matiere";
import { ApiProvider } from "../../providers/api/api";
import 'rxjs/add/operator/debounceTime';
import { UserProvider } from "../../providers/user/user";



/**
 * Generated class for the ForumPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-forum',
    templateUrl: 'forum.html',
})
export class ForumPage {

    @ViewChild(Content) content: Content;

    posts:any;
    matieres:any;
    users:any;
    usersComments:any;
    statut:string[];
    classes:any;
    classesComments:any;
    commentaires:any;
    showFilter:boolean;
    showCancelButton:boolean;
    listMatiere:any;
    testRadioResult:string;
    pattern:string;
    searchControl: FormControl;
    searching: any = false;
    serveurReponse:boolean;
    loader:any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        public forums:ForumProvider,
        public storage:Storage,
        public translate : TranslateService,
        public matiere:MatiereProvider,
        public alertCtrl:AlertController,
        public api:ApiProvider,
        public userService:UserProvider,
        public loadingCtrl:LoadingController

    ) {

        this.posts=[];
        this.matieres=[];
        this.users=[];
        this.usersComments=[];
        this.classes=[];
        this.classesComments=[];
        this.commentaires=[];
        this.showFilter=false;
        this.showCancelButton=false;
        this.listMatiere=[];
        this.testRadioResult="";
        this.statut=[];
        this.getPostsFromLocal();
        this.searchControl = new FormControl();
        this.pattern="";
        this.serveurReponse=false;

        this.translate.get(['PARENT','ENSEIGNANT']).subscribe(value=>{
            this.statut.push(value.PARENT);
            this.statut.push(value.ENSEIGNANT);

        });

    }

    ionViewDidLoad() {

        this.listPost(0,true);
        this.getItems();
        this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
            this.searching = false;
            this.getItems();
        });
    }






    /***
     * Add a post to a forum.
     */

    addPost(){

        let modal = this.modalCtrl.create(AddPostPage);

        modal.onDidDismiss(data => {
            this.getPostsFromServer();
        });

        modal.present();
    }


    //Go to the user profile
    goProfile(){
        this.navCtrl.push(ProfilPage);
    }



    //get the post on the server
    getPostsFromServer(){
        this.storage.get('token').then(token=>{

            this.forums.getPosts(token)
                .then(data=>{

                if(data&&data.posts){
                    this.posts=data.posts.post;
                    this.commentaires=data.posts.commentaires;
                }
                if(data&&data.matieres)
                    this.matieres=data.matieres;
                if(data&&data.users)
                    this.users=data.users;
                if(data&&data.usersComments)
                    this.usersComments=data.usersComments;
                if(data&&data.classes)
                    this.classes=data.classes;
                if(data&&data.classesComments)
                    this.classesComments=data.classesComments;
                this.serveurReponse=true;
            }).catch(err=>{
                    this.getPostsFromLocal();
                    this.serveurReponse=true;
            });
        });
    }

    //Get Posts form local storage
    getPostsFromLocal(){
        this.storage.get('posts').then(data=>{


            if(data&&data.posts){
                this.posts=data.posts.post;
                this.commentaires=data.posts.commentaires;
            }
            if(data&&data.matieres)
                this.matieres=data.matieres;
            if(data&&data.users)
                this.users=data.users;
            if(data&&data.usersComments)
                this.usersComments=data.usersComments;
            if(data&&data.classes)
                this.classes=data.classes;

            if(data&&data.classesComments)
                this.classesComments=data.classesComments;

            this.serveurReponse=true;

        });
    }



    //Get the list of post with their comments
    listPost(refresher,refresh){


        if(refresh){
            this.getPostsFromServer();
        }else{
            this.getPostsFromLocal();
            if(this.posts&&this.posts.length===0){
                this.getPostsFromServer();
            }
        }

        if(refresher!=0){
            refresher.complete();
        }
    }


    //Get the  matiere of the post
    getCourse(matiereId:number,matieres:any){

        let matiere="";

        if(matieres){
            Object.keys(matieres).map(function(key) {
                if(matieres[key].id===matiereId){
                    matiere =matieres[key].NOM_MATIERE;
                }
            });
        }
        return matiere;
    }


    //Get the username
    getUserInfos(userId:number,classe:any){

        let user:User=new User();

        if(classe){

            Object.keys(classe).map(function(key) {
                if(classe[key].id===userId){
                    user=classe[key];
                }
            });
        }

        return user;
    }


    //Format the post Date
    formatDatePost(date:string) :string{

        let dateFormat="";
        let currentDate=new Date();

        if(date){
            let postDate=new Date(date);
            if(currentDate.getDay()===postDate.getDay()){
                this.translate.get(['TODAY']).subscribe(value=>{
                    dateFormat=value.TODAY;
                });
            }else{
                dateFormat=date;
            }
        }

        return dateFormat;
    }

    //Show the detail comments of a Post
    showpostComments(post:any){
        this.navCtrl.push(ForumCommentsPage,{
            posts:post,
            commentaires:this.commentaires[post.id],
            classes:this.classes[post.user_id],
            classesComments:this.classesComments[post.id],
            matieres:this.getCourse(post.matiere_id,this.matieres),
            users: this.getUserInfos(post.user_id,this.users),
            usersComments:this.usersComments[post.id]
        });

        // add a view to the post
        this.storage.get('token').then(token=>{
            let data={"id_post":post.id};
            this.forums.viewPost(data,token)
                .then(data=>{

                    if(data&&data.reponse==="success"){
                        this.listPost(0,true);
                    }
                }).catch(err=>{});
        });

    }


    //Hide filter button and show the filter bar
    setFilter(){
        this.showFilter=true;
        this.content.resize();
    }

    //Hide the filter
    hideFilter(){
        this.showFilter=false;
        this.translate.get(['FORUM_FILTER_MESSAGE']).subscribe(value=>{
            this.testRadioResult=value.FORUM_FILTER_MESSAGE;
        });

        this.getPostsFromLocal();
        this.content.resize();
    }


    //Get the post list of a course
    postByCourse(matiereId:number){

        let dataPost=[];

        if(this.posts&&this.posts.length>0){

            for(let i=0;i<this.posts.length;i++) {

                if(this.posts[i].matiere_id===matiereId){
                    dataPost.push(this.posts[i]);
                }
            }

            this.posts=dataPost;
            this.content.resize();
        }
    }


    //Format the list of courses of the forum
    radioList(){

        let alert = this.alertCtrl.create();

        this.translate.get(['FORUM_SELECT_TEXT']).subscribe(value=>{
            alert.setTitle(value.FORUM_SELECT_TEXT);
        });


          let matieres,selectMatiere=[];
          matieres=this.api.objetToArray(this.matieres);
         if(matieres&&matieres.length>0) {
             selectMatiere.push(matieres[0]);
             for (let i = 0; i < matieres.length; i++) {

                 if (matieres[i]&&!this.api.isSubjectsIn(selectMatiere,matieres[i].id)) {
                     selectMatiere.push(matieres[i]);
                     alert.addInput({
                         type: 'radio',
                         label: matieres[i].NOM_MATIERE.toUpperCase(),
                         value: matieres[i],
                         checked: false
                     });
                 }
             }
         }

        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: data => {

                if(data){
                    this.testRadioResult=data.NOM_MATIERE;
                    this.postByCourse(data.id);
                }
                this.showFilter=false;
                this.content.resize();
            }
        });

        alert.present();
    }

    filterItems(searchTerm){

        return this.posts.filter((item) => {
            return item.summary.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });

    }

    //show the loading
    onSearchInput(){
        this.searching = true;
    }


    //Get the list which match the pattern
    getItems(){
        this.posts=this.filterItems(this.pattern);
        this.content.resize();
    }


    //Delete a post
    disabledPost(post,index){

        this.storage.get('token').then(token=>{

            this.storage.get('user').then(user=>{

                this.loader=this.loadingCtrl.create({
                    content:'',
                    dismissOnPageChange:true,
                    duration: 60000
                });

                this.loader.present();

                let param={"id_post":post.id,"user_id":user.id};
                this.forums.disablePost(param,token)
                    .then(data=>{

                        this.loader.dismiss();
                        if(data&&data.reponse=="success"){
                            this.posts.splice(1,index);
                            this.content.resize();
                        }

                    }).catch(err=>{

                        this.loader.dismiss();
                        if(err&&err.reponse=="failure"){
                            this.translate.get(['FORUM_POST_DELETE_FAILED']).subscribe(value=>{
                                this.userService.toastAlert(value.FORUM_POST_DELETE_FAILED);
                            });
                        }

                    });

            });
        });

    }



    //Note a post
    notePost(post,note){

        this.storage.get('token').then(token=>{

            this.storage.get('user').then(user=>{

                this.loader=this.loadingCtrl.create({
                    content:'',
                    dismissOnPageChange:true,
                    duration: 60000
                });
                this.loader.present();

                let param={"id_post":post.id,"user_id":user.id,note};
                this.forums.notePost(param,token)
                    .then(data=>{

                        this.loader.dismiss();
                        if(data&&data.reponse=="success"){

                            if(data.posts&&data.posts.length>0){
                                this.posts=data.posts;
                            }
                        }

                    }).catch(err=>{

                        this.loader.dismiss();
                        if(err&&err.reponse=="failure"){
                            this.translate.get(['FORUM_POST_NOTE_FAILED']).subscribe(value=>{
                                this.userService.toastAlert(value.FORUM_POST_DELETE_FAILED);
                            });
                        }

                    });

            });
        });

    }

}
