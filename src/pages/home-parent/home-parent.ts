import {Component, ViewChild} from '@angular/core';
import { Storage } from "@ionic/storage";
import {Content, NavController, NavParams} from 'ionic-angular';

import { ManageAccountPage } from "../manage-account/manage-account";
import { MyCoursePage } from "../my-course/my-course";
import { ForumPage } from "../forum/forum";
import { ProfilPage } from "../profil/profil";
import { AlertSettingPage  } from "../alert-setting/alert-setting";
import {TeacherStudentsPage} from "../teacher-students/teacher-students";

/**
 * Generated class for the HomeParentPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-home-parent',
    templateUrl: 'home-parent.html',
})
export class HomeParentPage {

    @ViewChild(Content) content : Content;

    logo:string;
    user:any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public storage:Storage
    ) {

        this.logo="assets/img/MooExams_pre.png";
    }

    //Get user informations
    getUserInfos(){
        this.storage.get('user').then(user=>{
            this.user=user;
        });
    }

    ionViewDidLoad() {
        this.getUserInfos();
        this.content.resize();
    }

    //Manage account
    accounts(){

        //this.navCtrl.push(ManageAccountPage);

        this.storage.get('user').then(user=> {

            if(user&&user.role_id==5){
                this.navCtrl.push(ManageAccountPage);
            }else{
                this.navCtrl.push(TeacherStudentsPage);
            }

        });
    }

    //View notes
    myCourse(){
        this.navCtrl.push(MyCoursePage);
    }

    //Show the forum list
    forum(){
        this.navCtrl.push(ForumPage);
    }


    //go to the user profile
    goProfile(){
        this.navCtrl.push(ProfilPage);
    }


    //Set the period of email reception
    alertSetting(){
        this.navCtrl.push(AlertSettingPage);
    }

}
