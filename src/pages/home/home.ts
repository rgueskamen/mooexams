import { ConcoursDashPage } from './../concours-dash/concours-dash';
import {Component, ViewChild} from '@angular/core';
import {Content, Events, NavController} from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { ParamExamPage } from '../param-exam/param-exam';
import { ParamRevisionPage } from  '../param-revision/param-revision';
import { LeaderBoardPage } from '../leader-board/leader-board';
import { TimeTablePage } from '../time-table/time-table';
import { ForumPage } from "../forum/forum";
import { ProfilPage } from '../profil/profil';
import { ChallengePage} from "../challenge/challenge";
import { ApiProvider} from "../../providers/api/api";
import {UserProvider} from "../../providers/user/user";
import {ChallengeProvider} from "../../providers/challenge/challenge";



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

    @ViewChild(Content) content : Content;

  logo:string;
  menuModal:boolean;
  user : any;
  nbreInvitations:number;
  interval:any;

  constructor(
      public navCtrl: NavController,
      public storage:Storage,
      public api:ApiProvider,
      public events:Events,
      public userService:UserProvider,
      public challenge:ChallengeProvider
  ) {

    this.logo="assets/img/MooExams_pre.png";
    this.menuModal=false;
      this.interval=null;

    this.storage.get('user').then(user=>{
        if(user){
            this.user=user.username;
        }else{
            this.user="Fan";
        }
    });



      // Update  the notifications badge
      events.subscribe('user:badge', (notifications, time) => {
         this.nbreInvitations = notifications.length + time.length;
      });


      // Update  the user informations
      events.subscribe('user:updated', (user, time) => {

        if(user){
          this.user=user.username;
        }else{
          this.user="Fan";
        }
      });

  }

    ionViewDidLoad() {
        this.setBadge();
        this.content.resize();
    }


    setBadge() {
       // Get the number of invitations everytime the page is load.
       if (navigator.onLine) {
           // we check if we have internet
           this.api.updateBadge().then(nombre => {
               this.nbreInvitations = nombre;
           }, nombre => {
               this.nbreInvitations = nombre;
           });
       }
   }




    //Go to the training setting page
    exam(){
        this.navCtrl.push(ParamExamPage);
    }

    //Go to the timetable page
    timeTable(){
        this.navCtrl.push(TimeTablePage);
    }

    //Go to exam resume page
    training(){

        this.navCtrl.push(ParamRevisionPage);
    }

    //Go to the leaderBoard page.
    leaderBoard(){
        this.navCtrl.push(LeaderBoardPage);
    }

    forum(){
        this.navCtrl.push(ForumPage);
    }

    //Go to your profile

    goProfile(){
       this.navCtrl.push(ProfilPage);
    }

    //Go to the challenge
    challenges(){
        this.navCtrl.push(ChallengePage);
    }


    //Go to concours
    jeuconcours(){
        this.navCtrl.push(ConcoursDashPage);
    }

}
