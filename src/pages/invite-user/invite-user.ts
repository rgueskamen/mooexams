import { Component } from '@angular/core';
import {LoadingController, ModalController, NavController, NavParams} from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { AddUserPage } from "../add-user/add-user";


import { UserProvider } from "../../providers/user/user";

import { TranslateService } from '@ngx-translate/core';
import { ProfilPage} from "../profil/profil";
import { ApiProvider} from "../../providers/api/api";
import { User} from "../../models/user";
import {ModalMessagePage} from "../modal-message/modal-message";
/**
 * Generated class for the InviteUserPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-invite-user',
    templateUrl: 'invite-user.html',
})
export class InviteUserPage {

    logo:string;
    students: any;
    classes:any;
    usersSchool:any;
    pattern:string;
    shouldShowCancel:boolean;
    studentSelect:any;
    classeSelect:any;
    schoolSelect:any;
    showProfil:boolean;
    currentUser:any;
    serverAnswer:boolean;
    showModal:boolean;
    message:string;
    userRole:Array<User>;
    loader:any;
    serveurReponse:boolean;
    matiere:any;


    constructor(
        public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public navParams: NavParams,
        public user:UserProvider,
        public storage:Storage,
        public translate:TranslateService,
        public api:ApiProvider,
        public modalCtrl:ModalController

    ) {

        this.logo="assets/img/MooExams_pre.png";
        this.students=[];
        this.classes={};
        this.usersSchool={};
        this.shouldShowCancel=true;
        this.studentSelect={};
        this.classeSelect={};
        this.schoolSelect={};
        this.matiere=this.navParams.get('matiere');
        this.showProfil=false;
        this.serverAnswer=false;
        this.showModal=false;
        this.userRole=this.navParams.get('users');
        this.serveurReponse=true;

        this.storage.get('user').then(user=>{
            this.currentUser=user;
        });
    }


    ionViewDidLoad() {

    }

    //Show user profile
    addUser(){
        this.storage.get('user').then(user=>{
            this.navCtrl.push(AddUserPage,{user:user,matiere:this.matiere});
        });
    }


    //Get the user by tag
    getUser(pattern:string){

        let data={"pattern":pattern};
        this.serveurReponse=false;
        this.showProfil=false;
        this.storage.get('token').then(token=>{
            this.user.searchStudent(data,token)
                .subscribe(data=>{

                    this.serveurReponse=true;
                    if(data&&data.reponse==="success"){

                        //Filtre uniquement les users qui ne sont pas dans sa liste.
                        this.students=this.api.userFilter(data.students,this.userRole);
                        this.classes=data.classes;
                        this.usersSchool=data.userClasse;
                    }

                    this.serverAnswer=true;

                },err=>{
                    this.serveurReponse=true;
                    this.serverAnswer=true;
                });
        });

    }


    //Find a substring and put it to bold
    match(pattern:string,username:string) : string{

        let chaine = username;
        if(pattern&&chaine){
            return chaine.replace(pattern,'<b>'+pattern+'</b>');
        }else{
            return chaine;
        }
    }


    //Select the user to invite
    invite(student:any,classe:any,school:any){
        this.studentSelect=student;
        this.classeSelect=classe;
        this.schoolSelect=school;
        this.showProfil=true;
    }


    //Event emit when a user enter a pattern
    onInput(event){
        if(event){
            this.getUser(this.pattern);
        }
    }


    //Event emit when the user cancel the search
    onCancel(event){
        if(event){
            this.students=[];
            this.serverAnswer=false;
        }
    }

    //Event emit when the the clear button is presssed
    onClear($event){
        if(event){
            this.students=[];
            this.serverAnswer=false;
        }
    }


    //Invite a new student
    addStudent(studentData:any){

        let message='';

        switch (studentData.language){
            case 'fr':
                message=`<b>${studentData.prenom} ${studentData.nom}</b> votre ami(e) <b>${this.currentUser.prenom} ${this.currentUser.nom}</b> souhaite vous suivre. Veuillez accepter sa demande si vous êtes d'accord.`;
                break;
            case 'en':
                message=`<b>${studentData.prenom} ${studentData.nom}</b> your friend <b>${this.currentUser.prenom} ${this.currentUser.nom}</b> want to follow you. Please accept the request if you agree.`;
                break;

            default :
                message=`<b>${studentData.prenom} ${studentData.nom}</b> votre ami(e)  <b> ${this.currentUser.prenom} ${this.currentUser.nom}</b> souhaite vous suivre. Veuillez accepter sa demande si vous êtes d'accord.`;
                break;
        }

        let param={

            "status":0,
            "message":message,
            "type":"friend",
            "sender_id": this.currentUser.id,
            "user_id":studentData.id,
            "active":1,
            "matiere_id":"null"
        };


        this.loader = this.loadingCtrl.create({
            content: "",
            dismissOnPageChange:true,
            duration: 60000
        });


        this.loader.present();

        this.storage.get('token').then(token=>{

            this.storage.get('user').then(user=>{

                if(user) {
                    switch (user.role_id) {
                        case 4:
                            param.type = "friend";
                            break;
                        case 5 :
                            param.type = "parent";
                            break;
                        case  6 :
                            param.type = "teacher";
                            param.matiere_id=this.matiere.id;
                            break;
                        default:
                            param.type = "friend";
                            break;
                    }
                }

                    this.user.sendInvitation(param,token)
                        .then(res=>{

                        this.loader.dismiss();
                        this.serverAnswer=false;
                        this.students=[];
                        this.pattern="";
                        this.showProfil=false;
                        if(res&&res.reponse==="success"){
                            this.translate.get(['MESSAGE_INVITATION_SEND']).subscribe(value=>{
                                this.user.toastAlert(value.MESSAGE_INVITATION_SEND);
                                this.message=value.MESSAGE_INVITATION_SEND;
                                this.openMessage(this.message);
                            });

                        }else if(res&&res.reponse==="failure"){

                            this.showModal=true;
                            this.translate.get(['MESSAGE_INVITATION_FAILED']).subscribe(value=>{
                                this.user.toastAlert(value.MESSAGE_INVITATION_FAILED);
                                this.message=value.MESSAGE_INVITATION_FAILED;
                                this.openMessage(this.message);
                            });

                        }

                    }).catch(err=>{
                        this.loader.dismiss();
                    });
                });

        });

    }


    //Show the level of the user
    openMessage(message){

        let modal = this.modalCtrl.create(ModalMessagePage,{message:message});

        modal.onDidDismiss(data => {
        });
        modal.present();
    }


    //Go to the user Profile
    gotoProfile(){
        this.navCtrl.push(ProfilPage);
    }

}
