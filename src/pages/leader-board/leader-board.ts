import { Component,ViewChild} from '@angular/core';
import { NavController, NavParams ,Platform} from 'ionic-angular';
import { ProfilPage } from "../profil/profil";
import { ScoreProvider } from "../../providers/score/score";
import { Storage} from "@ionic/storage";
import { Resultat } from '../../models/resultat';
import { Matiere } from '../../models/matiere';
import {TranslateService} from "@ngx-translate/core";

import chartJs from 'chart.js';

/**
 * Generated class for the LeaderBoardPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


interface BorderBackgrounds{
    border:string;
    background:string;
}


interface DataCharts{
    axis:Array<string>;
    ords:Array<string>;
    labels:Array<string>;
}


interface ResultatQuestionnaire{
    matiere:Matiere;
    nbrePoints_matiere:number;
    border:string;
    background:string,
    matrix:Array<Matiere[]>;
    bonne_reponse:number;
    mauvaise_reponse:number;
    points:number;
    label:string;
    cart:DataCharts;

}

@Component({
    selector: 'page-leader-board',
    templateUrl: 'leader-board.html',
})
export class LeaderBoardPage {

    @ViewChild('lineCanvas') lineCanvas;

    choix:string;
    mode:string;
    time:string;
    currentResult:any;
    currentMatiere:Matiere;
    borderAndBacground:Array<BorderBackgrounds>;
    allResult:any;
    allMatiere:any;
    months:any;
    days:any;
    years:any;
    lineChart: any;
    legende:string;
    serveurReponse:boolean;
    istablet:boolean;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public score:ScoreProvider,
        public storage:Storage,
        public translate:TranslateService,
        public plt: Platform

    ) {

        this.choix="matiere";
        this.mode="revision";
        this.time="week";
        this.currentResult=[];
        this.allResult=[];
        this.borderAndBacground=[];
        this.currentMatiere={id:0,NOM_MATIERE:""};
        this.allMatiere=[];
        this.getMonths();
        this.getDays();
        this.years=[];
        this.serveurReponse=false;
        this.translate.get(['RESULT_TEXTE_4']).subscribe(value=>{
            this.legende=value.RESULT_TEXTE_4;
            console.log("Legende");
            console.log(this.legende);
        });
        this.istablet=false;

        if (this.plt.is('tablet')) {
            this.istablet=true;
        }
    }

    ionViewDidLoad() {
        this.initBorderAndBackground();
        this.getResultData(0,true);

    }


    //Init background color and border of a line
    initBorderAndBackground(){
        this.borderAndBacground.push({border:"#e6194b",background:"rgba(230, 25, 75,0.1)"});//1
        this.borderAndBacground.push({border:"#3cb44b",background:"rgba(60, 180, 75,0.1)"});//2
        this.borderAndBacground.push({border:"#ffe119",background:"rgba(255, 225, 25,0.1)"});//3
        this.borderAndBacground.push({border:"#0082c8",background:"rgba(0, 130, 200,0.1)"});//4
        this.borderAndBacground.push({border:"#f58231",background:"rgba(245, 130, 48,0.1)"});//5
        this.borderAndBacground.push({border:"#911eb4",background:"rgba(145, 30, 180,0.1)"});//6
        this.borderAndBacground.push({border:"#46f0f0",background:"rgba(70, 240, 240,0.1)"});//7
        this.borderAndBacground.push({border:"#f032e6",background:"rgba(240, 50, 230,0.1)"});//8
        this.borderAndBacground.push({border:"#d2f53c",background:"rgba(210, 245, 60,0.1)"});//9
        this.borderAndBacground.push({border:"#fabebe",background:"rgba(250, 190, 190,0.1)"});//10
        this.borderAndBacground.push({border:"#008080",background:"rgba(0, 128, 128,0.1)"});//11
        this.borderAndBacground.push({border:"#e6beff",background:"rgba(230, 190, 255,0.1)"});//12
        this.borderAndBacground.push({border:"#aa6e28",background:"rgba(170, 110, 40,0.1)"});//13
        this.borderAndBacground.push({border:"#fffac8",background:"rgba(255, 250, 200,0.1)"});//14
        this.borderAndBacground.push({border:"#800000",background:"rgba(128, 0, 0,0.1)"});//15
        this.borderAndBacground.push({border:"#aaffc3",background:"rgba(170, 255, 195,0.1)"});//16
        this.borderAndBacground.push({border:"#808000",background:"rgba(128, 128, 0,0.1)"});//17
        this.borderAndBacground.push({border:"#ffd8b1",background:"rgba(255, 215, 180,0.1)"});//18
        this.borderAndBacground.push({border:"#000080",background:"rgba(0, 0, 128,0.1)"});//19
        this.borderAndBacground.push({border:"#808080",background:"rgba(128, 128, 128,0.1)"});//20
    }


    getChart(context, chartType, data, options?) {
        return new chartJs(context, {
            data,
            options,
            type: chartType,
        });
    }

    //get the active subject
    getActiveSubject(data:any,currentMatiere):ResultatQuestionnaire{
        let resulat:ResultatQuestionnaire,i=0,trouve=false;


        while(i<data.length&&!trouve){
            if(data[i]&&data[i].matiere.id==currentMatiere.id){
                resulat=data[i];
                trouve=true;
            }
            i++;
        }
        if(resulat)
            return resulat;
        else
            return {
                matiere:new Matiere(),
                nbrePoints_matiere:0,
                matrix:[[]],
                bonne_reponse:0,
                border:"",
                background:"",
                mauvaise_reponse:0,
                points:0,label:"",
                cart:{axis:[],ords:[],labels:[]}
            };
    }



    // Format les données pour afficher sur le diagramme
    getLineChart(dataResult) {

        let  labels=[],dataSets=[];

        for(let i=0;i<dataResult.length;i++) {


            if(i==0){
                labels= dataResult[i].cart.labels;
            }
            dataSets.push(
                {
                    label: this.legende,
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: this.borderAndBacground[i].background,
                    borderColor: this.borderAndBacground[i].border,
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(75,192,192,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 1,
                    pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 1,
                    pointRadius: 2,
                    pointHitRadius: 10,
                    data: dataResult[i].cart.axis,
                    spanGaps: false,
                });

        }

        const data={
            labels:labels,
            datasets:dataSets
        };
        if(this.lineCanvas)
            return this.getChart(this.lineCanvas.nativeElement, 'line', data);
        else
            return [];
    }




    //Go to the user Profile
    goProfile(){
        this.navCtrl.push(ProfilPage);
    }


    //Get the months format
    getMonths(){

        this.months=[];
        this.translate.get(['MONTH1','MONTH2','MONTH3','MONTH4','MONTH5','MONTH6','MONTH7','MONTH8','MONTH9','MONTH10','MONTH11','MONTH12'])
            .subscribe(months=>{
                this.months.push(months.MONTH1);
                this.months.push(months.MONTH2);
                this.months.push(months.MONTH3);
                this.months.push(months.MONTH4);
                this.months.push(months.MONTH5);
                this.months.push(months.MONTH6);
                this.months.push(months.MONTH7);
                this.months.push(months.MONTH8);
                this.months.push(months.MONTH9);
                this.months.push(months.MONTH10);
                this.months.push(months.MONTH11);
                this.months.push(months.MONTH12);
            });
    }

    //Get the days format
    getDays(){
        this.days=[];
        this.translate.get(['SHORTDAY1','SHORTDAY2','SHORTDAY3','SHORTDAY4','SHORTDAY5','SHORTDAY6','SHORTDAY7'])
            .subscribe(days=>{
                this.days.push(days.SHORTDAY1);
                this.days.push(days.SHORTDAY2);
                this.days.push(days.SHORTDAY3);
                this.days.push(days.SHORTDAY4);
                this.days.push(days.SHORTDAY5);
                this.days.push(days.SHORTDAY6);
                this.days.push(days.SHORTDAY7);
            });

    }



    //select the tab to show
    myChoice(choice:string){
        this.choix=choice;
        this.getResultData(0,false);
    }

    //Change the mode revision or exam
    switchMode(mode:string){
        this.mode=mode;
        this.getResultData(0,false);
    }

    //set the periode
    setTime(time:string){
        this.time=time;
        this.getResultData(0,false);
    }

    //Set the current subjetc
    setCurrentSubject(matiere:Matiere){
        this.currentMatiere=matiere;
        this.getResultData(0,false);
    }

    getResultData(refresher,refresh){

       if(refresh){
            this.getAllHistorique();
        }else{
            this.getHistoryLocal();
            if(this.allResult&&this.allResult.length==0){
                this.getAllHistorique();
            }
        }

        if(refresher!=0)
            refresher.complete();
    }


    //sum the element of an array
    sumArrayElemts(data:Array<number>):number{
        let somme=0;
        for(let i=0;i< data.length;i++){
            somme+=data[i];
        }
        return somme;
    }


    // Convert an array to object
    objetToArray(data:any):Array<Matiere>{

        let matiere:Matiere[] = [];

        if(data){
            Object.keys(data).map(function(key) {
                matiere.push(data[key]);
            });
        }

        return matiere;
    }

    //Get the history from local
    getHistoryLocal(){

      console.log("je prends les données en local");

        this.storage.get('user-results').then(data=>{
            if(data){
                this.allMatiere=this.objetToArray(data.matieres);

                this.allResult=data.resultats;



                if(this.currentMatiere&&this.currentMatiere.NOM_MATIERE==""){
                    this.currentMatiere=this.allMatiere[0];
                }

                this.years=[];

                if(this.allResult&&this.allResult.length>0) {
                    this.years.push(this.allResult[0].annee.split("-")[0]);
                    this.years.push(this.allResult[0].annee.split("-")[1]);
                }

                if(this.mode=='examen' && this.choix=='all'){
                    this.getHistoryExamAllSubjects();
                }else if(this.mode=='examen' && this.choix=='matiere'){
                    this.getHistoryExamBySubjects(this.currentMatiere);
                }else if(this.mode=='revision' && this.choix=='matiere'){
                    this.getHistoryTrainingBySubjects(this.currentMatiere);
                }else if(this.mode=='revision' && this.choix=='all'){
                    this.getHistoryTrainingAllSubjects();
                }
            }
            this.serveurReponse=true;
        });

    }


    //Get all history From the server
    getAllHistorique(){

        this.storage.get('token').then(token=>{

            this.storage.get('user').then(user=>{
                this.storage.get('user-school').then(school=>{

                    let param={"id_user":user.id,"annee":school.annee};

                    this.years=[];
                    this.years.push(param.annee.split("-")[0]);
                    this.years.push(param.annee.split("-")[1]);

                    console.log("Les années");
                    console.log(this.years);

                    this.score.getAllHistory(param,token)
                        .then(data=>{

                            this.serveurReponse=true;
                            if(data&&data.reponse=="success"){
                                this.allMatiere=this.objetToArray(data.matieres);
                                this.allResult=data.resultats;

                                console.log("Données à Filtrer");
                                console.log(this.allResult);

                                console.log("La liste des matières");
                                console.log(this.allMatiere);

                                console.log("Matière courante");
                                console.log(this.currentMatiere);

                                if(this.currentMatiere&&this.currentMatiere.NOM_MATIERE==""){
                                    this.currentMatiere=this.allMatiere[0];
                                    console.log("Matière courante");
                                    console.log(this.currentMatiere);
                                }

                                if(this.mode=='examen' && this.choix=='all'){
                                    this.getHistoryExamAllSubjects();
                                }else if(this.mode=='examen' && this.choix=='matiere'){
                                    this.getHistoryExamBySubjects(this.currentMatiere);
                                }else if(this.mode=='revision' && this.choix=='matiere'){
                                    this.getHistoryTrainingBySubjects(this.currentMatiere);
                                }else if(this.mode=='revision' && this.choix=='all'){
                                    this.getHistoryTrainingAllSubjects();
                                    console.log("Choix par Defaut");
                                    console.log(this.mode);
                                    console.log(this.choix);
                                }
                            }

                        }).catch(err=>{
                            this.serveurReponse=true;
                            this.getHistoryLocal();
                        });
                });
            });
        });

    }


    //Get the history of revision by subjects
    getHistoryTrainingBySubjects(matiere:Matiere){
      console.log("La Matiere");
      console.log(matiere);

      console.log("Toutes les Matieres");
      console.log(this.allMatiere);

      console.log("Toutes les resultats");
      console.log(this.allResult);

        this.currentResult=this.getResultCurrentSubject(this.allMatiere,this.allResult,1,matiere);
        setTimeout(() => {
            this.lineChart = this.getLineChart(this.currentResult);

        console.log("Diagramme");
        console.log(this.lineChart);
        },250);

        console.log(" Resultat Final par matiere storage");
        console.log(this.currentResult);

    }

    //Get the history of exam by subjects
    getHistoryExamBySubjects(matiere:Matiere){
        this.currentResult=this.getResultCurrentSubject(this.allMatiere,this.allResult,2,matiere);
        setTimeout(() => {
            this.lineChart = this.getLineChart(this.currentResult);
        },250);

        console.log(this.currentResult);
    }


    //Get the history of revision all subjects
    getHistoryTrainingAllSubjects(){

        this.currentResult=this.getResultsAll(this.allMatiere,this.allResult,1);
        console.log("Resulats Final");
        console.log(this.currentResult);
        setTimeout(() => {
            this.lineChart = this.getLineChart(this.currentResult);
            console.log("Diagramme");
            console.log(this.lineChart);
        },250);

    }


    //Get the history of exam all subjects
    getHistoryExamAllSubjects(){
        this.currentResult=this.getResultsAll(this.allMatiere,this.allResult,2);
        setTimeout(() => {
            this.lineChart = this.getLineChart(this.currentResult);
        }, 250);

        console.log(this.currentResult);
    }



    //Get good answers of the result
    goodAnswersResult(resultat:Array<Resultat>,idmatiere:number):number{
        let result=0;

        if(resultat.length>0){
            for(let i=0;i<resultat.length;i++){
                if(resultat[i]&&resultat[i].id_matiere==idmatiere){
                    result+=resultat[i].nbre_bonne_reponse;result+=resultat[i].nbre_bonne_reponse;
                }

            }
        }

        return result;
    }

    //Get good wrong of the result
    wrongsAnswersResult(resultat:Array<Resultat>,idmatiere:number):number{
        let result=0;

        if(resultat.length>0){
            for(let i=0;i<resultat.length;i++){
                if(resultat[i]&&resultat[i].id_matiere==idmatiere) {
                    result += resultat[i].nbre_mauvaise_reponse;
                }
            }
        }

        return result;
    }

    //Get points of the result
    pointsAnswersResult(resultat:Array<Resultat>,idmatiere:number):number{
        let result=0;

        if(resultat.length>0){
            for(let i=0;i<resultat.length;i++){
                if(resultat[i]&&resultat[i].id_matiere==idmatiere) {
                    result += resultat[i].nbrePoints;
                }
            }
        }

        return result;
    }


    // Verify that 2 days are in the same week
    isDaysHasSameWeek(currentDate:Date,lastDate:Date):boolean{

        let answer=false;

        let time1=currentDate.getTime();
        let time2=lastDate.getTime();
        let day1=currentDate.getDay();
        let day2=lastDate.getDay();
        let weekTime=7*24*3600000;
        let time=time1-time2;

        if(time<=weekTime&&day2<=day1){
            answer=true;
        }

        return answer;

    }

    //week filter
    filterResulByWeek(data:Array<Resultat>):Array<Resultat>{
        let resultats : Array<Resultat>=[] ;
        let currentDate =new Date();
        let dateInter:Date;

        if(data.length>0){
            for (let i=0;i<data.length;i++){
                dateInter=new Date(data[i].created_at);
                if(dateInter.getFullYear()==currentDate.getFullYear()&&dateInter.getMonth()==currentDate.getMonth()&&this.isDaysHasSameWeek(currentDate,dateInter)){
                    resultats.push(data[i]);
                }
            }
        }
        return resultats;

    }


    //month filter
    filterResulByMonth(data:Array<Resultat>):Array<Resultat>{
        let resultats : Array<Resultat>=[] ;
        let currentDate =new Date();
        let dateInter:Date;
        if(data.length>0){
            for (let i=0;i<data.length;i++){
                dateInter=new Date(data[i].created_at);
                if(dateInter.getFullYear()==currentDate.getFullYear()&&dateInter.getMonth()<=currentDate.getMonth()){
                    resultats.push(data[i]);
                }
            }
        }
        return resultats;
    }

    //Year filter
    filterResulByYear(data:Array<Resultat>):Array<Resultat>{
        let resultats : Array<Resultat>=[] ;
        let currentDate =new Date();
        let dateInter:Date;
        if(data.length>0){
            for (let i=0;i<data.length;i++){
                dateInter=new Date(data[i].created_at);
                if(dateInter.getFullYear()==(currentDate.getFullYear()-1)||dateInter.getFullYear()==currentDate.getFullYear()||dateInter.getFullYear()==(currentDate.getFullYear()+1)){
                    resultats.push(data[i]);
                }
            }
        }
        return resultats;

    }


    //select which time filter to call
    timeFilter(periode:string,data:Array<Resultat>):Array<Resultat>{

        switch (periode){
            case "week":
                return this.filterResulByWeek(data);
            case "month":
                return this.filterResulByMonth(data);
            case "year":
                return this.filterResulByYear(data);
            default:
                return this.filterResulByWeek(data);

        }
    }



    //Get all the result of a mode
    filterMode(data:Array<Resultat>,mode:number):Array<Resultat>{

        let resultats : Array<Resultat>=[] ;

        if(data.length>0){
            for (let i=0;i<data.length;i++){
                if(data[i]&&data[i].mode==mode){
                    resultats.push(data[i]);
                }
            }
        }

        return resultats;
    }


    //Get a subject in a list of subject
    getMatiere(matieres:Array<Matiere>,id:number):Matiere{
        let matiere=new Matiere();

        let isIn=false,i=0;
        if(matieres.length>0) {
            while (!isIn && i < matieres.length) {
                if (matieres[i]&&matieres[i].id == id) {
                    isIn = true;
                    matiere = matieres[i];
                }
                i++;
            }
        }

        return matiere;
    }


    //Verify it a subjets is  in the array
    isSubjectsIn(matieres:Array<Matiere>,id:number):boolean{
        let isIn=false,i=0;
        if(matieres.length>0) {
            while (!isIn && i < matieres.length) {

                if (matieres[i]&&matieres[i].id == id) isIn = true;
                i++;
            }
        }
        return isIn;
    }

    //Get all subjects of the history result
    getAllSubjects(matieres:Array<Matiere>,resultat:Array<Resultat>):Array<Matiere>{

        let subjects : Array<Matiere>=[];
        let matiere:Matiere;

        if(resultat.length>0){

            for(let i=0;i<resultat.length;i++){
                if(resultat[i]) {
                    matiere = this.getMatiere(matieres, resultat[i].id_matiere);
                    if (!this.isSubjectsIn(subjects, matiere.id)) {
                        subjects.push(matiere);
                    }
                }
            }

        }


        return subjects;
    }


    //Get the the results of all Sybjets by mode
    getResultsAll(matieres:Array<Matiere>,resultat:Array<Resultat>,mode:number):Array<ResultatQuestionnaire>{

        let bonne_reponse=0,mauvaise_reponse=0,nbrePoints=0;
        let resultatFinal:Array<ResultatQuestionnaire>=[];


        let resultMode=this.filterMode(resultat,mode);
        console.log("Tous les resultats du mode : "+mode);
        console.log(resultMode);

        let resultatTime=this.timeFilter(this.time,resultMode);
        console.log("Tous les resultats du mode : "+mode+"de la periode : "+this.time);
        console.log(resultatTime);
        let resultSubjects=this.getAllSubjects(matieres,resultMode);

        console.log("Tous les resultats du mode : "+mode+"de la periode : "+this.time+"et lecons");
        console.log(matieres);
        console.log(resultSubjects);


        if(resultSubjects&&resultSubjects.length>0&&resultatTime&&resultatTime.length>0){
            for(let i=0;i<resultSubjects.length;i++){
                if(resultSubjects[i]) {
                    bonne_reponse += this.goodAnswersResult(resultatTime, resultSubjects[i].id);
                    mauvaise_reponse += this.wrongsAnswersResult(resultatTime, resultSubjects[i].id);
                    nbrePoints += this.pointsAnswersResult(resultatTime, resultSubjects[i].id);
                }
            }

            for(let i=0;i<resultSubjects.length;i++){
                if(resultSubjects[i]) {
                    resultatFinal.push(
                        {
                            matiere: resultSubjects[i],
                            matrix: this.formatDataForSubjectsToMatrix(resultSubjects),
                            nbrePoints_matiere: this.pointsAnswersResult(resultatTime, resultSubjects[i].id),
                            border: this.borderAndBacground[i].border,
                            background: this.borderAndBacground[i].background,
                            bonne_reponse: bonne_reponse,
                            mauvaise_reponse: mauvaise_reponse,
                            points: nbrePoints,
                            label: resultSubjects[i].NOM_MATIERE?resultSubjects[i].NOM_MATIERE.toLocaleLowerCase():'',
                            cart: this.selectTheTimeFormat(resultatTime, resultSubjects[i], this.time)
                        });
                }
            }
        }


        console.log("Resultat Final du mode: "+mode);
        console.log(resultatFinal);

        return resultatFinal;
    }






    //Get the the results of current subjects by mode
    getResultCurrentSubject(matieres:Array<Matiere>,resultat:Array<Resultat>,mode:number,currentMatiere:Matiere):Array<ResultatQuestionnaire>{

        let resultatFinal:Array<ResultatQuestionnaire>;
        let resultMode=this.filterMode(resultat,mode);
        console.log("Les resultats du mode : " +mode);
        console.log(resultMode);
        let resultatTime=this.timeFilter(this.time,resultMode);
        console.log("Les resultats du mode : " +mode + "de la periode :"+this.time);
        console.log(resultatTime);

        console.log("Tous les sujets");
        let resultSubjects=this.getAllSubjects(matieres,resultatTime);
        console.log(resultSubjects);

        const TypeMatiere={
          id : 0 ,
          NOM_MATIERE :""
        };


        if(currentMatiere&&currentMatiere.NOM_MATIERE) {

            let bonne_reponse = this.goodAnswersResult(resultatTime, currentMatiere.id);
            let mauvaise_reponse = this.wrongsAnswersResult(resultatTime, currentMatiere.id);
            let nbrePoints = this.pointsAnswersResult(resultatTime, currentMatiere.id);

            resultatFinal = [{
                matiere: currentMatiere,
                nbrePoints_matiere:nbrePoints,
                border:this.borderAndBacground[0].border,
                background:this.borderAndBacground[0].background,
                matrix:this.formatDataForSubjectsToMatrix(resultSubjects),
                bonne_reponse: bonne_reponse,
                mauvaise_reponse: mauvaise_reponse,
                points: nbrePoints,
                label: currentMatiere.NOM_MATIERE?currentMatiere.NOM_MATIERE.toLocaleLowerCase():'',
                cart:this.selectTheTimeFormat(resultatTime,currentMatiere,this.time)
            }];
        }else{
            resultatFinal=[{
                matiere: TypeMatiere,
                nbrePoints_matiere:0,
                border:this.borderAndBacground[0].border,
                background:this.borderAndBacground[0].background,
                matrix:[[]],
                bonne_reponse: 0,
                mauvaise_reponse: 0,
                points: 0,
                label: "",
                cart:{axis:[],ords:[],labels:[]}
            }]
        }

        console.log("Resulat Final de la matiere");
        return resultatFinal;
    }

    //Format data for the week
    formatDataForWeek(resultat:Array<Resultat>,matiere:Matiere):DataCharts{
        let resultFormat:DataCharts;
        let x=[],y=[],labels=[];
        let timeInter : Date;
        let bonne_reponse=0,nombre_question=0;
        let resultatTime=this.timeFilter('week',resultat);

        if(resultatTime.length>0){
            for(let j=0;j<=6;j++) {
                bonne_reponse=0;
                nombre_question=0;
                for (let i = 0; i < resultatTime.length; i++) {
                    timeInter = new Date(resultatTime[i].created_at);
                    if(timeInter.getDay()==j&&resultatTime[i].id_matiere==matiere.id){
                        bonne_reponse=resultatTime[i].nbre_bonne_reponse;
                        nombre_question=resultatTime[i].nbre_bonne_reponse+resultatTime[i].nbre_mauvaise_reponse;

                    }
                }
                x.push(bonne_reponse);
                y.push(nombre_question);
                labels.push(this.days[j]);
            }

        }

        resultFormat={axis:x,ords:y,labels:labels};

        return resultFormat;
    }

    //Format data for the month
    formatDataForMonth(resultat:Array<Resultat>,matiere:Matiere):DataCharts{
        let resultFormat:DataCharts;
        let x=[],y=[],labels=[];
        let timeInter : Date;
        let bonne_reponse=0,nombre_question=0;
        let resultatTime=this.timeFilter('month',resultat);
        if(resultatTime.length>0){
            for(let j=0;j<=11;j++) {
                bonne_reponse=0;
                nombre_question=0;
                for (let i = 0; i < resultatTime.length; i++) {
                    timeInter = new Date(resultatTime[i].created_at);
                    if(timeInter.getMonth()==j&&resultatTime[i].id_matiere==matiere.id){
                        bonne_reponse=resultatTime[i].nbre_bonne_reponse;
                        nombre_question=resultatTime[i].nbre_bonne_reponse+resultatTime[i].nbre_mauvaise_reponse;
                    }
                }
                x.push(bonne_reponse);
                y.push(nombre_question);
                labels.push(this.months[j]);

            }

        }

        resultFormat={axis:x,ords:y,labels:labels};

        return resultFormat;
    }

    //Format data for the year
    formatDataForYear(resultat:Array<Resultat>,matiere:Matiere):DataCharts{
        let resultFormat:DataCharts;
        let x=[],y=[],labels=[];
        let timeInter : Date;
        let bonne_reponse=0,nombre_question=0;
        let resultatTime=this.timeFilter('year',resultat);
        if(resultatTime.length>0){
            for(let j=0;j<this.years.length;j++) {
                bonne_reponse=0;
                nombre_question=0;
                for (let i = 0; i < resultatTime.length; i++) {
                    timeInter = new Date(resultatTime[i].created_at);
                    if(String(timeInter.getFullYear())==String(this.years[j])&&resultatTime[i].id_matiere==matiere.id){
                        bonne_reponse=resultatTime[i].nbre_bonne_reponse;
                        nombre_question=resultatTime[i].nbre_bonne_reponse+resultatTime[i].nbre_mauvaise_reponse;

                    }
                }
                x.push(bonne_reponse);
                y.push(nombre_question);
                labels.push(this.years[j]);
            }

        }

        resultFormat={axis:x,ords:y,labels:labels};

        return resultFormat;
    }

    //Switch the format the time

    selectTheTimeFormat(resultat:Array<Resultat>,matiere:Matiere,time:string):DataCharts{

        switch (time){

            case "week":
                return this.formatDataForWeek(resultat,matiere);
            case "month":
                return this.formatDataForMonth(resultat,matiere);
            case "year":
                return this.formatDataForYear(resultat,matiere);
            default :
                return this.formatDataForWeek(resultat,matiere);
        }

    }


    //Format data for the week
    formatDataForSubjectsToMatrix(matieres:Array<Matiere>):Array<Matiere[]>{

        let nbreElements=matieres.length;
        // [{id:0,NOM_MATIERE:""}]
        let matrixMatiere:Array<Matiere[]>=[];
        let ligneMatrix:Array<Matiere>;
        let nbreLignes= Math.floor(nbreElements/3);
        let j;

        if(nbreElements>3){
            for(let i=0;i<nbreLignes;i++){
                ligneMatrix=[];
                j=0;
                while(j<3){

                    if(matieres[i*3+j]&&matieres[i*3+j].NOM_MATIERE!=""){
                        ligneMatrix.push(matieres[i*3+j]);
                    }
                    j++;
                }

                if(ligneMatrix.length>0){
                    matrixMatiere.push(ligneMatrix);
                }
            }
        }else{
            matrixMatiere.push(matieres);
        }


        return matrixMatiere;
    }
}
