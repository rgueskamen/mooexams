import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from "@ionic/storage";

import { LeconsProvider } from "../../providers/lecons/lecons";

import { Lessons } from "../../models/lessons";

import { CourseResumePage } from '../course-resume/course-resume';
import { HomePage } from "../home/home";

/**
 * Generated class for the LeconsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-lecons',
    templateUrl: 'lecons.html',
})
export class LeconsPage {

    lessons : Lessons[];
    course : any;
    nbreQuestion:number;
    serveurReponse:boolean;
    logo:string;


    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public storage : Storage,
        public lesson: LeconsProvider
    ) {

        this.lessons=this.navParams.get('lessons');
        this.course = this.navParams.get('subjects');
        this.nbreQuestion=0;
        this.serveurReponse=false;
        this.logo="assets/img/MooExams_pre.png";

    }

    ionViewDidLoad() {
        this.getLessons(0,false);
        this.autoRefresher();
    }


    //Format the data
    format(chaine:string){

        let ch=String(chaine);
        let resultat=ch.replace(/\\'/g,"'");
        resultat=resultat.replace(/\\"/g,'"');
        return resultat;
    }


    //Limt the description caracters
    limitChaine(chaine:string){
        
        let ch=String(chaine);
        let resultat=ch.replace(/\\'/g,"'");
        resultat=resultat.replace(/\\"/g,'"');

        return resultat.length>100?resultat.substring(0,100)+'...':resultat;
    }


    //Get the lessons from params
    getParamsFromPage(){
        this.lessons=this.navParams.get('lessons');
        this.course = this.navParams.get('subjects');
        this.serveurReponse=true;
    }


    //get the list of lessons from the server
    getLessonsFromServer(){

        if(this.course&&this.course.id) {

            this.storage.get('token').then(token=>{

                let data = {"id_matiere": this.course.id};
                this.lesson.lessons_with_nb_questions(data,token)
                    .then(data => {

                    this.serveurReponse=true;

                    if (data && data.rubriques && data.rubriques.length > 0) {
                        this.lessons = data.rubriques;
                        this.course = data.matiere;

                        let nb_questions = 0;

                        for (let rubric of data.rubriques) {
                            nb_questions += parseInt(rubric.nbre_question);
                        }
                        this.nbreQuestion=nb_questions;

                    }

                }).catch(err => {
                    this.serveurReponse=true;
                    this.getParamsFromPage();
                });
            });
        }
    }


    //Get the list of lessons of the current subjects.
    getLessons(refresher,refresh){

        if(refresh){
            this.getLessonsFromServer();
        }else{
            this.getParamsFromPage();
            if(this.lessons&&this.lessons.length==0){
                this.getLessonsFromServer();
            }
        }

        if(refresher)
            refresher.complete();
    }


    //Open the subjets page
    openResume(subject:any,lesson:any){
        this.navCtrl.push(CourseResumePage,{subject:subject,lessons:lesson});
    }

    //Auto-refresh the list of lessons
    autoRefresher(){
        setTimeout(()=>{
            this.getLessonsFromServer();
        },3000);
    }

    //Go to profil page
    gotoDash(){
        this.navCtrl.setRoot(HomePage);
    }

}
