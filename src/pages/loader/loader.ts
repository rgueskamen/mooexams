
import { ViewChild,Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { Slides } from 'ionic-angular';

import { LoginPage } from '../login/login';

import {TranslateService} from '@ngx-translate/core';


/**
 * Generated class for the LoaderPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@Component({
    selector: 'page-loader',
    templateUrl: 'loader.html'
})



export class LoaderPage {

    @ViewChild(Slides) slides: Slides;


    slideData :any[];
    logo:string;
    index:number;
    keys:string[];



    constructor(public navCtrl: NavController, public navParams: NavParams,public menuCtrl: MenuController,private translate: TranslateService) {


        this.logo="assets/img/MooExams.png";
        this.index=0;

        this.keys=['TUTORIAL_SLIDE1_TITLE','TUTORIAL_SLIDE1_DESCRIPTION','TUTORIAL_SLIDE2_TITLE','TUTORIAL_SLIDE2_DESCRIPTION'
            ,'TUTORIAL_SLIDE3_TITLE','TUTORIAL_SLIDE3_DESCRIPTION','TUTORIAL_SLIDE4_TITLE','TUTORIAL_SLIDE4_DESCRIPTION'];

        this.translate.get(this.keys).subscribe((value) => {

            this.slideData=[
                { title:value.TUTORIAL_SLIDE1_TITLE,
                    picture:"assets/img/tuto-image.png",
                    description:value.TUTORIAL_SLIDE1_DESCRIPTION
                },
                { title:value.TUTORIAL_SLIDE2_TITLE,
                    picture:"assets/img/revision.png",
                    description:value.TUTORIAL_SLIDE2_DESCRIPTION
                },
                { title:value.TUTORIAL_SLIDE3_TITLE,
                    picture:"assets/img/examen.png",
                    description:value.TUTORIAL_SLIDE3_DESCRIPTION
                },
                { title:value.TUTORIAL_SLIDE4_TITLE,
                    picture:"assets/img/resultat.png",
                    description:value.TUTORIAL_SLIDE4_DESCRIPTION
                }];
        });

    }

    ionViewDidLoad() {
        //Handle the page loading
        this.menuCtrl.close();
        this.menuCtrl.swipeEnable(false);
    }

    //Navigate to a slide
    goToSlide(index) {
        this.slides.slideTo(index,300);
    }

    updateIndex(){

        switch (this.slides.getActiveIndex()){
            case 1:
                this.index=0;
                break;
            case 2:
                this.index=1;
                break;
            case 3:
                this.index=2;
                break;
            case 4:
                this.index=3;
                break;
            default:
                this.index=0;
                break;
        }

        if(this.slides.isEnd()){
           this.pageLogin();
        }


    }

    pageLogin(){

       let  choix='Inscription';
        this.navCtrl.push(LoginPage,{option:choix});
    }

}
