import { ApiProvider } from './../../providers/api/api';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, LoadingController, ModalController,Events, NavParams, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { UserProvider } from '../../providers/user/user';

import { HomePage } from '../home/home';
import { TermesPage } from '../termes/termes';
import { ForgotPassPage } from '../forgot-pass/forgot-pass';
import { ModalClassesPage } from '../modal-classes/modal-classes';
import { ModalMatieresPage } from "../modal-matieres/modal-matieres";
import { ModalPaysPage } from "../modal-pays/modal-pays";
import { HomeParentPage } from "../home-parent/home-parent";
// import { OneSignal } from "@ionic-native/onesignal";



/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})

export class LoginPage {

    title:string;
    logo:string;
    choix:string;
    status:string[];
    annees:string[];
    dataUser:object;
    keys:string[];
    loadingMessage=[];
    loadingKeys=[];
    classe : string;
    course:string;
    showAnnee:boolean;
    isActive:boolean;
    countries:any;
    country:string;
    body:any;
    loginLoader:any;
    currentUserData:any;
    currentCountryId:number;


    constructor(
        public loadingCtrl: LoadingController,
        public navCtrl: NavController,
        public navParams: NavParams,
        public fb:FormBuilder,
        public storage:Storage,
        private translate : TranslateService,
        public user:UserProvider,
        public modalCtrl: ModalController,
        public platform:Platform,
       // public onesignal:OneSignal,
        public events: Events,
        public api : ApiProvider
    ) {

        this.isActive=false;
        this.country="";
        this.countries=[];
        this.logo="assets/img/MooExams.png";
        this.choix=this.navParams.get('option');
        this.title= this.choix;
        this.keys=["ENSEIGNANT", "PARENT","ELEVE"];
        this.loadingKeys=["LOADER_LOGIN","LOADER_REGISTRATION","LOADER_CODE","LOADER_RESET_PASS","LOADER_TEXT"];

        this.translate.get(this.keys).subscribe((value) => {
            this.status=[value.ENSEIGNANT,value.PARENT,value.ELEVE];
        });

        this.translate.get(this.loadingKeys).subscribe((value) => {
            this.loadingMessage=[value.LOADER_LOGIN,value.LOADER_REGISTRATION,value.LOADER_CODE,value.LOADER_RESET_PASS,value.LOADER_TEXT];
        });

        this.classe="";
        this.course="";
     
        this.showAnnee=false;
        this.currentCountryId=1;
        this.annees=[];

    }

    ionViewDidLoad() {

        this.setSchoolYear();
        this.getCurrentCountryId();
        this.initLogin();
        this.initRegistration();

    }


    getCurrentCountryId(){
        // Get current country data informations
         this.api.getCountryByIpAdress().then(country=>{
               // Get the list of countries registered in the database
               this.user.getCountries().then(data =>{
                this.currentCountryId= this.api.getCountryInfos(data.pays,country).id
           }).catch(err=>{
           });
       }).catch(err=>{
       });
   }


    //Create event to send user data
    createUser(user) {
        this.events.publish('user:created', user, Date.now());
    }


    public  loginForm = this.fb.group({
        username: ["", Validators.required],
        password: ["", Validators.required],
        token_notification: [""]
    });

    initLogin() {
        //Construction du controleur du formulaire de login
        this.loginForm = this.fb.group({
            username: ["", Validators.required],
            password: ["", Validators.required],
            token_notification: [""]
        });
      //  this.registerPush();
        this.setSchoolYear();
    }

    //Return form instance control fields for login
    get usernameLogin() { return this.loginForm.get('username'); }
    get passwordLogin() { return this.loginForm.get('password'); }



    //Construction du controleur du formulaire d'incription
    public registerForm = this.fb.group({

        roles: ["student", Validators.required],
        id_classe: ["null"],
        nom: ["",  Validators.compose([Validators.maxLength(50)])],
        prenom: ["",  Validators.compose([Validators.maxLength(50)])],
        language: ['fr'],
        pays_id:[this.currentCountryId],
        matiere: ["null"],
        id_matiere: ["null"],
        tel: ["", Validators.compose([Validators.required,Validators.pattern('^[0-9]{9,13}')])],
        email: ["", Validators.compose([Validators.pattern('^[a-zA-Z0-9_.]+@[a-zA-Z0-9_.]+\\.[a-zA-Z]{2,5}')])],
        username: ["", Validators.compose([Validators.required,Validators.minLength(4),Validators.maxLength(50)])],
        annee: ["null"],
        password: ["",Validators.compose([ Validators.required,Validators.minLength(6),Validators.maxLength(50)])],
        token_notification: [""],
        terms: [false, Validators.compose([Validators.required])]

    });

    initRegistration()  {
        this.setSchoolYear();
        this.registerForm = this.fb.group({
            roles: ["student", Validators.required],
            id_classe: ["null"],
            nom: ["",  Validators.compose([Validators.maxLength(50)])],
            prenom: ["",  Validators.compose([Validators.maxLength(50)])],
            language: ['fr'],
            pays_id:[this.currentCountryId],
            matiere: ["null"],
            id_matiere: ["null"],
            tel: ["", Validators.compose([Validators.required,Validators.pattern('^[0-9]{9,13}')])],
            email: ["", Validators.compose([Validators.pattern('^[a-zA-Z0-9_.]+@[a-zA-Z0-9_.]+\\.[a-zA-Z]{2,5}')])],
            username: ["", Validators.compose([Validators.required,Validators.minLength(4),Validators.maxLength(50)])],
            annee: [this.annees[0]],
            password: ["",Validators.compose([ Validators.required,Validators.minLength(4),Validators.maxLength(50)])],
            token_notification: [""],
            terms: [false, Validators.compose([Validators.required])]
        });
      // this.registerPush();    
    }

    initWithCurentData(data){


        if(data){

            this.setSchoolYear();
            this.registerForm = this.fb.group({
                roles: [data.roles, Validators.required],
                id_classe: [data.id_classe],
                nom: [data.nom,  Validators.compose([Validators.maxLength(50)])],
                prenom: [data.prenom,  Validators.compose([Validators.maxLength(50)])],
                language: [data.language],
                pays_id:[data.pays_id],
                matiere: [data.matiere],
                id_matiere: [data.id_matiere],
                tel: [data.tel, Validators.compose([Validators.required,Validators.pattern('^[0-9]{9,13}')])],
                email: [data.email, Validators.compose([Validators.pattern('^[a-zA-Z0-9_.]+@[a-zA-Z0-9_.]+\\.[a-zA-Z]{2,5}')])],
                username: [data.username, Validators.compose([Validators.required,Validators.minLength(4),Validators.maxLength(50)])],
                annee: [data.annee],
                password: [data.password,Validators.compose([ Validators.required,Validators.minLength(4),Validators.maxLength(50)])],
                token_notification: [data.token_notification],
                terms: [true, Validators.compose([Validators.required])]
            });

       //   this.registerPush();
           
        }else{
            this.initRegistration();
        }

    }


    //Return form instance control fields for registration
    get roles() { return this.registerForm.get('roles'); }
    get clas() { return this.registerForm.get('id_classe'); }
    get annee() { return this.registerForm.get('annee'); }
    get matiere() { return this.registerForm.get('matiere'); }
    get nom() { return this.registerForm.get('nom'); }
    get prenom() { return this.registerForm.get('prenom'); }
    get language(){ return this.registerForm.get('language'); }
    get pays(){return this.registerForm.get('pays_id'); }
    get tel() { return this.registerForm.get('tel'); }
    get email() { return this.registerForm.get('email'); }
    get username() { return this.registerForm.get('username'); }
    get password() { return this.registerForm.get('password'); }
    get terms() { return this.registerForm.get('terms'); }


    //Register a pushNotification
    registerPush() {
        this.onesignal.getIds().then( ids=> {
            if(ids&&ids.userId){
                this.storage.set('device_id',ids.userId);
                this.registerForm.controls['token_notification'].setValue(ids.userId);
                this.loginForm.controls['token_notification'].setValue(ids.userId);
            }
        });
    }


    //Go to login form
    GotoConnexion(choix:string){
        this.choix=choix;
        this.title=choix;
    }

    //Go to registration form
    GotoInscription(choix:string){
        this.choix=choix;
        this.title=choix;
    }

    //Show and hide the password
    showPass(){
        this.isActive=!this.isActive;
    }

    //show the current academic year
    setSchoolYear(){
       
        let date = new Date();
        let an = date.getFullYear();
        let annee1=(an-1)+"-"+an;
        let annee2=an+"-"+(an+1);
        this.annees.push(annee1);
        this.annees.push(annee2);
    };

    //Select your classe
    selectClass(){

        this.showAnnee=true;
        let modal = this.modalCtrl.create(ModalClassesPage);

        modal.onDidDismiss(data => {

            if(data){
                this.registerForm.controls['id_classe'].setValue(data.id);
                if(data.OPTION_CLASSE!='NONE'){
                    this.classe=data.NOM_CLASSE+" "+data.OPTION_CLASSE;
                }else{
                    this.classe=data.NOM_CLASSE;
                }
            }
        });

        modal.present();
    }

    //Select your class
    selectCourse(){

        let modal = this.modalCtrl.create(ModalMatieresPage);

        modal.onDidDismiss(data => {

            if(data){
                this.registerForm.controls['id_matiere'].setValue(data.id);
                this.registerForm.controls['matiere'].setValue(data.NOM_MATIERE);
                this.course=data.NOM_MATIERE;
            }

        });

        modal.present();
    }


    /**
     * Select the user countries
     */

    selectCountry(){

        let modal = this.modalCtrl.create(ModalPaysPage);

        modal.onDidDismiss(data => {
            if(data){
                this.registerForm.controls['pays_id'].setValue(data.id);
                this.country=data.nom;
            }
        });

        modal.present();
    }


    //



    //Show the app's terms and conditions
    showTermes(){

        if(this.registerForm.value.terms){
            this.navCtrl.push(TermesPage);
        }
    }






    //Login on the app
    login(data:any){

        //this.navCtrl.setRoot(HomePage);
        let param = {
            username_or_email:  data.username,
            password:  data.password,
            token_notification:data.token_notification
        };

        this.dataUser={user:param};

        this.loginLoader = this.loadingCtrl.create({
            content: '',
            dismissOnPageChange: true,
            duration: 60000
        });
        this.loginLoader.present();

        //Cette methode renvoie un objet de type Response
        this.user.login(this.dataUser).then(res=>{


            this.loginLoader.dismiss();

            if (res&&res.reponse==="success") {

                this.createUser(res.user);

                this.initLogin();
                //Get the user role
                switch (res.user.role_id){

                    case 4:
                        this.navCtrl.setRoot(HomePage);
                        break;

                    case 5:
                        this.navCtrl.setRoot(HomeParentPage);
                        break;

                    case 6:
                        this.navCtrl.setRoot(HomeParentPage);
                        break;

                    default:
                        this.navCtrl.setRoot(HomePage);
                        break;
                }

            }

        }).catch(error=>{
            this.loginLoader.dismiss();
            this.initLogin();

        });
    }

    updateAnnee(index:number){
        index==0?this.registerForm.controls['annee'].setValue( this.annees[0]):this.registerForm.controls['annee'].setValue( this.annees[1]);
    }

    //Create a new account
    register(data:any){

        this.currentUserData=data;

        this.loginLoader = this.loadingCtrl.create({
            content: '',
            dismissOnPageChange: true,
            duration: 60000
        });

        this.loginLoader.present();
        this.dataUser={user:data};
        this.user.signup(this.dataUser)
            .then(res=>{

            if (res&&res.reponse==="success") {

                this.initRegistration();
                this.createUser(res.user);

                this.currentUserData=null;

                switch (res.user.role_id){

                    case 4:

                        this.navCtrl.setRoot(HomePage);
                        break;

                    case 5:
                        this.navCtrl.setRoot(HomeParentPage);
                        break;

                    case 6:
                        this.navCtrl.setRoot(HomeParentPage);
                        break;

                    default:
                        this.navCtrl.setRoot(HomePage);
                        break;
                }
            }
            this.loginLoader.dismiss();

        }).catch(error=>{
            this.loginLoader.dismiss();
            this.initWithCurentData(this.currentUserData);
        });
    }

    //Go to the forgot password page
    forgotPass(){
        this.navCtrl.push(ForgotPassPage);
    }

}
