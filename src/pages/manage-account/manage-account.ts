import { Component } from '@angular/core';
import { ModalController, NavController, NavParams } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { UserProvider } from "../../providers/user/user";
import { InviteUserPage } from "../invite-user/invite-user";
import { ProfilPage } from "../profil/profil";
import { DetailUserPage } from '../detail-user/detail-user';
import { ReportsPage } from '../reports/reports';

/**
 * Generated class for the ManageAccountPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-manage-account',
    templateUrl: 'manage-account.html',
})
export class ManageAccountPage {

    logo:string;
    userData:any;
    userClasse:any;
    userSchool:any;
    serveurReponse:boolean;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public storage:Storage,
        public user:UserProvider,
        public modalCtrl:ModalController
    ) {

        this.logo="assets/img/MooExams_pre.png";
        this.serveurReponse=false;
        this.userData=[];
        this.userClasse={};
        this.userSchool={};
        this.getStudents();
    }

    ionViewDidLoad() {
        this.getUserChild(0,false);
    }



    //Go to the user profile
    goProfile(){
        this.navCtrl.push(ProfilPage);
    }


    //Add a student
    addStudent(){

        this.storage.get('user-students').then(data=>{
            if(data){
                this.navCtrl.push(InviteUserPage,{users:data.students,matiere:null});
            }else{
                this.navCtrl.push(InviteUserPage,{users:[],matiere:null});
            }
        });
    }

    //Get user from server
    getStudents(){
        this.storage.get('token').then(token=>{
            this.user.getAllStudent(token)
                .then(data=>{

                    this.serveurReponse=true;
                    if(data.token=="success"||data.reponse=="success"){
                        this.userData=data.students;
                        this.userClasse=data.classes;
                        if(data.userClasse)
                            this.userSchool=data.userClasse;

                    }else{
                        this.userData=[];
                        this.userClasse={};
                        this.userSchool={};
                    }

                }).catch(err=>{
                    this.serveurReponse=true;
                });
        });
    }


    //Get user from local
    getUserLocal(){
        this.storage.get('user-students').then(user=>{

            if(user){
                this.userData=user.students;
                this.userClasse=user.classes;
                this.userSchool=user.userClasse;

            }else{
                this.userData=[];
                this.userClasse={};
                this.userSchool={};
            }

        });
    }

    getUserChild(refresher,refresh){

        if(refresh){
            this.getStudents();
        }else{

            this.getUserLocal();
            if(this.userData&&this.userData.length===0){
                this.getStudents();
            }
        }

        if(refresher!=0)
            refresher.complete();
    }

    //Get all statistics of the user
    userStats(user,school){
        //show the user detail result
        this.navCtrl.push(ReportsPage,{options:{id:user.id,annee:school.annee}});
    }

    //more informations about the student
    moreInfos(user:any,classe:any,school:any){
        //show a modal with user Informations
        let modal = this.modalCtrl.create(DetailUserPage,{user:user,classe:classe,school:school,choice:"infos"});

        modal.present();
    }
}
