import { Component } from '@angular/core';
import { NavController,ViewController, NavParams } from 'ionic-angular';
import { Matiere } from "../../models/matiere";
import { Storage } from "@ionic/storage";
import { MatiereProvider } from "../../providers/matiere/matiere";

/**
 * Generated class for the MatiereClassePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-matiere-classe',
    templateUrl: 'matiere-classe.html',
})
export class MatiereClassePage {

    listMatiere:Matiere[];
    prefix:string;
    choix:Matiere;
    serveurReponse:boolean;
    backgroundColors:string[];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public storage:Storage,
        public matiere:MatiereProvider,
        public viewCtrl:ViewController
    ) {
        this.prefix="http://mooexams.sdkgames.com";
        this.listMatiere=[];
        this.serveurReponse=false;
        this.backgroundColors=["rgba(128, 128, 128,0.2)","rgba(60, 180, 75,0.2)","rgba(255, 215, 180,0.2)","rgba(70, 240, 240,0.2)"];
    }

    ionViewDidLoad() {
        this.getCourses(0,true);
    }

    //Get course from local
    getCoursesFromLocal(){
        this.storage.get('classe_matieres').then(data=>{
            if(data){
                this.listMatiere=data;
            }else{
                this.listMatiere=[];
            }
            this.serveurReponse=true;
        });
    }


    //Get the course from server
    getCourseFromServer(){
        this.storage.get('token').then(token=>{
            this.matiere.classCourses(token)
                .then(data=>{

                    let matiere=[];
                    if(data){
                        Object.keys(data).map(function (key) {
                            matiere.push(data[key]);
                        });
                    }
                    this.listMatiere=matiere;
                    this.serveurReponse=true;

                }).catch(err=>{
                    this.getCoursesFromLocal();
                    this.serveurReponse=true;
                });
        });
    }

    //Take the list of Course
    getCourses(refresher,refresh){

        if(refresh){
            this.getCourseFromServer();
        }else{
            this.getCoursesFromLocal();
            if(this.listMatiere&&this.listMatiere.length==0){
                this.getCourseFromServer();
            }
        }

        if(refresher)
            refresher.complete();
    }

    //Get the user course
    getChoix(choix){
        this.choix=choix;
        if(choix){
            this.viewCtrl.dismiss(this.choix);
        }
    }


    //close the modal
    close(){
        this.viewCtrl.dismiss(this.choix);
    }



}
