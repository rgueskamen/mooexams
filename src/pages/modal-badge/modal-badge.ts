import { TranslateService } from '@ngx-translate/core';
import { ApiProvider } from './../../providers/api/api';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { Storage } from "@ionic/storage" ;
import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the ModalBadgePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
    selector: 'page-modal-badge',
    templateUrl: 'modal-badge.html',
})
export class ModalBadgePage {

    resultas:any;
    user:any;
    level:any;
    classe:any;
    message : string;
    title:string;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public viewCtrl:ViewController,
        public storage:Storage,
        public api:ApiProvider,
        public userService:UserProvider,
        public translate: TranslateService
    ) {
        this.resultas=this.navParams.get('resultats');
        this.user=this.resultas.user;
        this.level=this.resultas.level;

        this.storage.get('user-classes').then(classe=>{
                this.classe=classe;
        });

        this.message="";
        this.title="";
    }

    ionViewDidLoad() {

    }

    //close the modal
    close(){
        this.viewCtrl.dismiss();
    }

    share(){

        this.translate.get(['SHARE_MESSAGE_FAILLED','STUDENT','CONGRATULATION','GRADE_UP_MESSAGE','PROFIL_ALERT','DOWNLOAD_MOOEXAMS','MOOEXAMS_LEVEL']).subscribe(value=>{
            this.message=`${this.user.prenom}  ${this.user.nom} \n
            ${value.STUDENT} ${this.classe.NOM_CLASSE}   ${this.classe.OPTION_CLASSE!='NONE' ? this.classe.OPTION_CLASSE :''} \n 
            ${value.CONGRATULATION} \n
            ${value.GRADE_UP_MESSAGE} \n
            ${value.PROFIL_ALERT}   ${this.level.badge}\n
            ${value.DOWNLOAD_MOOEXAMS}\n
            `;

            this.title=value.MOOEXAMS_LEVEL;
        });

        let data={
            message:this.message,
            name:this.title,
            image:"",
            link:"https://goo.gl/u34l3k"
        };

        this.close();

        this.api.share(data).then(reponse=>{
        
        }).catch(err=>{
            this.translate.get(['SHARE_MESSAGE_FAILLED']).subscribe(value=>{
                this.userService.toastAlert(value.SHARE_MESSAGE_FAILLED);
            });
        });

    }

}
