import { UserProvider } from './../../providers/user/user';
import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams, ViewController} from 'ionic-angular';
import { ClasseProvider} from "../../providers/classe/classe";
import { Storage } from "@ionic/storage";
import { Classe   } from "../../models/classe";
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the ModalClassesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */



@Component({
    selector: 'page-modal-classes',
    templateUrl: 'modal-classes.html',
})
export class ModalClassesPage {

    listClasse : Classe[];
    prefix:string;
    choix: Classe;
    serveurReponse:boolean;
    backgroundColors:string[];
   
    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public user : UserProvider,
        public classe : ClasseProvider,
        public api : ApiProvider,
        public storage : Storage,
        public viewCtrl: ViewController,
        public loaderCtrl:LoadingController
    ) {

        this.listClasse=[];
        this.prefix="http://mooexams.sdkgames.com";
        this.serveurReponse=false;
        this.backgroundColors=["rgba(128, 128, 128,0.2)","rgba(60, 180, 75,0.2)","rgba(255, 215, 180,0.2)","rgba(70, 240, 240,0.2)"];
    

    }


ionViewDidLoad() {
        this.doRefresh(0,true);
}


getClasseFromLocal(){

        this.storage.get('classes-by-country').then(data=>{
            if(data){
                this.listClasse=data;
            }else{
                this.listClasse=[];
            }
             this.serveurReponse=true;
        });
}


 //Get the list of class of current countries
  getServerClass(){

         // Get current country data informations
          this.api.getCountryByIpAdress().then(country=>{

                // Get the list of countries registered in the database
                this.user.getCountries().then(data =>{

                    let param={};

                    if(data&&data.reponse==="success"){
                        country = this.api.getCountryInfos(data.pays,country);
                       
                        param={
                            "langue":"fr",
                            "pays_id":country.id
                        };
                    }else{
                        param={
                            "langue":"fr",
                            "pays_id":1
                        };
                    }
                  

                    // Get the list of classes
                    this.classe.listByCountry(param).then(res=>{

                        if(res&&res.reponse==="success"){
                            this.listClasse=res.classes;
                         
                        }
                        this.serveurReponse=true;
                    }).catch(err=>{
                        this.serveurReponse=true;
                     
                    });
            }).catch(err=>{
               
                this.serveurReponse=true;
            });
        }).catch(err=>{
            this.serveurReponse=true;
           
        });

    }

   /* getServerClass(){

        this.classe.list().
        then(res=>{

            if(res){
                this.listClasse=res;
            }
            this.serveurReponse=true;
        }).catch(err=>{
            this.serveurReponse=true;
        });
    }

        //Get country classes for database
        getClasseFromLocal(){
            this.storage.get('classes').then(data=>{
                if(data){
                    this.listClasse=data;
                }else{
                    this.listClasse=[];
                }
            });
        }*/

        
    //Take the list of class
    doRefresh(refresher,refresh){

        if(refresh){
            this.getServerClass();
        }else{
            this.getClasseFromLocal();
            if(this.listClasse&&this.listClasse.length==0){
                this.getServerClass();
            }
        }

        if(refresher != 0)
            refresher.complete();
    }


    //Get the user choice
    getChoix(choix){
        this.choix=choix;
        if(choix){
            this.viewCtrl.dismiss(this.choix);
        }
    }


    //close the modal
    close(){
        this.viewCtrl.dismiss(this.choix);
    }

}
