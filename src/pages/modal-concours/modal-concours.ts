import { HomePage } from './../home/home';
import { TranslateService } from '@ngx-translate/core';
import { ApiProvider } from './../../providers/api/api';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { Storage } from "@ionic/storage" ;
import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the ModalConcoursPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-modal-concours',
  templateUrl: 'modal-concours.html',
})
export class ModalConcoursPage {
  resultas:any;
  user:any;
  level:any;
  classe:any;
  message : string;
  points:number;
  success:number;
  failed : number;
  title:string;

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public viewCtrl:ViewController,
      public storage:Storage,
      public api:ApiProvider,
      public userService:UserProvider,
      public translate: TranslateService
  ) {
      this.resultas=this.navParams.get('resultats');
      this.points=this.resultas.points;
      this.success=this.resultas.success;
      this.failed=this.resultas.failed;
     
      this.storage.get('user').then(user=>{
            this.user=user;
      });

      this.storage.get('user-classes').then(classe=>{
        this.classe=classe;
      });

      this.message="";
      this.title="";
  }

  ionViewDidLoad() {
    this.playsound();
  }

  playsound(){
      if(this.success >= this.failed ){
          this.api.playSuccessSound();
      }else{
          this.api.playFailledSound();
      }
  }

  stopsound(){
    if(this.success >= this.failed ){
        this.api.stopSuccessSound();
    }else{
        this.api.stopFailedSound();
    }
 }

  ionViewDidLeave() {
    this.stopsound();
  }

  //close the modal
  close(){
      this.viewCtrl.dismiss();
      this.stopsound();
      this.navCtrl.setRoot(HomePage);
  }

  share(){

      this.translate.get(['TOTAL_POINTS','STUDENT','CONGRATULATION','CONCOURS_GOOD_ANSWER','CONCOURS_WRONG_ANSWER','DOWNLOAD_MOOEXAMS','CONCOURS_RESULT']).subscribe(value=>{
          this.message=` ${value.CONCOURS_RESULT} \n ${this.user.prenom}  ${this.user.nom} \n
          ${value.STUDENT} ${this.classe.NOM_CLASSE}   ${this.classe.OPTION_CLASSE!='NONE' ? this.classe.OPTION_CLASSE :''} \n 
          ${value.CONGRATULATION} \n
          ${value.CONCOURS_GOOD_ANSWER} : ${ this.success}\n 
          ${value.CONCOURS_WRONG_ANSWER}   ${this.failed}\n
          ${value.TOTAL_POINTS}   ${this.points}\n
          ${value.DOWNLOAD_MOOEXAMS}
          `;

          this.title=value.CONCOURS_RESULT;
      });

      let data={
          message:this.message,
          name:this.title,
          image:"",
          link:"https://goo.gl/u34l3k"
      };

      this.close();

      this.api.share(data).then(reponse=>{

        this.navCtrl.setRoot(HomePage);
      
      }).catch(err=>{
          this.translate.get(['SHARE_MESSAGE_FAILLED']).subscribe(value=>{
              this.userService.toastAlert(value.SHARE_MESSAGE_FAILLED);
          });
          this.navCtrl.setRoot(HomePage);
      });

  }

}
