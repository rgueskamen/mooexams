import { Component } from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';

/**
 * Generated class for the ModalMessagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-modal-message',
  templateUrl: 'modal-message.html',
})
export class ModalMessagePage {

    message:string;

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public viewCtrl:ViewController
  )
  {
      this.message=this.navParams.get('message');
  }

  ionViewDidLoad() {

  }

  close(){
    this.viewCtrl.dismiss();
  }

}
