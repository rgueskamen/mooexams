import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { UserProvider } from "../../providers/user/user";

/**
 * Generated class for the ModalPaysPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-modal-pays',
    templateUrl: 'modal-pays.html',
})
export class ModalPaysPage {

    countries:any;
    choix:any;
    serveurReponse:boolean;
    backgroundColors:string[];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public storage:Storage,
        public user:UserProvider,
        public viewCtrl: ViewController

    ) {
        this.countries=[];
        this.serveurReponse=false;
        this.backgroundColors=["rgba(128, 128, 128,0.2)","rgba(60, 180, 75,0.2)","rgba(255, 215, 180,0.2)","rgba(70, 240, 240,0.2)"];
    }

    ionViewDidLoad() {
        this.getCountries(0,true);
    }


    //getCountries from local
    getCountriesFromLocal(){
        this.storage.get('countries').then(countries=>{

            if(countries){
                this.countries=countries;
            }else{
                this.countries=[];
            }
            this.serveurReponse=true;
        });
    }


    //Get countries from server
    getCountriesFromServer(){

        this.user.getCountries()
            .then(  data  =>{

                if(data&&data.reponse==="success"){
                    this.countries=data.pays;
                }else{
                    this.countries=[];
                }
                this.serveurReponse=true;
            }).catch(err=>{
                this.getCountriesFromLocal();
                this.serveurReponse=true;
            });
    }


    //Get the List of all countries
    getCountries(refresher,refresh){
        if(refresh){
            this.getCountriesFromServer();
        }else{
            this.getCountriesFromLocal();
            if(this.countries&&this.countries.length>0){
                this.getCountriesFromServer();
            }
        }

        if(refresher != 0)
            refresher.complete();
    }

    //Get the user course
    getChoix(choix){
        this.choix=choix;
        if(choix){
            this.viewCtrl.dismiss(this.choix);
        }
    }


    //close the modal
    close(){
        this.viewCtrl.dismiss(this.choix);
    }


}
