import { Component } from '@angular/core';
import { NavController, NavParams, ViewController,Events } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the ModalSuccessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
    selector: 'page-modal-success',
    templateUrl: 'modal-success.html',
})
export class ModalSuccessPage {


    questions:any;
    url:string;
    activeButton:boolean;

    constructor(
        public navCtrl: NavController,
        public events:Events,
        public api:ApiProvider,
        public navParams: NavParams,
        public viewCtrl : ViewController
    ) {
        this.questions=this.navParams.get('question');
        this.url="http://mooexams.sdkgames.com/";
        this.activeButton=false;

        events.subscribe('modal:close', (time) => {
            this.close();
        });
      
    }



  ionViewDidLoad() {
    this.playSound();
   }

   ionViewDidLeave() {
    this.api.stopSuccessSound();
   }

    playSound(){

        this.api.playSuccessSound().then(playsound=>{
           this.activeButton=true;
       }).catch(failedPlaySound=>{
           this.activeButton=true;
       });
   }


    //close the modal
    close(){
        this.viewCtrl.dismiss();
    }

    //Format the data
    format(chaine:string){

        let ch=String(chaine);
        let resultat=ch.replace(/\\'/g,"'");
        resultat=resultat.replace(/\\"/g,'"');
        return resultat;
    }

}
