import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController} from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { MatiereProvider } from "../../providers/matiere/matiere";
import { TranslateService } from "@ngx-translate/core";
import { ChallengeProvider } from '../../providers/challenge/challenge';
import { ProfilPage} from "../profil/profil";
import { ChallengeResultPage } from "../challenge-result/challenge-result";
import { ChallengeOptionPage } from "../challenge-option/challenge-option";
import { UserProvider } from "../../providers/user/user";
import { ChallengeUserPage } from "../challenge-user/challenge-user";
import { CreateChallengePage } from "../create-challenge/create-challenge";

/**
 * Generated class for the MyChallengePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
    selector: 'page-my-challenge',
    templateUrl: 'my-challenge.html',
})
export class MyChallengePage {

    backgroundColors:string[];
    showDetailChallenge:boolean[];
    challenges:any;
    users:any;
    participants:any;
    matieres:any;
    schools:any;
    classe:any;
    criteres_classe:any;
    days:any;
    months:any;
    serveurReponse:boolean;
    loader:any;
    url:string;


    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public storage:Storage,
        public matiere:MatiereProvider,
        public translate:TranslateService,
        public alertCtrl:AlertController,
        public challenge:ChallengeProvider,
        public loading:LoadingController,
        public userService:UserProvider
    ) {
       // this.backgroundColors=["#e0d2c3","#d7efbf","#ffe2c4","#aaf9ff","#c2c6c6"];
        this.backgroundColors=["#e0d2c3","rgba(128, 128, 128,0.2)","rgba(60, 180, 75,0.2)","rgba(255, 215, 180,0.2)","rgba(70, 240, 240,0.2)"];
        this.showDetailChallenge=[false];
        this.challenges=[];
        this.users=[];
        this.matieres=[];
        this.schools=[];
        this.classe=[];
        this.criteres_classe=[];
        this.participants=[];
        this.serveurReponse=false;
        this.getMonths();
        this.getDays();
        this.url="http://mooexams.sdkgames.com/";

    }




    //Get the months format
    getMonths(){

        this.months=[];
        this.translate.get(['MONTH1','MONTH2','MONTH3','MONTH4','MONTH5','MONTH6','MONTH7','MONTH8','MONTH9','MONTH10','MONTH11','MONTH12'])
            .subscribe(months=>{
                this.months.push(months.MONTH1);
                this.months.push(months.MONTH2);
                this.months.push(months.MONTH3);
                this.months.push(months.MONTH4);
                this.months.push(months.MONTH5);
                this.months.push(months.MONTH6);
                this.months.push(months.MONTH7);
                this.months.push(months.MONTH8);
                this.months.push(months.MONTH9);
                this.months.push(months.MONTH10);
                this.months.push(months.MONTH11);
                this.months.push(months.MONTH12);
            });
    }

    //Get the days format
  getDays(){

      this.days=[];
      this.translate.get(['SHORTDAY1','SHORTDAY2','SHORTDAY3','SHORTDAY4','SHORTDAY5','SHORTDAY6','SHORTDAY7'])
          .subscribe(days=>{
              this.days.push(days.SHORTDAY1);
              this.days.push(days.SHORTDAY2);
              this.days.push(days.SHORTDAY3);
              this.days.push(days.SHORTDAY4);
              this.days.push(days.SHORTDAY5);
              this.days.push(days.SHORTDAY6);
              this.days.push(days.SHORTDAY7);
      });

  }

    ionViewDidLoad() {
        this.getChallenges(0,false);
    }

    //Get the list of challenges
    getChallengesFromServer(){

        this.storage.get('token').then(token=>{

            let param={"token":token};

            this.storage.get('user').then(user=>{

                param["user_id"]=user.id;

                this.challenge.getChallenges(param,param.token)
                    .then(data=>{

                        this.serveurReponse=true;
                        if(data&&data.reponse=="success"){
                            if(data.challenges) {
                                this.challenges = data.challenges;
                                for (let j=0;j<this.challenges.length;j++){
                                    this.showDetailChallenge.push(false);
                                }
                            }
                            if(data.classes_criteres)
                                this.criteres_classe=data.classes_criteres;
                            if(data.userClasse)
                                this.schools=data.userClasse;
                            if(data.users)
                                this.users=data.users;
                            if(data.matieres)
                                this.matieres=data.matieres;
                            if(data.classes)
                                this.classe=data.classes;
                            if(data.participants)
                                this.participants=data.participants;
                        }

                    }).catch(err=>{
                        this.serveurReponse=true;
                        this.getChallengesFromLocal();
                    });
            });
        });

    }

    //Add a Challenge
    addChallenge(){

        this.storage.get('user').then(user=>{

            let param={"user":user};
            this.storage.get('user-classes').then(classe=>{
                this.navCtrl.push(CreateChallengePage,{user:param.user,classe:classe});
            });

        });

    }

    //Get the list form local
    getChallengesFromLocal() {

        this.storage.get('my-challenges').then(data=>{
            if(data){
                if(data.challenges) {
                    this.challenges = data.challenges;
                    for (let j=0;j<this.challenges.length;j++){
                        this.showDetailChallenge.push(false);
                    }
                }
                if(data.classes_criteres)
                    this.criteres_classe=data.classes_criteres;
                if(data.userClasse)
                    this.schools=data.userClasse;
                if(data.users)
                    this.users=data.users;
                if(data.matieres)
                    this.matieres=data.matieres;
                if(data.classes)
                    this.classe=data.classes;
                if(data.participants)
                    this.participants=data.participants;
            }
            this.serveurReponse=true;
        });
    }


    //Get the list of challenges
    getChallenges(refresher,refresh){

        if(refresh){
            this.getChallengesFromServer();
        }else{
            this.getChallengesFromLocal();
            if(this.challenges&&this.challenges.length==0){
                this.getChallengesFromServer();
            }
        }

        if(refresher!=0)
            refresher.complete();
    }

    //Hide and show the list of Challenge
    showDetail(index){
        this.showDetailChallenge[index]=!this.showDetailChallenge[index];
    }

    //Go to the user profil page
    goProfile(){
        this.navCtrl.push(ProfilPage);
    }

    formatDate(date:Date) : string{

        if(date) {
            let datejour = new Date(date);
            return this.days[datejour.getDay()] + ". " + datejour.getDate() + " " + this.months[datejour.getMonth()] + ". " + datejour.getFullYear();
        }else{
            return "";
        }
    }

    //Show the result of the Challenge
    gotoResult(challenge,participants,users,matieres,classe,school){

        this.navCtrl.push(ChallengeResultPage,{
            challenge:challenge,
            participants:participants,
            users:users,
            matieres:matieres,
            classe:classe,
            school:school
        });

    }

    //Invite your friends to the challenge
    inviteUsers(challenge){
        this.navCtrl.push(ChallengeUserPage,{challenge:challenge,users:this.participants});
    }

    //Add criteria to the Challenge
    addCriteria(challenge){
        this.navCtrl.push(ChallengeOptionPage,{challenge:challenge});
    }

    //enable the challenge
    activateChallenge(challenge,index){

        this.storage.get('user').then(user=>{

            let data={"user_id":user.id,"id_challenge":challenge.id};
            this.loader =this.loading.create({
                content : '',
                dismissOnPageChange:true,
                duration: 60000
            });
            this.loader.present();

            this.storage.get('token').then(token=>{

                this.challenge.enableChallenge(data,token)
                    .then(reponse=>{

                        this.loader.dismiss();
                        if(reponse&&reponse.reponse=="success"){
                            this.challenges[index].active=0;
                            this.getChallengesFromServer();
                        }

                    }).catch(err=>{

                        this.loader.dismiss();
                        if(err&&err.reponse=="failure"){

                            this.translate.get(['CHALLENGES_ERROR_ACTIVATE']).subscribe(value=>{
                                this.userService.toastAlert(value.CHALLENGES_ERROR_ACTIVATE);
                            });
                        }
                    });
            });

        });

    }

    //Disable de challenge
    disableChallenge(challenge,index){

        this.storage.get('user').then(user=>{

            let data={"user_id":user.id,"id_challenge":challenge.id};
            this.loader =this.loading.create({
                content : '',
                dismissOnPageChange:true,
                duration: 60000
            });
            this.loader.present();

            this.storage.get('token').then(token=>{

                this.challenge.disableChallenge(data,token)
                    .then(reponse=>{

                        this.loader.dismiss();
                        if(reponse&&reponse.reponse=="disabled"){
                            this.challenges[index].active=0;
                            this.getChallengesFromServer();
                        }

                    }).catch(err=>{

                        this.loader.dismiss();
                        if(err&&err.reponse=="failure"){

                            this.translate.get(['CHALLENGES_ERROR_DISABLE']).subscribe(value=>{
                                this.userService.toastAlert(value.CHALLENGES_ERROR_DISABLE);
                            });
                        }
                    });
            });

        });

    }

}
