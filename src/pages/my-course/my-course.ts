import { Component } from '@angular/core';
import { AlertController, LoadingController, NavController, NavParams} from 'ionic-angular';
import { MatiereProvider } from "../../providers/matiere/matiere";
import { TranslateService } from "@ngx-translate/core";
import { UserProvider } from "../../providers/user/user";
import { Storage } from "@ionic/storage";
import { FormBuilder, FormGroup, Validators} from "@angular/forms";
import {TeacherStudentsPage} from "../teacher-students/teacher-students";

/**
 * Generated class for the MyCoursePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
    selector: 'page-my-course',
    templateUrl: 'my-course.html',
})
export class MyCoursePage {

    serveurReponse:boolean;
    courses:any;
    backgroundColors:Array<string>;
    allCourses:any;
    courseForm:FormGroup;
    loaderAlert:any;
    loader:any;
    listMatiere:any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public storage:Storage,
        public matiere:MatiereProvider,
        public translate:TranslateService,
        public userService:UserProvider,
        public fb:FormBuilder,
        public loadingCtrl:LoadingController,
        public alertCtrl : AlertController

    ) {

        this.backgroundColors=["rgba(128, 128, 128,0.2)","rgba(60, 180, 75,0.2)","rgba(255, 215, 180,0.2)","rgba(70, 240, 240,0.2)"];
        this.serveurReponse=false;
        this.allCourses=[];
        this.listMatiere=[];
        this.courses=[];
    }

    ionViewDidLoad() {
        this.getUserMatieres(0,false);
    }


    //Go to the user profile
    gotoMyStudents(){
        this.navCtrl.setRoot(TeacherStudentsPage);
    }

    //init course form

    initCourse(){

        this.storage.get('user').then(user=>{
            if(user){
                this.courseForm = this.fb.group({
                    "user_id" : [user.id,Validators.compose([Validators.required])],
                    "id_matiere" : ["",Validators.compose([Validators.required])]
                });
            }
        });
    }


    //Get the list of Courses of a user

    getCourses(){

        let data={};

        this.storage.get('user').then(user=>{

            data={"teacher_id":user.id};

            this.storage.get('token').then(token=>{
                this.matiere.coursesTeaches(data,token)
                    .then(data=>{

                        if(data&&data.matieres.length>0){
                            this.courses=data.matieres;
                        }
                        this.serveurReponse=true;
                    }).catch(err=>{
                        this.serveurReponse=true;
                        this.getUserCousres();
                    });
            });
        });
    }


    //get user teaching courses

    getUserCousres(){
        this.storage.get('user-matieres').then(data=>{
            if(data){
                this.courses=data;
            }else{
                this.courses=[];
            }
            this.serveurReponse=true;
        });
    }

    //get Matieres

    getUserMatieres(refresher,refresh){

        if(refresh){
            this.getCourses();
        }else{
            this.getUserCousres();
            if(this.courses&&this.courses.length==0){
                this.getCourses();
            }
        }

        if(refresher!=0){
            refresher.complete();
        }
    }

    //Verify if a course is not in the user Courses

    courseNotExist(courses,subject):boolean{

        let exist=false,i=0;

        if(courses&&courses.length>0){
            while(i<courses.length&&!exist){
                if(courses[i].id==subject.id){
                    exist=true;
                }
                i++;
            }
        }

        return exist;
    }


    //Format the list of courses
    radioCourse(){

        this.initCourse();

        if(this.allCourses&&this.allCourses.length>0) {

            let alert = this.alertCtrl.create();

            this.translate.get(['SELECT_COURSE']).subscribe(value => {
                alert.setTitle(value.SELECT_COURSE);
            });

            for (let i = 0; i < this.allCourses.length; i++) {

                if (this.allCourses[i] && !this.courseNotExist(this.courses, this.allCourses[i])) {
                    alert.addInput({
                        type: 'radio',
                        label: this.allCourses[i].NOM_MATIERE.toUpperCase(),
                        value: this.allCourses[i],
                        checked: false
                    });
                }
            }

            alert.addButton('Cancel');
            alert.addButton({
                text: 'OK',
                handler: data => {

                    if (data) {
                        this.courseForm.controls["id_matiere"].setValue(data.id);
                        this.addMatiere(this.courseForm.value);

                    } else {
                        this.initCourse();
                    }
                }
            });

            alert.present();
        }else{
            this.translate.get(['COURSE_GROUP_MESSAGE']).subscribe(value => {
                this.userService.toastAlert(value.COURSE_GROUP_MESSAGE);
            });
        }
    }


    //Save teacher Courses
    addMatiere(data:any){

        this.loaderAlert = this.loadingCtrl.create({
            content: '',
            dismissOnPageChange: true,
            duration: 60000
        });

        this.storage.get('token').then(token=>{

            this.loaderAlert.present();
            this.matiere.teacherCourses(data,token)
                .then(
                data=>{

                    this.loaderAlert.dismiss();
                    if(data&&data.reponse==="update"){
                        //Upadte de user Matiere
                        this.initCourse();

                        if(data.matieres){
                            this.courses=data.matieres;
                            this.storage.set('user-matieres',data.matieres);
                            this.userService.getAllUsersByCourses(token);
                        }else{
                            this.getCourses();
                        }

                    }else if(data&&data.reponse==="failure"){
                        this.initCourse();

                        this.translate.get(['FAILED']).subscribe(value=>{
                            this.userService.toastAlert(value.FAILED);
                        });
                    }

                }).catch(err=>{
                    this.loaderAlert.dismiss();
                    this.initCourse();
                }
            )
        });

    }


    //delete a subject
    deleteCourse(course){

        let param={};

        this.storage.get('user').then(user=>{
            param={
                "user_id":user.id,
                "id_matiere":course.id
            }
        });


        this.loaderAlert = this.loadingCtrl.create({
            content: '',
            dismissOnPageChange: true,
            duration: 60000
        });

        this.loaderAlert.present();

        this.storage.get('token').then(token=>{

            this.matiere.deleteSubject(param,token)
                .then(data=>{

                this.loaderAlert.dismiss();
                if(data&&data.reponse=="success"){
                    this.courses=data.matieres;
                    this.userService.getAllUsersByCourses(token);
                }
            }).catch(err=>{

                this.loaderAlert.dismiss();
                if(err&&err.reponse=="failure"){
                    this.translate.get(['DELETE_ERROR_MESSAGE']).subscribe(value=>{
                        this.userService.toastAlert(value.DELETE_ERROR_MESSAGE);
                    });
                }
            })

        });
    }

    //Get the all groupes of matieres

    getAllCoursesGroup(){

        this.storage.get('token').then(token=>{

            this.loader = this.loadingCtrl.create({
                content: '',
                dismissOnPageChange: true,
                duration: 60000
            });

            this.loader.present();
            this.matiere.groupsSubjects(token)
                .then(data=>{
                    this.loader.dismiss();
                    if(data&&data.reponse=="success"){

                        let matieres=[];

                        for(let i=0;i<data.groupe.length;i++){
                            matieres=matieres.concat(data.matiereGroupe[data.groupe[i].id]);
                        }
                        this.allCourses=matieres;
                        this.radioCourse();
                    }

                }).catch(err=>{
                    this.loader.dismiss();
                })

        });
    }


    //Get the all  subjects of a group

    getGroupCourses(){

        this.loader = this.loadingCtrl.create({
            content: '',
            dismissOnPageChange: true,
            duration: 60000
        });

        this.storage.get('token').then(token=>{

            if(this.courses[0].id_groupe) {

                this.loader.present();

                let param = {"id_groupe": this.courses[0].id_groupe};

                this.matiere.subjetsGroup(param, token)
                    .then(data => {

                        this.loader.dismiss();
                        if (data && data.reponse == "success") {

                            this.allCourses = data.matieres;
                            this.radioCourse();
                        }

                    }).catch(err => {
                        this.loader.dismiss();
                    });
            }

        });

    }


    //add a new course
    addCourse(){
        if(this.courses&&this.courses.length==0){
            this.getAllCoursesGroup();
        }else{
            this.getGroupCourses();
        }
    }


}
