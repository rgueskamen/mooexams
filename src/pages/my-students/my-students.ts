import { Component } from '@angular/core';
import { ModalController, NavController, NavParams } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { UserProvider } from "../../providers/user/user";
import { InviteUserPage } from "../invite-user/invite-user";
import { DetailUserPage } from '../detail-user/detail-user';
import { ReportsPage } from '../reports/reports';
import { HomeParentPage } from "../home-parent/home-parent";

/**
 * Generated class for the MyStudentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-students',
  templateUrl: 'my-students.html',
})
export class MyStudentsPage {

    logo:string;
    userData:any;
    userClasse:any;
    userSchool:any;
    course:any;
    serveurReponse:boolean;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public storage:Storage,
        public user:UserProvider,
        public modalCtrl:ModalController
    ) {
        this.serveurReponse=false;
        this.userData=[];
        this.userClasse={};
        this.userSchool={};
        this.course={};
    }

    ionViewDidLoad() {
        this.getUsers();
    }

    getUsers(){
        this.userData=this.navParams.get('students');
        this.userClasse=this.navParams.get('classes');
        this.userSchool=this.navParams.get('userClasses');
        this.course=this.navParams.get('course');
    }


    //Go to the user profile
    gotoDash(){
        this.navCtrl.push(HomeParentPage);
    }


    //Add a student
    addStudent(){
        this.navCtrl.push(InviteUserPage,{users:this.userData,matiere:this.course});
    }


    //Get all statistics of the user
    userStats(user,school){
        //show the user detail result
        this.navCtrl.push(ReportsPage,{options:{id:user.id,annee:school.annee}});
    }

    //more informations about the student
    moreInfos(user:any,classe:any,school:any){
        //show a modal with user Informations
        let modal = this.modalCtrl.create(DetailUserPage,{user:user,classe:classe,school:school});

        modal.present();
    }

}
