import { Component } from '@angular/core';
import { NavController,AlertController,ModalController,LoadingController, NavParams } from 'ionic-angular';
import { LeconsProvider } from '../../providers/lecons/lecons';
import { Storage } from "@ionic/storage";
import { QuestionDefisPage } from '../question-defis/question-defis';
import { MatiereProvider } from "../../providers/matiere/matiere";
import {ProfilPage} from "../profil/profil";
import {UserProvider} from "../../providers/user/user";

/**
 * Generated class for the ParamExamPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@Component({
    selector: 'page-param-exam',
    templateUrl: 'param-exam.html',
})

export class ParamExamPage {

    logo:string;
    parram :any;
    url:string;
    matieres:any;
    backgroundColors:Array<string>;
    serveurReponse:boolean;
    loader:any;

    constructor(

        public navCtrl: NavController,
        public navParams: NavParams,
        public alertCtrl: AlertController,
        public modalCtrl: ModalController,
        public loadingCtrl: LoadingController,
        public lesson: LeconsProvider,
        public storage: Storage,
        public matiere:MatiereProvider,
        public userService:UserProvider

    ) {
        this.logo="assets/img/MooExams_pre.png";
        this.backgroundColors=["rgba(128, 128, 128,0.2)","rgba(60, 180, 75,0.2)","rgba(255, 215, 180,0.2)","rgba(70, 240, 240,0.2)"];
        this.url="http://mooexams.sdkgames.com/";
        this.parram={id_matiere:0, id_user:0};
        this.serveurReponse=false;
        this.matieres=[];
    }

    ionViewDidLoad() {
        this.listOfCourses(0,false);
        this.updateInfos();
    }


    gotoProfile(){
        this.navCtrl.push(ProfilPage);
    }


    getcoursesFromLocal(){

        this.storage.get('classe_matieres').then(matieres=>{

            if(matieres&&matieres.length>0){
                this.matieres=matieres;
                this.serveurReponse=true;
            }else{
                this.matieres=[];
                this.serveurReponse=true;
            }

        });

    }

    getCoursesFromServer(){
        this.storage.get('token').then(token=>{
            this.matiere.classCourses(token)

                .then(data=>{

                let matiere=[];
                this.serveurReponse=true;
                if(data){
                    Object.keys(data).map(function (key) {
                        matiere.push(data[key]);
                    });
                    this.matieres=matiere;
                }
            }).catch(err=>{
                    this.serveurReponse=true;
                    this.getcoursesFromLocal();
            });
        });
    }


    //Take the list of Course
    listOfCourses(refresher,refresh){

        if(refresh){
            this.getCoursesFromServer();
        }else{

            this.getcoursesFromLocal();
            if(this.matieres&&this.matieres.length==0){
                this.getCoursesFromServer();
            }

        }

        if(refresher != 0)
            refresher.complete();
    }



    //send the revision param
    setParam(data){

        this.loader=this.loadingCtrl.create({
            content: "",
            dismissOnPageChange:true,
            duration: 60000
        });
        this.loader.present();
        this.storage.get('user').then(user=>{

            this.loader.dismiss();
            this.parram.id_user=user.id;
            this.parram.id_matiere=data.id;
            this.navCtrl.push(QuestionDefisPage,{params :this.parram});
        });

    }

    //Update the user infos
    updateInfos(){
        this.storage.get('token').then(token=> {
            if(token)
                this.userService.userInfos(token);
        });
    }


}
