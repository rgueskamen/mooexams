
import { Component } from '@angular/core';
import { Storage } from "@ionic/storage";
import { NavController, NavParams } from 'ionic-angular';
import { MatiereProvider } from "../../providers/matiere/matiere";
import { LeconsPage } from "../lecons/lecons";
import { ProfilPage } from '../profil/profil';
import {UserProvider} from "../../providers/user/user";



/**
 * Generated class for the ParamRevisionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@Component({
    selector: 'page-param-revision',
    templateUrl: 'param-revision.html',
})
export class ParamRevisionPage {

    courses:any;
    user:any;
    classe:any;
    url:string;
    backgroundColors:Array<String>;
    serveurReponse:boolean;
    logo:string;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public matiere: MatiereProvider,
        public storage:Storage,
        public userService:UserProvider
    ) {
        this.courses=[];
        this.logo="assets/img/MooExams_pre.png";
        this.url="http://mooexams.sdkgames.com/";
        this.backgroundColors=["rgba(128, 128, 128,0.2)","rgba(60, 180, 75,0.2)","rgba(255, 215, 180,0.2)","rgba(70, 240, 240,0.2)"];
        this.serveurReponse=false;
    }

    ionViewDidLoad() {
        this.getAllCourses(0,false);
    }

    //
    gotoProfile(){
        this.navCtrl.push(ProfilPage);
    }


    //Get courses with their lessons
    getCourseLessons(){

        let param={
            "id_user":"",
            "id_classe":""
        };

        this.storage.get('user').then(user=>{
            this.user=user;
            param.id_user=user.id;

            this.storage.get('user-classes').then(classe=>{

                if(classe) {

                    this.classe = classe;
                    param.id_classe = classe.id;

                    this.storage.get('token').then(token => {

                        this.matiere.getCourseLesson(param, token)
                            .then(data => {

                                if (data&&data.reponse=="success") {
                                    this.courses = data.leconMatiere;
                                }
                                this.serveurReponse=true;
                            }).catch(err=>{
                                this.serveurReponse=true;
                                this.getLocalCourses();
                            });
                    });
                }else{
                    this.storage.get('token').then(token => {
                        if(token)
                            this.userService.userInfos(token);
                    });
                }

            });

        });

    }

    //Get courses frol local Storage
    getLocalCourses(){
        this.storage.get('course-lessons').then(data=>{
            if(data){
                this.courses=data;
            }else{
                this.courses=[];
            }
            this.serveurReponse=true;

        });
    }

    //Get the courses to show
    getAllCourses(refresher,refresh){

        if(refresh){
            this.getCourseLessons();
        }else{
            this.getLocalCourses();
            if(this.courses&&this.courses.matieres&&this.courses.matieres.length==0||this.courses.length==0){
                this.getCourseLessons();
            }
        }

        if(refresher)
            refresher.complete();
    }


    //Open the subjets page
    openSubjects(matiere:any,lecon:any){
        this.navCtrl.push(LeconsPage,{subjects:matiere,lessons:lecon});
    }

    //Write the filter by group of subjects
    filterGroupSubjects(){

    }

}
