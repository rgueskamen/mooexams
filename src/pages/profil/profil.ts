import { Component } from '@angular/core';
import { AlertController, LoadingController, NavController, Events, NavParams,ItemSliding} from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { UserProvider } from "../../providers/user/user";
import { ApiProvider } from "../../providers/api/api";
import { LoaderPage } from "../loader/loader";
import { FormBuilder, Validators } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import { ClasseProvider } from "../../providers/classe/classe";
import { MatiereProvider } from "../../providers/matiere/matiere";
import { ChallengeProvider} from "../../providers/challenge/challenge";



/**
 * Generated class for the ProfilPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-profil',
    templateUrl: 'profil.html',
})
export class ProfilPage {

    userData : any ={};
    option:string;
    tabs:any;
    pays:any;
    classes:any;
    imageBackgroung:string;
    countryName:string;
    imageStyles:any;
    notifications:any;
    options:any;
    shortDays:any;
    courses:any;
    niveaux:any;
    listMatiere:any;
     public url : string ;
    showBoutton:boolean;
    imageServer:boolean;
    challenges:any;
    usersChallenge:any;
    schoolsChallenge:any;
    matieresChallenge:any;
    classeChallenge:any;
    participantsChallenge:any;
    nbreInvitations:number;
    interval:any;
    loader:any;


    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public events: Events,
        public user : UserProvider,
        private storage:Storage,
        public loadingCtrl: LoadingController,
        public api:ApiProvider,
        public fb:FormBuilder,
        public translate:TranslateService,
        public alertCtrl: AlertController,
        public classe:ClasseProvider,
        public matiere:MatiereProvider,
        public challenge:ChallengeProvider

    ) {

        this.option='identification';
        this.url="http://mooexams.sdkgames.com/";
        this.showBoutton=false;
        this.imageServer=false;

        this.challenges=[];
        this.usersChallenge=[];
        this.schoolsChallenge=[];
        this.usersChallenge={};
        this.matieresChallenge=[];
        this.classeChallenge=[];
        this.participantsChallenge=[];
        this.nbreInvitations=0;
        this.interval=null;

        this.shortDays=[];
        this.translate.get(['SHORTDAY1','SHORTDAY2','SHORTDAY3','SHORTDAY4','SHORTDAY5','SHORTDAY6','SHORTDAY7'])
            .subscribe(days=>{
                this.shortDays.push(days.SHORTDAY1);
                this.shortDays.push(days.SHORTDAY2);
                this.shortDays.push(days.SHORTDAY3);
                this.shortDays.push(days.SHORTDAY4);
                this.shortDays.push(days.SHORTDAY5);
                this.shortDays.push(days.SHORTDAY6);
                this.shortDays.push(days.SHORTDAY7);
            });

        this.options=[
            {choix:false,value:"nom"},
            {choix:false,value:"prenom"},
            {choix:false,value:"email"},
            {choix:false,value:"username"},
            {choix:false,value:"tel"},
            {choix:false,value:"language"},
            {choix:false,value:"gender"},
            {choix:false,value:"password"},
            {choix:false,value:"pays"},
            {choix:false,value:"ville"},
            {choix:false,value:"adresse"},
            {choix:false,value:"classe"},
            {choix:false,value:"school"},
            {choix:false,value:"annee"}
        ];

        this.tabs=[];
        this.imageBackgroung="";
        this.imageStyles = "assets/img/profil.jpg";


        //Get user informations
        this.getUserInfo();


        this.storage.get('user').then(user=>{

            if(user&&user.role_id==4){
                //Get the user class
                this.getUserClass();
                //Get the user notifications
                this.getInvitations();
            }
        });

    }


    ionViewDidLoad() {

        this.listOfcountries();
        this.listOfClasses();
        this.getUserInfos(true,0);
        this.getChallenges(0,true);
    }

    // Listen to user update Informations
    updateUser(user) {
      this.events.publish('user:updated', user, Date.now());
    }

    // Listen to user update classe
    updateClass(classe) {
      this.events.publish('user:classes', classe, Date.now());
    }

     // Listen to user update classe
     updateBadge(challenges,notifications) {
      this.events.publish('user:badge',notifications,challenges);
    }

    //init user form
    public userForm =  this.fb.group({
        "user_id":[""],
        "username":[""],
        "username_or_email":[""],
        "nom": [""],
        "tel":[""],
        "prenom": [""],
        "image_data":[""],
        "gender":[""],
        "language":[""],
        "longitude":[""],
        "latitude":[""],
        "pays":[""],
        "pays_id":[""],
        "ville":[""],
        "adresse":[""],
        "email":["",Validators.compose([Validators.pattern('^[a-zA-Z0-9_]+@[a-zA-Z0-9_.]+\\.[a-zA-Z]{2,5}')])],
        "oldPassword":[""],
        "newPassword":[""]
    });



    //class form
    public classeForm =  this.fb.group({
        "user_id":[""],
        "classe_id":[""],
        "school":[""],
        "annee":[""]
    });


    //FORMAT THE DATE
    formatDate(date:Date):string{

        if(date) {
            let dateFormat=new Date(date);
            return " " + this.shortDays[dateFormat.getDay()] + " " + (dateFormat.getDate() <= 9 ? "0" + dateFormat.getDate() : dateFormat.getDate()) + " | " + ((dateFormat.getMonth() + 1) <= 9 ? "0" + (dateFormat.getMonth() + 1) : (dateFormat.getMonth() + 1)) + " | " + dateFormat.getFullYear();
        }else{
            return "";
        }
    }

    //Get the academic Year of the user

    getAcademicYear() {

        this.storage.get('user-school').then(school=>{

            if(school){
                this.classeForm.controls['annee'].setValue(school.annee);
                this.classeForm.controls['school'].setValue(school.school);
                this.classeForm.controls['user_id'].setValue(school.id_user);

                if(this.userData&&this.userData.classes){
                    this.userData.classes.annee=school.annee;
                    this.userData.classes.school=school.school;
                }

            }else {
                this.getUserInfosByServer();
            }

        });
    }

    getUserCousres(){
        this.storage.get('user-matieres').then(data=>{
            if(data){
                this.courses=data;
            }else{
                this.courses=[];
            }
        });
    }


    //Get the user infos
    getUserInfo(){

        this.storage.get('user').then(data=>{
            if(data){
                this.userData["user"]=data;
                this.initForm(data);
                this.countryName=this.getCountry(data.pays_id);
                this.imageBackgroung = data.image_url;
                if(this.imageBackgroung){
                    this.imageStyles ='http://mooexams.sdkgames.com/'+this.imageBackgroung;
                    this.imageServer=true;
                }

            }else{
                this.userData["user"]=[];
                this.getUserInfosByServer();
            }
        });
    }

    //get the class of user
    getUserClass(){

        this.storage.get('user-classes').then(data=>{
            if(data){
                this.userData["classes"]=data;
                this.initClassForm(data);
            }else{
                this.userData["classes"]=[];
                this.getUserInfosByServer();
            }
        });

    }


    //Get the user level in courses
    getUserLevel(){

        this.storage.get('user-niveaux').then(data=>{

            if(data){
                this.niveaux=data;
            }else{

                if(this.userData){
                    let param={
                        "id_user":this.userData.user.id,
                        "annee":this.userData.classes.annee
                    };

                    this.storage.get('token').then(token=>{
                        this.matiere.getUsersLevels(param,token)
                            .then(data=>{

                                if(data&&data.reponse=="success"){
                                    this.niveaux=data.niveaux_matiere;
                                }

                        }).catch(err=>{

                                    this.niveaux=[];
                        });
                    });

                }else{
                    this.niveaux=[];
                }
            }
        });
    }


    //get Invitations
    getInvitations(){
        //Get the user invitations
        this.storage.get('user-invitations').then(data=>{
            if(data&&data.length>0){
                this.notifications=data;
            }else{
                this.notifications=[];
                this.getInvitationsList();
            }
        });
    }



    //Get the user options
    getOption(value,index){

        this.option=value;

        if(index==0){
            this.getUserInfo();
        }

        if (this.userData&&this.userData.user&&this.userData.user.role_id==4) {

            if (index==1) {
                this.getUserClass();
                this.getUserCousres();
                this.getUserLevel();
            }

            if (index==2) {
                this.getInvitations();
                this.getChallenges(0,false);
            }
        }


    }


    //Init the formData with user informations
    initForm(data){

        if(data){
            this.userForm =  this.fb.group({
                "user_id":[data.id],
                "username":[data.username],
                "username_or_email":[data.email||data.username],
                "nom": [data.nom],
                "tel":[data.tel],
                "prenom": [data.prenom],
                "image_data":[data.image_data],
                "gender":[data.gender],
                "language":[data.language],
                "longitude":[data.longitude],
                "latitude":[data.latitude],
                "pays_id":[data.pays_id],
                "pays":[""],
                "ville":[data.ville],
                "adresse":[data.adresse],
                "email":[data.email,Validators.compose([Validators.pattern('^[a-zA-Z0-9_]+@[a-zA-Z0-9_.]+\\.[a-zA-Z]{2,5}')])],
                "oldPassword":[""],
                "newPassword":[""],
                "classe_id":[""],
                "school":[""],
                "annee":[""]
            });

        }

    }


    //Init the class form
    initClassForm(data){

        if(data){
            this.classeForm =  this.fb.group({
                "user_id":[""],
                "classe_id":[data.id],
                "school":[""],
                "annee":[""]
            });
        }

        this.getAcademicYear();

    }


    //Get the list of country
    listOfcountries(){

        this.storage.get('countries').then(countries=>{

            if(countries&&countries.length>0){
                this.pays=countries;
            }else {

                this.user.getCountries()
                    .then(  data  =>{

                        if(data&&data.reponse==="success"){
                            this.pays=data.pays;
                        }
                    }).catch(err=>{

                    });
            }
        });
    }

    //Get the list of classes
    listOfClasses(){

        this.storage.get('classes').then(data=>{
            if(data){
                this.classes=data;
            }else{
                this.classe.list()
                    .then(res=>{

                        if(res){
                            this.classes=res;
                        }

                    });
            }
        });
    }


    //Get the country name of a user
    getCountry(paysId:number) :string{

        let pays="";

        if(this.pays&&this.pays.length>0){
            for(let i=0;i<this.pays.length;i++){
                if(this.pays[i].id===paysId){
                    pays=this.pays[i].nom;
                }
            }
        }

        return pays;
    }

    //Get the user informations on the server
    getUserInfosByServer(){
        this.storage.get('token').then(token=>{

            this.user.userInfos(token)
                .subscribe(res => {

                if (res&&res.reponse=="success") {

                    this.userData = res;

                    if(this.userData){
                        this.initForm(this.userData.user);
                        this.initClassForm(this.userData.classes);
                        this.courses=this.userData.matieres;
                        this.niveaux=this.userData.niveaux;
                        this.notifications=this.userData.invitations;
                        this.countryName=this.getCountry(this.userData.user.pays_id);
                        this.imageBackgroung = this.userData.user.image_url;
                    }

                    if(this.imageBackgroung){

                        this.imageStyles ='http://mooexams.sdkgames.com/'+this.imageBackgroung;

                        this.imageServer=true;
                    }
                }
            },err=>{
                this.getUserInfo();
            });
        });
    }


    // Get all user information on the system
    getUserInfos(refresh,refresher){

        if(refresh) {
            this.getUserInfosByServer();
        }else{
            this.getUserInfo();
            if(this.userData&&(this.userData.length===0||this.userData=={})){
                this.getUserInfosByServer();
            }
        }
        if (refresher)
            refresher.complete();
    }


    // Select the picture or select on the gallery
    getPicture(){

        this.api.selectPicture().then(image=>{

            this.userForm.controls['image_data'].setValue(image);
            this.loader = this.loadingCtrl.create({
                content: '',
                dismissOnPageChange:true,
                duration: 60000
            });
            this.loader.present();

            this.storage.get('token').then(token=>{
                this.user.updateInfos(this.userForm.value,token)
                    .then(data=>{

                        this.loader.dismiss();
                        this.userData.user.image_url=data.user.image_url;
                        this.initForm(this.userData.user);

                    }).catch(err=>{

                        this.loader.dismiss();
                    });
            });

        }).catch(err=>{

            if(err==="none"){
                this.translate.get(['PICTURE_ERROR_MESSAGE']).subscribe(value=>{
                    this.user.toastAlert(value.PICTURE_ERROR_MESSAGE);
                });
            }

        });

    }



    //Show the Genre option
    radioListGender(){

        let alert = this.alertCtrl.create();

        this.translate.get(['GENDER']).subscribe(value=>{
            alert.setTitle(value.GENDER);
        });

        alert.addInput({
            type: 'radio',
            label: "M",
            value: "M",
            checked: false
        });

        alert.addInput({
            type: 'radio',
            label: "F",
            value: "F",
            checked: false
        });

        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: data => {

                if(data){
                    this.userForm.controls['gender'].setValue(data);
                    this.editAccount("gender");
                }
            }
        });

        alert.present();
    }


    //Show the Language option
    radioListLanguage(){

        let alert = this.alertCtrl.create();

        this.translate.get(['LANGUAGE']).subscribe(value=>{
            alert.setTitle(value.LANGUAGE);
        });

        alert.addInput({
            type: 'radio',
            label: "FR",
            value: "fr",
            checked: false
        });

        alert.addInput({
            type: 'radio',
            label: "EN",
            value: "en",
            checked: false
        });

        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: data => {

                if(data){
                    this.userForm.controls['language'].setValue(data);
                    this.userData.user.language=data;
                    this.translate.use(data);
                    this.editAccount("language");
                }
            }
        });

        alert.present();
    }


    //Show year option
    radioListAnnee(){

        let alert = this.alertCtrl.create();

        this.translate.get(['YEAR']).subscribe(value=>{
            alert.setTitle(value.YEAR);
        });

        let date = new Date();
        let an = date.getFullYear();
        let annee1=(an-1)+"-"+an;
        let annee2=an+"-"+(an+1);

        alert.addInput({
            type: 'radio',
            label: annee1,
            value: annee1,
            checked: false
        });

        alert.addInput({
            type: 'radio',
            label: annee2,
            value: annee2,
            checked: false
        });

        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: data => {

                if(data){
                    this.classeForm.controls['annee'].setValue(data);
                    this.editAccount("annee");
                }
            }
        });

        alert.present();
    }




    //Show the countries list
    radioListCountries(){

        let alert = this.alertCtrl.create();

        this.translate.get(['COUNTRY']).subscribe(value=>{
            alert.setTitle(value.COUNTRY);
        });


        for(let i=0;i<this.pays.length;i++){

            if(this.pays[i].nom){
                alert.addInput({
                    type: 'radio',
                    label: this.pays[i].nom,
                    value: this.pays[i].id,
                    checked: false
                });
            }

        }

        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: data => {

                if(data){
                    this.userForm.controls['pays_id'].setValue(data);
                    this.editAccount("pays");
                }
            }
        });

        alert.present();
    }

    //Show the classes list
    radioListClasse(){

        let alert = this.alertCtrl.create();

        this.translate.get(['SELECT_CLASS']).subscribe(value=>{
            alert.setTitle(value.SELECT_CLASS);
        });


        for(let i=0;i<this.classes.length;i++){

            if(this.classes[i].NOM_CLASSE){
                alert.addInput({
                    type: 'radio',
                    label: this.classes[i].OPTION_CLASSE?this.classes[i].NOM_CLASSE+" "+this.classes[i].OPTION_CLASSE:this.classes[i].NOM_CLASSE,
                    value: this.classes[i].id,
                    checked: false
                });
            }

        }

        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: data => {

                if(data){

                    this.classeForm.controls['classe_id'].setValue(data);
                    this.editAccount("classe");
                }
            }
        });

        alert.present();
    }




    //Get the  matiere of the post
    getCourseName(matiereId:number){


        let matiere="";
        let trouve=false,i=0;

        if(this.courses&&this.courses.length>0){

            while(!trouve&&i<this.courses.length){
                if(this.courses[i].id===matiereId){
                    matiere =this.courses[i].NOM_MATIERE;
                    trouve=true;
                }
                i++;
            }
        }
        return matiere.toUpperCase();
    }

    //Next format data with level and send to the view



    //Edit user infos
    showclassEdition(index:number){

        this.options[index].choix=true;
        this.showBoutton=true;

        if(this.options[index].value==="classe"){
            //La liste des classes
            this.listOfClasses();
            this.radioListClasse();
        }

        if(this.options[index].value==="annee"){
            //Les annees academiques
            this.radioListAnnee();
        }

        for(let i=0;i<this.options.length;i++){
            if(i!=index){
                this.options[i].choix=false;
            }
        }

    }


    showEdition(index:number){

        this.options[index].choix=true;
        this.showBoutton=true;

        this.initClassForm(this.userData.classes);
        this.initForm(this.userData.user);

        if(this.options[index].value=="password"){
            this.userForm.controls[this.options[index].value].setValue("");
        }

        if(this.options[index].value==="language"){
            //Afficher la liste des langues au choix
            this.radioListLanguage();
        }

        if(this.options[index].value==="pays"){
            //Afficher la liste des pays
            this.listOfcountries();
            this.radioListCountries();
        }

        if(this.options[index].value==="gender"){
            //Afficher le genre
            this.radioListGender();
        }


        for(let i=0;i<this.options.length;i++){
            if(i!=index){
                this.options[i].choix=false;
            }
        }

    }



    //Edit the user informations
    editAccount(option:string){

        switch (option){

            case "nom":
                if(this.userData.user.nom!=this.userForm.value.nom) {
                    this.storage.get('token').then(token => {

                        this.loader = this.loadingCtrl.create({
                            content: '',
                            dismissOnPageChange:true,
                            duration: 60000
                        });
                        this.loader.present();

                        this.user.updateInfos(this.userForm.value, token)
                            .then(data => {
                                this.loader.dismiss();
                                this.options[0].choix = false;
                                if(data&&data.reponse==='success') {
                                    this.userData.user.nom = data.user.nom;
                                    this.storage.set('user',data.user);
                                    this.updateUser(data.user);
                                }
                                this.initForm(this.userData.user);

                            }).catch(err => {
                                this.loader.dismiss();
                                this.initForm(this.userData.user);
                                if(err&&err.reponse==='failure') {
                                    this.translate.get(['MESSAGE_UPDATE_ERROR']).subscribe(data => {
                                        this.user.toastAlert(data.MESSAGE_UPDATE_ERROR);
                                    });
                                }
                            });
                    });
                }else{
                    this.options[0].choix = false;
                    this.initForm(this.userData.user);
                    this.loader.dismiss();
                }
                break;

            case "prenom":
                if(this.userData.user.prenom!=this.userForm.value.prenom) {
                    this.storage.get('token').then(token => {

                        this.loader = this.loadingCtrl.create({
                            content: '',
                            dismissOnPageChange:true,
                            duration: 60000
                        });
                        this.loader.present();
                        this.user.updateInfos(this.userForm.value, token)
                            .then(data => {
                                this.loader.dismiss();
                                this.options[1].choix = false;
                                if(data&&data.reponse==='success') {
                                    this.userData.user.prenom = data.user.prenom;
                                    this.storage.set('user',data.user);
                                    this.updateUser(data.user);
                                }
                                this.initForm(this.userData.user);

                            }).catch( err => {
                                this.loader.dismiss();
                                this.initForm(this.userData.user);
                                if(err&&err.reponse==='failure') {
                                    this.translate.get(['MESSAGE_UPDATE_ERROR']).subscribe(data => {
                                        this.user.toastAlert(data.MESSAGE_UPDATE_ERROR);
                                    });
                                }
                            });
                    });
                }else{
                    this.options[1].choix = false;
                    this.initForm(this.userData.user);
                    this.loader.dismiss();
                }
                break;

            case "email":

                if(this.userData.user.email!=this.userForm.value.email) {
                    this.storage.get('token').then(token => {

                        this.loader = this.loadingCtrl.create({
                            content: '',
                            dismissOnPageChange:true,
                            duration: 60000
                        });
                        this.loader.present();
                        this.user.updateInfos(this.userForm.value, token)
                            .then(data => {
                                this.loader.dismiss();
                                this.options[2].choix = false;
                                if(data&&data.reponse==='success') {
                                    this.userData.user.email = data.user.email;
                                    this.storage.set('user',data.user);
                                    this.updateUser(data.user);
                                }
                                this.initForm(this.userData.user);

                            }).catch(err => {
                                this.loader.dismiss();
                                this.initForm(this.userData.user);
                                if(err&&err.reponse==='failure') {
                                    this.translate.get(['MESSAGE_UPDATE_ERROR']).subscribe(data => {
                                        this.user.toastAlert(data.MESSAGE_UPDATE_ERROR);
                                    });
                                }
                            });
                    });
                }else{
                    this.options[2].choix = false;
                    this.initForm(this.userData.user);
                    this.loader.dismiss();
                }
                break;

            case "username":

                if(this.userData.user.username!=this.userForm.value.username) {
                    this.storage.get('token').then(token => {

                        this.loader = this.loadingCtrl.create({
                            content: '',
                            dismissOnPageChange:true,
                            duration: 60000
                        });
                        this.loader.present();
                        this.user.updateInfos(this.userForm.value, token)
                            .then(data => {
                                this.loader.dismiss();
                                this.options[3].choix = false;
                                if(data&&data.reponse==='success') {
                                    this.userData.user.username = data.user.username;
                                    this.storage.set('user',data.user);
                                    this.updateUser(data.user);
                                }
                                this.initForm(this.userData.user);

                            }).catch(err => {
                                this.loader.dismiss();
                                this.initForm(this.userData.user);
                                if(err&&err.reponse==='failure') {
                                    this.translate.get(['MESSAGE_UPDATE_ERROR']).subscribe(data => {
                                        this.user.toastAlert(data.MESSAGE_UPDATE_ERROR);
                                    });
                                }
                            });
                    });
                }else{
                    this.options[3].choix = false;
                    this.initForm(this.userData.user);
                    this.loader.dismiss();
                }
                break;

            case "tel":
                if(this.userData.user.tel!=this.userForm.value.tel) {
                    this.storage.get('token').then(token => {

                        this.loader = this.loadingCtrl.create({
                            content: '',
                            dismissOnPageChange:true,
                            duration: 60000
                        });
                        this.loader.present();
                        this.user.updateInfos(this.userForm.value, token)
                            .then(data => {
                                this.loader.dismiss();
                                this.options[4].choix = false;
                                if(data&&data.reponse==='success') {
                                    this.userData.user.tel = data.user.tel;
                                    this.storage.set('user',data.user);
                                    this.updateUser(data.user);
                                }
                                this.initForm(this.userData.user);

                            }).catch(err => {
                                this.loader.dismiss();
                                this.initForm(this.userData.user);
                                if(err&&err.reponse==='failure') {
                                    this.translate.get(['MESSAGE_UPDATE_ERROR']).subscribe(data => {
                                        this.user.toastAlert(data.MESSAGE_UPDATE_ERROR);
                                    });
                                }
                            });
                    });
                }else{
                    this.options[4].choix = false;
                    this.initForm(this.userData.user);
                    this.loader.dismiss();
                }
                break;

            case "language":
                if(this.userData.user.language!=this.userForm.value.language) {
                    this.storage.get('token').then(token => {

                        this.loader = this.loadingCtrl.create({
                            content: '',
                            dismissOnPageChange:true,
                            duration: 60000
                        });
                        this.loader.present();
                        this.user.updateInfos(this.userForm.value, token)
                            .then(data => {
                                this.loader.dismiss();
                                this.options[5].choix = false;
                                if(data&&data.reponse==='success') {
                                    this.userData.user.language = data.user.language;
                                    this.storage.set('user',data.user);
                                    this.updateUser(data.user);
                                }
                                this.initForm(this.userData.user);
                            }).catch( err => {
                                this.loader.dismiss();
                                this.initForm(this.userData.user);
                                if(err&&err.reponse==='failure') {
                                    this.translate.get(['MESSAGE_UPDATE_ERROR']).subscribe(data => {
                                        this.user.toastAlert(data.MESSAGE_UPDATE_ERROR);
                                    });
                                }
                            });
                    });
                }else{
                    this.options[5].choix = false;
                    this.initForm(this.userData.user);
                    this.loader.dismiss();
                }
                break;

            case "gender":
                if(this.userData.user.gender!=this.userForm.value.gender) {
                    this.storage.get('token').then(token => {

                        this.loader = this.loadingCtrl.create({
                            content: '',
                            dismissOnPageChange:true,
                            duration: 60000
                        });
                        this.loader.present();
                        this.user.updateInfos(this.userForm.value, token)
                            .then(data => {
                                this.options[6].choix = false;
                                this.loader.dismiss();
                                if(data&&data.reponse==='success') {
                                    this.userData.user.gender = data.user.gender;
                                    this.storage.set('user',data.user);
                                    this.updateUser(data.user);
                                }
                                this.initForm(this.userData.user);
                            }).catch(err => {
                                this.loader.dismiss();
                                this.initForm(this.userData.user);
                                if(err&&err.reponse==='failure') {
                                    this.translate.get(['MESSAGE_UPDATE_ERROR']).subscribe(data => {
                                        this.user.toastAlert(data.MESSAGE_UPDATE_ERROR);
                                    });
                                }
                            });
                    });
                }else{
                    this.options[6].choix = false;
                    this.initForm(this.userData.user);
                    this.loader.dismiss();
                }
                break;

            case "password":
                if(this.userForm.value.oldPassword!=""&&this.userForm.value.newPassword!=""&&this.userForm.value.oldPassword!=this.userForm.value.newPassword) {
                    this.storage.get('token').then(token => {

                        this.loader = this.loadingCtrl.create({
                            content: '',
                            dismissOnPageChange:true,
                            duration: 60000
                        });
                        this.loader.present();
                        this.user.updatePassword(this.userForm.value, token)
                            .then(data => {
                                this.loader.dismiss();
                                if(data&&data.reponse==='success') {
                                    this.options[7].choix = false;
                                    this.storage.set('password',this.userForm.value.newPassword);
                                }
                            }).catch(err => {
                                this.loader.dismiss();
                                if(err&&err.reponse==='failure') {
                                    this.translate.get(['MESSAGE_UPDATE_ERROR']).subscribe(data => {
                                        this.user.toastAlert(data.MESSAGE_UPDATE_ERROR);
                                    });
                                }
                            });
                    });
                }else{
                    this.options[7].choix = false;
                    this.loader.dismiss();
                }
                break;

            case "pays":
                if(this.userData.user.pays_id!=this.userForm.value.pays_id) {
                    this.storage.get('token').then(token => {

                        this.loader = this.loadingCtrl.create({
                            content: '',
                            dismissOnPageChange:true,
                            duration: 60000
                        });
                        this.loader.present();
                        this.user.updateInfos(this.userForm.value, token)
                            .then(data => {
                                this.loader.dismiss();
                                this.options[8].choix = false;
                                if(data&&data.reponse==='success') {
                                    this.userData.user.pays_id = data.user.pays_id;
                                    this.countryName = this.getCountry(data.user.pays_id);
                                    this.storage.set('user',data.user);
                                    this.updateUser(data.user);
                                }
                                this.initForm(this.userData.user);
                            }).catch(err => {
                                this.loader.dismiss();
                                this.initForm(this.userData.user);
                                if(err&&err.reponse==='failure') {
                                    this.translate.get(['MESSAGE_UPDATE_ERROR']).subscribe(data => {
                                        this.user.toastAlert(data.MESSAGE_UPDATE_ERROR);
                                    });
                                }
                            });
                    });
                }else{
                    this.options[8].choix = false;
                    this.initForm(this.userData.user);
                    this.loader.dismiss();
                }
                break;

            case "ville":
                if(this.userData.user.ville!=this.userForm.value.ville) {
                    this.storage.get('token').then(token => {

                        this.loader = this.loadingCtrl.create({
                            content: '',
                            dismissOnPageChange:true,
                            duration: 60000
                        });
                        this.loader.present();
                        this.user.updateInfos(this.userForm.value, token)
                            .then(data => {
                                this.loader.dismiss();
                                this.options[9].choix = false;
                                if(data&&data.reponse==='success') {
                                    this.userData.user.ville = data.user.ville;
                                    this.storage.set('user',data.user);
                                    this.updateUser(data.user);
                                }
                                this.initForm(this.userData.user);
                            }).catch(err => {
                                this.loader.dismiss();
                                this.initForm(this.userData.user);
                                if(err&&err.reponse==='failure') {
                                    this.translate.get(['MESSAGE_UPDATE_ERROR']).subscribe(data => {
                                        this.user.toastAlert(data.MESSAGE_UPDATE_ERROR);
                                    });
                                }
                            });
                    });
                }else{
                    this.options[9].choix = false;
                    this.initForm(this.userData.user);
                    this.loader.dismiss();
                }
                break;

            case "adresse":
                if(this.userData.user.adresse!=this.userForm.value.adresse) {
                    this.storage.get('token').then(token => {

                        this.loader = this.loadingCtrl.create({
                            content: '',
                            dismissOnPageChange:true,
                            duration: 60000
                        });
                        this.loader.present();
                        this.user.updateInfos(this.userForm.value, token)
                            .then(data => {
                                this.loader.dismiss();
                                this.options[10].choix = false;
                                if(data&&data.reponse==='success') {
                                    this.userData.user.adresse = data.user.adresse;
                                    this.storage.set('user',data.user);
                                    this.updateUser(data.user);
                                }
                                this.initForm(this.userData.user);
                            }).catch(err => {
                                this.loader.dismiss();
                                this.initForm(this.userData.user);
                                if(err&&err.reponse==='failure') {
                                    this.translate.get(['MESSAGE_UPDATE_ERROR']).subscribe(data => {
                                        this.user.toastAlert(data.MESSAGE_UPDATE_ERROR);
                                    });
                                }
                            });
                    });
                }else{
                    this.options[10].choix = false;
                    this.initForm(this.userData.user);
                    this.loader.dismiss();
                }
                break;

            case "classe":
                if(this.userData.classes.id!=this.classeForm.value.classe_id) {
                    this.storage.get('token').then(token=>{

                        this.loader = this.loadingCtrl.create({
                            content: '',
                            dismissOnPageChange:true,
                            duration: 60000
                        });
                        this.loader.present();

                        this.user.updateClasse(this.classeForm.value,token)
                            .then(data=>{

                                this.loader.dismiss();
                                this.options[11].choix=false;
                                if(data&&data.reponse=="success"){
                                    this.storage.set('user-classes',data.classes);
                                    this.userData.classes.id=this.classeForm.value.classe_id;
                                    this.userData.classes.NOM_CLASSE=data.classes.NOM_CLASSE;
                                    this.userData.classes.OPTION_CLASSE=data.classes.OPTION_CLASSE;
                                    this.initClassForm(this.userData.classes);
                                    this.updateClass(data.classes);
                                }else{
                                    this.initClassForm(this.userData.classes);
                                }

                            }).catch(err=>{

                                this.loader.dismiss();
                                this.initClassForm(this.userData.classes);
                                if(err&&err.reponse==='failure') {
                                    this.translate.get(['MESSAGE_UPDATE_CLASS_ERROR']).subscribe(data => {
                                        this.user.toastAlert(data.MESSAGE_UPDATE_CLASS_ERROR);
                                    });
                                }

                            });
                    });
                }else {
                    this.options[11].choix=false;
                    this.initClassForm(this.userData.classes);
                    this.loader.dismiss();
                }
                break;


            case "school":
                if(this.userData.classes.school!=this.classeForm.value.school) {
                    this.storage.get('token').then(token => {
                        this.loader = this.loadingCtrl.create({
                            content: '',
                            dismissOnPageChange:true,
                            duration: 60000
                        });
                        this.loader.present();
                        this.user.updateClasse(this.classeForm.value, token)
                            .then(data => {
                                this.loader.dismiss();
                                this.options[12].choix = false;
                                if(data&&data.reponse=="success"){
                                    this.userData.classes.school = data.userClasse.school;
                                    if(data.userClasse){
                                        this.storage.set('user-school',data.userClasse);
                                        this.initClassForm(this.userData.classes);
                                    }else{
                                        this.updateUserData();
                                    }
                                }else{
                                    this.initClassForm(this.userData.classes);
                                }

                            }).catch(err => {
                                this.loader.dismiss();
                                this.initClassForm(this.userData.classes);
                                if(err&&err.reponse==='failure') {
                                    this.translate.get(['MESSAGE_UPDATE_SCHOOL_ERROR']).subscribe(data => {
                                        this.user.toastAlert(data.MESSAGE_UPDATE_SCHOOL_ERROR);
                                    });
                                }
                            });
                    });
                }else{
                    this.options[12].choix = false;
                    this.initClassForm(this.userData.classes);
                    this.loader.dismiss();
                }
                break;

            case "annee":
                if(this.userData.classes.annee!=this.classeForm.value.annee) {
                    this.storage.get('token').then(token => {
                        this.loader = this.loadingCtrl.create({
                            content: '',
                            dismissOnPageChange:true,
                            duration: 60000
                        });
                        this.loader.present();
                        this.user.updateClasse(this.classeForm.value, token)
                            .then(data => {
                                this.loader.dismiss();
                                this.options[13].choix = false;
                                if(data&&data.reponse=="success"){
                                    this.storage.set('annee-academique', data.userClasse.annee);
                                    this.userData.classes.annee = data.userClasse.annee;
                                    this.initClassForm(this.userData.classes);
                                    if(data.userClasse){
                                        this.storage.set('user-school',data.userClasse);
                                    }else{
                                        this.updateUserData();
                                    }
                                }else{
                                    this.initClassForm(this.userData.classes);
                                }

                            }).catch(err => {
                                this.loader.dismiss();
                                this.initClassForm(this.userData.classes);
                                if(err&&err.reponse==='failure') {
                                    this.translate.get(['MESSAGE_UPDATE_ACADEMIC_YEAR_ERROR']).subscribe(data => {
                                        this.user.toastAlert(data.MESSAGE_UPDATE_ACADEMIC_YEAR_ERROR);
                                    });
                                }
                            });
                    });
                }else{
                    this.options[13].choix = false;
                    this.initClassForm(this.userData.classes);
                    this.loader.dismiss();
                }
                break;

            default:
                this.loader.dismiss();
                break;

        }

    }

    //Update the user Data
    updateUserData(){
        this.storage.get('token').then(token=>{
            this.user.userInfos(token);
        });
    }


    //time out loading
    timeout(){
        setTimeout(()=>{this.loader.dismiss()},30000);
    }

    //Log out the app
    logout(){

        this.loader = this.loadingCtrl.create({
            content: '',
            dismissOnPageChange:true,
            duration: 60000
        });
        this.user.logout();
        this.navCtrl.push(LoaderPage);
        this.loader.present();
        this.timeout();
    }

    //Format the time

    formatTime(date:any) : string{

        let result = "";

        if(date) {

            let currentDate = new Date();
            let notifDate = new Date(date);
            let nbreDate1 = 0,heure=0,minute=0,seconde=0, nbreDate2 = 0, tempsEcoule;

            if (currentDate.getDay() === notifDate.getDay()) {

                nbreDate1 = Math.ceil(currentDate.getTime()/1000);
                nbreDate2 = Math.ceil(notifDate.getTime()/1000);
                tempsEcoule =nbreDate1 - nbreDate2;
                if(tempsEcoule>3600){
                     heure = Math.floor(tempsEcoule/3600);
                     minute = Math.floor((tempsEcoule%3600)/60);
                     seconde=(tempsEcoule%3600)%60;
                }else if(tempsEcoule>60){
                     minute = Math.floor(tempsEcoule/60);
                     seconde=tempsEcoule%60;
                }else{
                    seconde=tempsEcoule;
                }

                result = heure + "h " + minute + "m " + seconde + "s ago";
            } else {
                result = notifDate.getFullYear()+"-"+(notifDate.getMonth()+1)+"-"+notifDate.getDate();
            }
        }

        return result;

    }


    //Get the list of invitations

    getInvitationsList(){

        this.storage.get('token').then(token=>{
            this.user.invitations(token)
                .then(data=>{
                    if(data&&data.invitations.length>0){
                        this.notifications=data.invitations;
                    }
                });
        });

    }




    //Accepted the invitations

    accepted(data:any){


        data.status=1;
        data.invitation_id=data.id;
        let param={"invitations":[data]};

        this.loader = this.loadingCtrl.create({
            content: '',
            dismissOnPageChange:true,
            duration: 60000
        });
        this.loader.present();

        this.storage.get('token').then(token=>{
            this.user.acceptedInvitation(param,token)
                .then(data=>{


                    if(data.reponse==="success"){

                        this.notifications.splice(this.notifications.indexOf(data),1);
                        this.user.invitations(token);
                        this.updateBadge(this.challenges,this.notifications);
                    }
                    this.loader.dismiss();

                }).catch(err=>{
                    this.loader.dismiss();
                });
        });

    }

    //Accepted the invitations

    deleteInvitations(data,slidingItem: ItemSliding){

        slidingItem.close();

        data.status=2;
        data.invitation_id=data.id;
        let param={"invitations":[data]};

        this.loader = this.loadingCtrl.create({
            content: '',
            dismissOnPageChange:true,
            duration: 60000
        });
        this.loader.present();

        this.storage.get('token').then(token=>{
            this.user.acceptedInvitation(param,token)
                .then(data=>{

                    if(data.reponse==="success"){
                        this.notifications.splice(this.notifications.indexOf(data),1);
                        this.user.invitations(token);
                        this.updateBadge(this.challenges,this.notifications);
                    }
                    this.loader.dismiss();
                }).catch(err=>{
                    this.loader.dismiss();
                });
        });

    }



    //Get the list of challenges
    getChallengesFromServer(){

        this.storage.get('token').then(token=>{

            let param={"token":token};

            this.storage.get('user').then(user=>{

                if(user) {

                    param["user_id"] = user.id;

                    this.challenge.getChallengeInvited(param, param.token)
                        .then(data => {

                            if (data && data.reponse == "success") {
                                if (data.challenge)
                                    this.challenges = data.challenge;
                                if (data.users)
                                    this.usersChallenge = data.users;
                                if (data.matieres)
                                    this.matieresChallenge = data.matieres;
                                if (data.classes)
                                    this.classe = data.classes;
                                if (data.participants)
                                    this.participantsChallenge = data.participants;
                                if (data.userClasse)
                                    this.schoolsChallenge = data.userClasse;
                            }

                            console.log(this.challenges);

                        }).catch(err => {

                            this.getChallengesFromLocal();
                        });
                }
            });

        });
    }



    //Get the list form local
    getChallengesFromLocal() {

        this.storage.get('challenges-invites').then(data=>{
            if(data){
                if(data.challenge)
                    this.challenges = data.challenge;
                if(data.users)
                    this.usersChallenge=data.users;
                if(data.matieres)
                    this.matieresChallenge=data.matieres;
                if(data.classes)
                    this.classeChallenge=data.classes;
                if(data.participants)
                    this.participantsChallenge=data.participants;

                console.log(this.challenges);
            }
        });
    }
    //Get the list of challenges
    getChallenges(refresher,refresh){

        if(refresh){
            this.getChallengesFromServer();
        }else{
            this.getChallengesFromLocal();
            if(this.challenges&&this.challenges.length==0){
                this.getChallengesFromServer();
            }
        }

        if(refresher!=0)
            refresher.complete();

    }



    //Participate to a challenge
    participedToChallenge(index,challenge){

        this.storage.get('user').then(user=>{

            let data={"user_id":user.id,"challenge_id":challenge.id_challenge};
            this.loader = this.loadingCtrl.create({
                content: '',
                dismissOnPageChange:true,
                duration: 60000
            });

            this.storage.get('token').then(token=>{


                this.challenge.acceptedInvitations(data,token)
                    .then(reponse=>{


                        if(reponse&&reponse.reponse=="success"){
                            this.challenges.splice(index,1) ;
                            this.getChallengesFromServer();
                            this.updateBadge(this.challenges,this.notifications);

                        }
                        this.loader.dismiss();

                    }).catch(err=>{

                        this.loader.dismiss();
                        if(err&&err.reponse=="failure"){

                            this.translate.get(['CHALLENGES_ERROR_DECILNE']).subscribe(value=>{
                                this.user.toastAlert(value.CHALLENGES_ERROR_DECILNE);
                            });
                        }
                    });
            });

        });
    }


    //Decline the invitation to a challenge
    declineInvitation(index,challenge){

        this.storage.get('user').then(user=>{

            let data={"user_id":user.id,"challenge_id":challenge.id_challenge};
            this.loader = this.loadingCtrl.create({
                content: '',
                dismissOnPageChange:true,
                duration: 60000
            });
            this.loader.present();

            this.storage.get('token').then(token=>{

                this.challenge.refusedInvitations(data,token)
                    .then(data=>{


                        if(data&&data.reponse=="success"){
                            this.challenges.splice(index,1) ;
                            this.getChallengesFromServer();
                            this.updateBadge(this.challenges,this.notifications);
                        }
                        this.loader.dismiss();

                    }).catch(err=>{

                        this.loader.dismiss();
                        if(err&&err.reponse=="failure"){

                            this.translate.get(['CHALLENGES_ERROR_DECILNE']).subscribe(value=>{
                                this.user.toastAlert(value.CHALLENGES_ERROR_DECILNE);
                            });
                        }
                    });
            });

        });

    }


}
