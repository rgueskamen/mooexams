import { Component,ViewChild, ElementRef} from '@angular/core';
import { LoadingController, ModalController, NavController, NavParams,Content } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { ApiProvider } from "../../providers/api/api";
import { QuestionsProvider } from "../../providers/questions/questions";
import { Question } from '../../models/question';
import { ScoreProvider } from "../../providers/score/score";
import { ChallengeProvider } from "../../providers/challenge/challenge";
import { ChallengeResultPage } from "../challenge-result/challenge-result";
import { TranslateService } from "@ngx-translate/core";
import { UserProvider } from "../../providers/user/user";
import { ChallengePage} from "../challenge/challenge";
import {HomePage} from "../home/home";

import { ModalSuccessPage } from '../modal-success/modal-success';
import { ModalFailedPage } from './../modal-failed/modal-failed';


/**
 * Generated class for the QuestionChallengePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

interface QuestionTimer{

    value:number;
    points:number;
    border:string;
    color:string;
    background:string;
    time:number;
    percent:number
}

@Component({
    selector: 'page-question-challenge',
    templateUrl: 'question-challenge.html',
})
export class QuestionChallengePage {


    picture_logo: string;
    picture_revision: string;
    picture_light: string;
    options: any;
    questionnaires: Array<Question>;
    answers: number[];
    confirmAnswer: boolean[];
    slideShow: number;
    score: { failed: number, success: number, points: number };
    progressPercent: number;
    interval: any;
    labels: string[];
    url: string;
    answersTimer: Array<QuestionTimer>;
    timerPosition: number;
    serveurReponse: boolean;
    currentsPoints:number;
    loader:any;

    @ViewChild(Content) content : Content;


    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public storage: Storage,
                public api: ApiProvider,
                public questions: QuestionsProvider,
                public resultat: ScoreProvider,
                public modalCtrl: ModalController,
                public challenge: ChallengeProvider,
                public translate: TranslateService,
                public userService: UserProvider,
                public loading: LoadingController,
                public element: ElementRef) {
        this.picture_logo = "assets/img/MooExams_pre.png";
        this.picture_revision = "assets/img/mode_revision.png";
        this.picture_light = "assets/img/light.png";
        this.options = this.navParams.get('params');
        this.questionnaires = [];
        this.slideShow = -1;
        this.answers = [];
        this.answersTimer = [];
        this.confirmAnswer = [];
        this.score = {failed: 0, success: 0, points: 0};
        this.progressPercent = 53;
        this.interval = null;
        this.labels = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];
        this.url = "http://mooexams.sdkgames.com/";
        this.timerPosition = 0;
        this.serveurReponse = false;
        this.currentsPoints=0;

    }


    ionViewDidLoad() {
        this.getQuestions();
        this.initTimer();
    }

    ionViewWillLeave(){
        this.clearTimer();
    }


    ionViewDidLeave(){
        this.clearTimer();
    }


    //Clear the timer

    clearTimer(){
        clearInterval(this.interval);
        this.interval=null;

        for(let i=0;i<1000;i++){
            clearInterval(i);
        }
    }


    initTimer() {

        this.answersTimer.push({
            value: 5,
            points: 5,
            border: "#3cb44b",
            color: "#fff",
            background: "rgba(60, 180, 75,0.2)",
            time: 30000,
            percent: 100
        });
        this.answersTimer.push({
            value: 4,
            points: 4,
            border: "#3cb44b",
            color: "#fff",
            background: "rgba(60, 180, 75,0.2)",
            time: 30000,
            percent: 80
        });
        this.answersTimer.push({
            value: 3,
            points: 3,
            border: "#f58231",
            color: "#fff",
            background: "rgba(245, 130, 48,0.2)",
            time: 30000,
            percent: 60
        });
        this.answersTimer.push({
            value: 2,
            points: 2,
            border: "#f58231",
            color: "#fff",
            background: "rgba(245, 130, 48,0.2)",
            time: 30000,
            percent: 40
        });
        this.answersTimer.push({
            value: 1,
            points: 1,
            border: "#e6194b",
            color: "#fff",
            background: "rgba(230, 25, 75,0.2)",
            time: 30000,
            percent: 20
        });
        this.answersTimer.push({
            value: 0,
            points: 1,
            border: "#e6194b",
            color: "#fff",
            background: "rgba(230, 25, 75,0.2)",
            time: 100,
            percent: 0
        });
    }


    setTimer(index) {

        this.currentsPoints = this.answersTimer[0].points;

        this.interval = setInterval(() => {

            this.timerPosition++;
            if (this.timerPosition < 6) {
                this.currentsPoints = this.answersTimer[this.timerPosition].points;
            }
            if (this.timerPosition == 6) {

                this.timerPosition = 0;
                if (!this.questionnaires[index].choix) {
                    this.answers[index] = 0;
                }
                this.confirm(index);
              //  this.next(index);
                clearInterval(this.interval);
            }

        }, this.answersTimer[this.timerPosition].time);

    }


    //Start to answer question
    begin() {
        this.slideShow = 0;
        this.score = {failed: 0, success: 0, points: 0};
        this.setTimer(0);
        //init array of id answers
        for (let i = 0; i < this.questionnaires.length; i++) {
            this.answers.push(0);
            this.confirmAnswer.push(false);
        }
        this.content.resize();
    }


    //Next question
    next(index: number) {

        this.timerPosition = 0;
        clearInterval(this.interval);
        this.interval=null;
        //Afficher la page de resultats
        (this.questionnaires[index].bonne_reponse.id === this.answers[index]) ? this.score.success++ : this.score.failed++;
        if(this.questionnaires[index].bonne_reponse.id===this.answers[index]) this.score.points+=this.currentsPoints;
        this.currentsPoints=0;
        if (this.slideShow !== (this.questionnaires.length - 1)) {
            this.slideShow++;
            this.content.resize();
            this.setTimer(index + 1);
        } else {
            this.save(this.questionnaires);
        }
    }

    //the choice make by a user
    selectProposition(index: number, propo: number) {
        this.questionnaires[index].choix = true;
        if (this.questionnaires[index].bonne_reponse.id === propo) {
            this.answers[index] = propo;
        } else {
            this.answers[index] = 0;
        }
    }

        //Open the modal  success
        openModalSuccess(index){

            //Play success sound
            let modal = this.modalCtrl.create(ModalSuccessPage,{question:this.questionnaires[index]});
    
            modal.onDidDismiss(data => {
                this.next(index);
            });
            modal.present();
        }
    
    

        //Open the modal failed
        openModalEchec(index){
    
             //Play Failure sound
            let modal = this.modalCtrl.create(ModalFailedPage,{question:this.questionnaires[index]});
    
            modal.onDidDismiss(data => {
                this.next(index);
            });
    
            modal.present();
        }

    //confirm the user choice
    confirm(index) {
        this.confirmAnswer[index] = true;

        if(this.answers[index]==0){
            this.openModalEchec(index);
        }else{
            this.openModalSuccess(index);
        }
    }


    //Select the number of questions
    getQuestions() {

        this.storage.get('token').then(token => {

           /* this.loader = this.loading.create({
                content:'',
                dismissOnPageChange:true
            });

            this.loader.present();*/

            this.questions.user_challenge_questions(this.options, token)

                .then(questions => {

                    this.serveurReponse = true;
                   // this.loader.dismiss();
                    if (questions && questions.length>0) {
                        this.questionnaires = questions;
                    } else {
                        this.questionnaires = [];
                        this.translate.get(['CHALLENGE_QUESTIONS_MESSAGE']).subscribe(value => {
                            this.userService.toastAlert(value.CHALLENGE_QUESTIONS_MESSAGE);
                        });
                    }
                }).catch(err => {

                   // this.loader.dismiss();
                    this.serveurReponse = true;
                    if (err && err.reponse == "failure") {
                        this.translate.get(['GET_QUESTIONS_FAILED']).subscribe(value => {
                            this.userService.toastAlert(value.GET_QUESTIONS_FAILED);
                        });
                    }
                });

        });
    }


    //Format the data
    format(chaine: string) {

        let ch = String(chaine);
        let resultat = ch.replace(/\\'/g, "'");
        resultat = resultat.replace(/\\"/g, '"');
        return resultat;
    }


    //save the result of the challenge
    save(data: any) {

        let param = {
            "id_user": "",
            "id_challenge": "",
            "bonne_reponse": 0,
            "nbrePoints": 0,
            "mauvaise_reponse": 0,
            "liste_bonne_reponse": "",
            "liste_mauvaise_reponse": ""

        };

        clearInterval(this.interval);
        this.interval=null;
        this.currentsPoints=0;
        this.timerPosition = 0;

        for(let i=0;i<1000;i++){
            clearInterval(i);
        }

        this.storage.get('user').then(user => {

            param.id_user = user.id;
            param.id_challenge = this.options.id_challenge;
            param.bonne_reponse = this.score.success;
            param.mauvaise_reponse = this.score.failed;
            param.nbrePoints = this.score.points;

            let liste_bonne_reponse = "";
            let liste_mauvaise_reponse = "";

            for (let i = 0; i < this.answers.length; i++) {

                if (this.answers[i] == 0) {
                    if(data[i]&&data[i].value) {
                        if (liste_mauvaise_reponse == "") {
                            liste_mauvaise_reponse = data[i].value;
                        } else {
                            liste_mauvaise_reponse += "," + data[i].value;
                        }
                    }

                } else {

                    if(data[i]&&data[i].value) {
                        if (liste_bonne_reponse == "") {
                            liste_bonne_reponse = data[i].value;
                        } else {
                            liste_bonne_reponse += "," + data[i].value;
                        }
                    }
                }
            }

            param.liste_bonne_reponse = liste_bonne_reponse;
            param.liste_mauvaise_reponse = liste_mauvaise_reponse;


            this.storage.get('token').then(token => {

                this.loader = this.loading.create({
                    content: '',
                    dismissOnPageChange: true,
                    duration: 60000
                });

                this.loader.present();

                this.challenge.saveScore(param, token)
                    .then(data => {

                        this.loader.dismiss();
                        if (data && data.reponse == "saved") {
                            this.navCtrl.setRoot(ChallengeResultPage);
                        }

                    }).catch( err => {

                        this.loader.dismiss();
                        if (data && data.reponse == "failure") {
                            this.translate.get(['CHALLENGE_SAVE_RESULT_FAILED']).subscribe(value => {
                                this.userService.toastAlert(value.CHALLENGE_SAVE_RESULT_FAILED);
                            });
                        }

                        this.navCtrl.setRoot(ChallengePage);
                    });
            });

        });

    }


    //Go to the DashBoard
    gotoDash(){
        this.navCtrl.setRoot(HomePage);
    }

    //when we quit the page delete the timer

}
