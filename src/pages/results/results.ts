import { Component } from '@angular/core';
import {ModalController, NavController, NavParams} from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { QuestionRevisionPage } from "../question-revision/question-revision";
import {LeconsProvider} from "../../providers/lecons/lecons";
import {ModalBadgePage} from "../modal-badge/modal-badge";
import {ProfilPage} from "../profil/profil";

/**
 * Generated class for the ResultsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-results',
  templateUrl: 'results.html',
})

export class ResultsPage {

    picture_logo:string;
    choix:string;
    resultats:any;

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public lessonService:LeconsProvider,
      public storage:Storage,
      public modalCtrl:ModalController
  ) {

      this.picture_logo="assets/img/MooExams_pre.png";
      this.choix="recapitulatif";
      this.resultats=this.navParams.get('resultats');
  }

  ionViewDidLoad() {
      this.getUserLessonLevel();
  }

    GotoRecap(choix:string){
      this.choix=choix;
    }

    GotoPrev(choix:string){
        this.choix=choix;
        this.navCtrl.setRoot(QuestionRevisionPage,{params:this.resultats.param});
    }

    //Go to the profile page
    gotoProfile(){
        this.navCtrl.setRoot(ProfilPage);
    }


    //get user level in Lesson
    getUserLessonLevel(){

        this.storage.get('token').then(token=>{

            this.storage.get('user').then(user=>{

                let param={"user_id":user.id,"lecon_id":this.resultats.param.id_lecon,"matiere_id":this.resultats.param.id_matiere,"annee":this.resultats.param.annee};
                this.lessonService.userlessonsLevel(param,token)
                    .then(data=>{

                        if(data&&data.reponse=="success"&&data.niveau){

                            if(data.niveau.id_niveau!=this.resultats.param.id_niveau){
                                this.openBadgeMessage(user,data.niveau);
                                this.updateLevels();
                            }
                        }

                    }).catch(err=>{

                    });
            });

        });

    }

    //Show the level of the user
    openBadgeMessage(user,niveau){

        let modal = this.modalCtrl.create(ModalBadgePage,{resultats:{user:user,level:niveau}});

        modal.onDidDismiss(data => {

        });

        modal.present();
    }


    //update the level of the user in the subjets
    updateLevels(){
        this.storage.get('token').then(token=> {

            this.storage.get('user').then(user => {

                this.storage.get('user-school').then(school=>{

                    this.storage.get('user-classes').then(classe=>{
                        let param = {"user_id":user.id,"classe_id":classe.id,"matiere_id":this.resultats.param.id_matiere,"lecon_id":this.resultats.param.id_lecon,"annee":school.annee};
                        this.lessonService.lessonsLevels(param,token);
                    });

                });

            });
        });

    }


}
