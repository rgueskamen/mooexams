import { Storage } from '@ionic/storage';
import { ProfilPage } from './../profil/profil';
import { UserProvider } from './../../providers/user/user';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

/**
 * Generated class for the SurveyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-survey',
  templateUrl: 'survey.html',
})
export class SurveyPage {
  loginLoader:any;

  constructor(
    public navCtrl: NavController, 
    public storage:Storage,
    public userService : UserProvider,
    public navParams: NavParams,
    private iab: InAppBrowser,
    public loadingCtrl : LoadingController
  ) {
  }

  ionViewDidLoad() {
    
  }

  //Get the code of MooExams survey

getMooexamsCode(){

    //Login loader
    this.loginLoader = this.loadingCtrl.create({
      content: '',
      dismissOnPageChange:true,
      duration: 60000
  });

  this.loginLoader.present();

  this.storage.get('token').then(token=>{
       this.userService.getSurveycode(token).then(survey=>{
          this.loginLoader.dismiss();
       }).catch(err=>{
          this.loginLoader.dismiss();
       });
  });
     
}

goProfile(){
  this.navCtrl.push(ProfilPage);
}

//Go to play store
download(){

  const browser = this.iab.create('https://goo.gl/CKQCV8');
  
  browser.show();
}



}
