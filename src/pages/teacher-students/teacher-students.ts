import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { UserProvider} from "../../providers/user/user";
import { Storage } from "@ionic/storage";
import {MyCoursePage} from "../my-course/my-course";
import {MyStudentsPage} from "../my-students/my-students";

/**
 * Generated class for the TeacherStudentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
    selector: 'page-teacher-students',
    templateUrl: 'teacher-students.html',
})
export class TeacherStudentsPage {

    courses:any;
    students:any;
    classes:any;
    userClasses:any;
    serveurReponse:boolean;
    backgroundColors:string[];
    logo:string;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public userService:UserProvider,
        public storage:Storage
    ) {

        this.courses=[];
        this.logo="assets/img/MooExams_pre.png";
        this.students=[];
        this.classes=[];
        this.userClasses=[];
        this.serveurReponse=false;
        this.backgroundColors=["rgba(128, 128, 128,0.2)","rgba(60, 180, 75,0.2)","rgba(255, 215, 180,0.2)","rgba(70, 240, 240,0.2)"];
    }

    ionViewDidLoad() {
        this.getUsersCourses(0,false);
    }

    //go to the profile

    gotoCourses(){
      this.navCtrl.setRoot(MyCoursePage);
    }


    //Get the list of users By courses from local
    getUsersByCoursesFromLocal(){
        this.storage.get('user-courses').then(courses=>{

            if(courses){
                if(courses.matieres)
                    this.courses=courses.matieres;
                if(courses.students)
                    this.students=courses.students;
                if(courses.classes)
                    this.classes=courses.classes;
                if(courses.userClasse)
                    this.userClasses=courses.userClasse;
            }else{
                this.courses=[];
                this.students=[];
                this.classes=[];
                this.userClasses=[];
            }

            this.serveurReponse=true;

        });
    }


    //Get the list of users By courses from server
    getUsersByCoursesFromServer(){
        this.storage.get('token').then(token=>{
            this.userService.getAllUsersByCourses(token)
                .then(data=>{

                    if(data&&data.reponse=="success"){
                        if(data.matieres)
                            this.courses=data.matieres;
                        if(data.students)
                            this.students=data.students;
                        if(data.classes)
                            this.classes=data.classes;
                        if(data.userClasse)
                            this.userClasses=data.userClasse;
                    }
                    this.serveurReponse=true;

                }).catch(err=>{
                    this.getUsersByCoursesFromLocal();
                    this.serveurReponse=true;
                })
        });
    }

    //Get all courses with users
    getUsersCourses(refresher,refresh){

        if(refresh){
            this.getUsersByCoursesFromServer();
        }else{
            this.getUsersByCoursesFromLocal();
            if(this.courses&&this.courses.length==0){
                this.getUsersByCoursesFromServer();
            }
        }

        if(refresher)
            refresher.complete();

    }

    //Go to the users courses page
    gotoUsersPage(course){
        if(this.students&&this.students.length>0){
            this.navCtrl.push(MyStudentsPage,{course:course,students:this.students[course.id],classes:this.classes,userClasses:this.userClasses});
        }else{
            this.navCtrl.push(MyStudentsPage,{course:course,students:[],classes:[],userClasses:[]});
        }
    }

}
