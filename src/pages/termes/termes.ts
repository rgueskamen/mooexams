import { Component } from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { TranslateService } from "@ngx-translate/core";

/**
 * Generated class for the TermesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@Component({
    selector: 'page-termes',
    templateUrl: 'termes.html'
})
export class TermesPage {

    language:string;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public storage:Storage,
        public translate:TranslateService,
        public viewCtrl:ViewController
    ) {
        this.language="fr";
    }


    ionViewDidLoad() {
        this.getLanguage();
    }


    //get system Language
    getLanguage(){

        if (this.translate.getBrowserLang() !== undefined) {
            this.language=this.translate.getBrowserLang();
        }

    }

    close(){
        this.viewCtrl.dismiss();
    }


}
