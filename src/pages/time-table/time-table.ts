import { Component, ElementRef ,Renderer} from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { TimetableProvider } from "../../providers/timetable/timetable";
import { Storage } from "@ionic/storage";
import { TranslateService } from "@ngx-translate/core";
import { DatePicker } from '@ionic-native/date-picker';
import { UserProvider } from "../../providers/user/user";
import {ProfilPage} from "../profil/profil";


/**
 * Generated class for the TimeTablePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
    selector: 'page-time-table',
    templateUrl: 'time-table.html',
})
export class TimeTablePage {

    
    timetable:any;
    user:any;
    logo:string;
    daysOfWeek:any;
    selectDays:boolean;
    unselectDays:boolean;
    sameTime:boolean;
    distinctTime:boolean;
    repeatTime:boolean;
    repeatDay:boolean;
    timeChecked:boolean;
    defaultTime:Date;
    showDaysCard:boolean;
    showTimeCard:boolean;
    canUpated:boolean;
    annee:string;
    serveurReponse:boolean;
    loginLoader:any;
    elementHtml;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public time : TimetableProvider,
        private translate : TranslateService,
        private datePicker: DatePicker,
        public alertCtrl: AlertController,
        private storage : Storage,
        public userService : UserProvider,
        public loadingCtrl: LoadingController,
        public element: ElementRef, 
        public renderer: Renderer

    ) {

        this.logo="assets/img/MooExams_pre.png";
        this.defaultTime=new Date();
        this.timeChecked=true;
        this.showDaysCard=true;
        this.showTimeCard=true;
        this.canUpated=false;
        this.serveurReponse=false;
        


        //We get the user informations
        this.storage.get('user').then(data=>{
            this.user=data;
        });

        //We get the academic year
        this.storage.get('user-school').then(data=>{
            this.annee=data.annee;
        });

        this.translate.get(['DAYS1','DAYS2','DAYS3','DAYS4','DAYS5','DAYS6','DAYS7']).subscribe((value) => {

            let defaultDate = new Date();

            this.daysOfWeek=[
                {choix:false,jour:0,title:value.DAYS1,heure_debut:defaultDate,heure_fin:defaultDate,heure:defaultDate.getHours(),minute:defaultDate.getMinutes(),active:0,reminder:0,repeat:0},
                {choix:false,jour:1,title:value.DAYS2,heure_debut:defaultDate,heure_fin:defaultDate,heure:defaultDate.getHours(),minute:defaultDate.getMinutes(),active:0,reminder:0,repeat:0},
                {choix:false,jour:2,title:value.DAYS3,heure_debut:defaultDate,heure_fin:defaultDate,heure:defaultDate.getHours(),minute:defaultDate.getMinutes(),active:0,reminder:0,repeat:0},
                {choix:false,jour:3,title:value.DAYS4,heure_debut:defaultDate,heure_fin:defaultDate,heure:defaultDate.getHours(),minute:defaultDate.getMinutes(),active:0,reminder:0,repeat:0},
                {choix:false,jour:4,title:value.DAYS5,heure_debut:defaultDate,heure_fin:defaultDate,heure:defaultDate.getHours(),minute:defaultDate.getMinutes(),active:0,reminder:0,repeat:0},
                {choix:false,jour:5,title:value.DAYS6,heure_debut:defaultDate,heure_fin:defaultDate,heure:defaultDate.getHours(),minute:defaultDate.getMinutes(),active:0,reminder:0,repeat:0},
                {choix:false,jour:6,title:value.DAYS7,heure_debut:defaultDate,heure_fin:defaultDate,heure:defaultDate.getHours(),minute:defaultDate.getMinutes(),active:0,reminder:0,repeat:0}
            ];
        });


        this.selectDays=false;
        this.unselectDays=false;
        this.sameTime=false;
        this.distinctTime=false;
        this.repeatTime=false;
        this.repeatDay=false;

    }



    ionViewDidLoad() {
        this.listTimeTable(false,0);
    }


    //Set the default value of the timetable
    initTimetable(timetable:any){

        let temporyDate1="", temporyDate2="", temporyDate3="", temporyDate4="",heure_debut="",heure_fin="",date_debut="",date_fin="";

        if(timetable&&timetable.length>0){


            for(let i=0;i<timetable.length;i++){

                if(this.timetable[i]){

                    timetable[i].active===1?this.daysOfWeek[timetable[i].jour].choix=true:this.daysOfWeek[timetable[i].jour].choix=false;

                    this.daysOfWeek[timetable[i].jour].active=timetable[i].active;
                    this.daysOfWeek[timetable[i].jour].repeat=timetable[i].repeat;
                    this.daysOfWeek[timetable[i].jour].reminder=timetable[i].reminder;
                    this.daysOfWeek[timetable[i].jour].id=timetable[i].id;

                    temporyDate1=timetable[i].heure_debut;
                    date_debut=temporyDate1.split(' ')[0];

                    temporyDate2=timetable[i].heure_fin;
                    date_fin=temporyDate2.split(' ')[0];

                    temporyDate3=timetable[i].heure_debut;
                    heure_debut=temporyDate3.split(' ')[1];

                    temporyDate4=timetable[i].heure_fin;
                    heure_fin=temporyDate4.split(' ')[1];

                    this.daysOfWeek[timetable[i].jour].heure_debut=new Date(parseInt(date_debut.split('-')[0]),parseInt(date_debut.split('-')[1]),parseInt(date_debut.split('-')[2]),parseInt(heure_debut.split(':')[0]),parseInt(heure_debut.split(':')[1]),parseInt(heure_debut.split(':')[2]),0);
                    this.daysOfWeek[timetable[i].jour].heure_fin=  new Date(parseInt(date_fin.split('-')[0]),parseInt(date_fin.split('-')[1]),parseInt(date_fin.split('-')[2]),parseInt(heure_fin.split(':')[0]),parseInt(heure_fin.split(':')[1]),parseInt(heure_fin.split(':')[2]),0);

                    this.daysOfWeek[timetable[i].jour].heure=heure_debut.split(':')[0];
                    this.daysOfWeek[timetable[i].jour].minute=heure_debut.split(':')[1];
                }

                if(timetable[i].repeat==1){
                    this.repeatDay=true;
                }

                if(timetable[i].reminder==1){
                    this.repeatTime=true;
                }

            }

            this.canUpated=true;
        }


    }

    //Hide or shw the days panel
    hideShowDaysCard(){
        this.showDaysCard?this.showDaysCard=false:this.showDaysCard=true;
    }


    //hide or show the time pannel
    hideShowTimeCard(){
        this.showTimeCard ?this.showTimeCard=false:this.showTimeCard=true;
    }


    //Set the days the user has selected
    selectDay(day:number){

        switch (day){
            case 0:
                this.daysOfWeek[0].choix ? this.daysOfWeek[0].choix=false :this.daysOfWeek[0].choix=true;
                this.daysOfWeek[0].choix ? this.daysOfWeek[0].active=1 :this.daysOfWeek[0].active=0;
                break;
            case 1:
                this.daysOfWeek[1].choix ? this.daysOfWeek[1].choix=false :this.daysOfWeek[1].choix=true;
                this.daysOfWeek[1].choix ? this.daysOfWeek[1].active=1 :this.daysOfWeek[1].active=0;
                break;
            case 2:
                this.daysOfWeek[2].choix ? this.daysOfWeek[2].choix=false :this.daysOfWeek[2].choix=true;
                this.daysOfWeek[2].choix ? this.daysOfWeek[2].active=1 :this.daysOfWeek[2].active=0;
                break;
            case 3:
                this.daysOfWeek[3].choix ? this.daysOfWeek[3].choix=false :this.daysOfWeek[3].choix=true;
                this.daysOfWeek[3].choix ? this.daysOfWeek[3].active=1 :this.daysOfWeek[3].active=0;
                break;
            case 4:
                this.daysOfWeek[4].choix ? this.daysOfWeek[4].choix=false :this.daysOfWeek[4].choix=true;
                this.daysOfWeek[4].choix ? this.daysOfWeek[4].active=1 :this.daysOfWeek[4].active=0;
                break;
            case 5:
                this.daysOfWeek[5].choix ? this.daysOfWeek[5].choix=false :this.daysOfWeek[5].choix=true;
                this.daysOfWeek[5].choix ? this.daysOfWeek[5].active=1 :this.daysOfWeek[5].active=0;
                break;
            case 6:
                this.daysOfWeek[6].choix ? this.daysOfWeek[6].choix=false :this.daysOfWeek[6].choix=true;
                this.daysOfWeek[6].choix ? this.daysOfWeek[6].active=1 :this.daysOfWeek[6].active=0;
                break;
            default:
                break;
        }


        for(let i=0;i<this.daysOfWeek.length;i++){
            if(this.daysOfWeek[i].choix){
                this.timeChecked=false;
            }
        }
    }

    //show the clock for editing

    showClock(day,dayOfWeek){

        this.datePicker.show({
            date: new Date(),
            mode: 'time',
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
        }).then(
            date =>{

                this.daysOfWeek[dayOfWeek.indexOf(day)].heure_debut=date;
                this.daysOfWeek[dayOfWeek.indexOf(day)].heure_fin=date;
                this.daysOfWeek[dayOfWeek.indexOf(day)].heure=date.getHours();
                this.daysOfWeek[dayOfWeek.indexOf(day)].minute=date.getMinutes();
                this.defaultTime=date;
            },
            err => {

            }
        );
    }


    //change with days options

    dayOption(choice:string){

        switch(choice){

            case "all":

                this.selectDays?this.selectDays=false:this.selectDays=true;
                for (let i=0;i<7;i++){
                    this.daysOfWeek[i].choix=this.selectDays;
                    this.daysOfWeek[i].active=1;
                }
                this.timeChecked=!this.selectDays;
                this.unselectDays=false;
                break;

            case "none":
                this.unselectDays?this.unselectDays=false:this.unselectDays=true;
                for (let i=0;i<7;i++){
                    this.daysOfWeek[i].choix=false;
                    this.daysOfWeek[i].active=0;
                }
                this.selectDays=false;
                this.timeChecked=true;
                break;

            default:
                break;

        }

    }


    //change with time option
    timeOption(choice:string){

        switch(choice){

            case "same":

                this.sameTime?this.sameTime=false:this.sameTime=true;
                for (let i=0;i<7;i++){
                    this.daysOfWeek[i].heure_debut=this.defaultTime;
                    this.daysOfWeek[i].heure_fin=this.defaultTime;
                    this.daysOfWeek[i].heure=this.defaultTime.getHours();
                    this.daysOfWeek[i].minute=this.defaultTime.getMinutes();
                }
                this.distinctTime=false;
                break;

            case "distinct":
                this.distinctTime?this.distinctTime=false:this.distinctTime=true;
                this.sameTime=false;
                break;

            default:
                break;

        }

    }


    //Check if the user want to repeat the times
    repeatTimes(){

        this.repeatTime?this.repeatTime=false:this.repeatTime=true;

        for(let i=0;i<this.daysOfWeek.length;i++){

            if(this.daysOfWeek[i].choix){
                this.daysOfWeek[i].reminder=1;
            }else{
                this.daysOfWeek[i].reminder=0;
            }
        }
    }


    //Check if the user want to repeat the days
    repeatDays(){

        this.repeatDay ? this.repeatDay=false : this.repeatDay=true;

        for(let i=0;i<this.daysOfWeek.length;i++){

            if(this.daysOfWeek[i].choix){
                this.daysOfWeek[i].repeat=1;
            }else{
                this.daysOfWeek[i].repeat=0;
            }
        }

    }


    //Get the timetable of the save on the database

    getLocalTimeTable(){

        this.storage.get('user-timetable').then(times=>{
            if(times&&times.length>0){
                this.timetable=times;
                this.initTimetable(times);
                this.canUpated=true;
            }else{
                this.timetable=[];
            }
            this.serveurReponse=true;
        });
    }


    //Get the timetable from the server
    getTimeFromServer(){

        this.storage.get('token').then(token=>{

            let param={
                "user_id": this.user.id,
                "annee": this.annee
            };
            this.time.listTimeTable(param,token)
                .then(data=>{


                if(data&&data.reponse=="success"){
                    this.timetable=data.timetables;
                    this.initTimetable(data.timetables);
                }
                this.serveurReponse=true;

            }).catch(err=>{

                this.getLocalTimeTable();
                this.serveurReponse=true;
            });
        });

    }

    //Go to the user profile
    goProfile(){
        this.navCtrl.setRoot(ProfilPage);
    }

    //Get the user timetable
    listTimeTable(refresh,refresher){

        if(refresh){
            this.getTimeFromServer();
        }else{
            this.getLocalTimeTable();
            if(this.timetable&&this.timetable.length==0){
                this.getTimeFromServer();
            }
        }

        if(refresher)
            refresher.complete();

    }

    //add a new timetable for a user
    add(){

        //Parcourir l'objet dayOfWeek , formater et envoyer

        let param={};
        let times=[];

        for(let i=0;i<this.daysOfWeek.length;i++){
            times.push({heure_debut:this.daysOfWeek[i].heure_debut,heure_fin:this.daysOfWeek[i].heure_fin,jour:this.daysOfWeek[i].jour,active:this.daysOfWeek[i].choix?1:0,reminder:this.daysOfWeek[i].reminder});
        }

        param={user_id:this.user.id,annee:this.annee,times:times};

        //Login loader
        this.loginLoader = this.loadingCtrl.create({
            content: '',
            dismissOnPageChange:true,
            duration: 60000
        });
        this.loginLoader.present();

        this.storage.get('token').then(token=>{
            this.time.addTimeTable(param,token)
                .then(data=>{

                this.loginLoader.dismiss();
                if(data&&data.reponse==="success"){
                    //Success message
                    this.timetable=data.timetables;
                    this.canUpated=true;
                    this.initTimetable(data.timetables);

                    this.translate.get(['TIMETABLE_SERVER_RESPONSE_1']).subscribe(value=>{
                        this.userService.toastAlert(value.TIMETABLE_SERVER_RESPONSE_1);
                    });

                }

            }).catch(err=>{

                if(err&&err.reponse==="failure"){
                    //Error message
                    this.translate.get(['TIMETABLE_SERVER_RESPONSE_2']).subscribe(value=>{
                        this.userService.toastAlert(value.TIMETABLE_SERVER_RESPONSE_2);
                    });
                }
                this.loginLoader.dismiss();
            });

        });

    }

    //update the user timetable
    updated(){



        let param={};
        let times=[];

        for(let i=0;i<this.daysOfWeek.length;i++){

            times.push({id:this.daysOfWeek[i].id,heure_debut:this.daysOfWeek[i].heure_debut,heure_fin:this.daysOfWeek[i].heure_fin,jour:this.daysOfWeek[i].jour,active:this.daysOfWeek[i].choix?1:0,reminder:this.daysOfWeek[i].reminder});
        }

        param={user_id:this.user.id,annee:this.annee,times:times};

        //Login loader
        this.loginLoader = this.loadingCtrl.create({
            content: '',
            dismissOnPageChange:true,
            duration: 60000
        });
        this.loginLoader.present();
        this.storage.get('token').then(token=>{

            this.time.updateTimeTable(param,token)
                .then(data=>{

                this.loginLoader.dismiss();

                if(data&&data.reponse==="updated"){
                    //Updated message
                    this.timetable=data.timetables;
                    this.canUpated=true;
                    this.initTimetable(data.timetables);
                    this.translate.get(['TIMETABLE_SERVER_RESPONSE_3']).subscribe(value=>{
                        this.userService.toastAlert(value.TIMETABLE_SERVER_RESPONSE_3);
                    });
                }

            }).catch(err=>{
                this.loginLoader.dismiss();

                if(err&&err.reponse==="failure"){
                    //Error message
                    this.translate.get(['TIMETABLE_SERVER_RESPONSE_4']).subscribe(value=>{
                        this.userService.toastAlert(value.TIMETABLE_SERVER_RESPONSE_4);
                    });
                }
            });

        });

    }


    //Disabled the user timetable
    deleted(data){

        //Login loader
        this.loginLoader = this.loadingCtrl.create({
            content: '',
            dismissOnPageChange:true,
            duration: 60000
        });
        this.loginLoader.present();
        this.storage.get('token').then(token=>{
            this.time.deleteTimeTable(data,token)
                .then(data=>{

                this.loginLoader.dismiss();

                if(data&&data.reponse==="deleted"){
                    //Delete message
                    this.timetable=data.timetables;
                    this.canUpated=true;
                    this.initTimetable(data.timetables);
                    this.translate.get(['TIMETABLE_SERVER_RESPONSE_5']).subscribe(value=>{
                        this.userService.toastAlert(value.TIMETABLE_SERVER_RESPONSE_5);
                    });
                }

            }).catch(err=>{
                this.loginLoader.dismiss();

                if(err&&err.reponse==="failure"){
                    //Error message
                    this.translate.get(['TIMETABLE_SERVER_RESPONSE_6']).subscribe(value=>{
                        this.userService.toastAlert(value.TIMETABLE_SERVER_RESPONSE_6);
                    });
                }
            });

        });

    }

}
