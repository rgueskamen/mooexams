import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { LoginPage } from '../login/login';

/**
 * Generated class for the InscriptionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})

export class WelcomePage {

  logo:string;
  choix:string;
  illustration:string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.logo="assets/img/MooExams.png";
    this.illustration="assets/img/welcome-img.png";
    this.choix="";
  }


  //changer de page(entre la creation de compte ete le login)
  navigate(choix){
    this.choix=choix;
    this.navCtrl.push(LoginPage,{option:this.choix});
  }


  ionViewDidLoad() {

  }

}
