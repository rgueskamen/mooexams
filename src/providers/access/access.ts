
import { Injectable } from '@angular/core';
import { Storage } from "@ionic/storage";
import { ApiProvider } from "../api/api";
import { UserProvider } from "../user/user";
import {HttpClient} from "@angular/common/http";

/*
  Generated class for the AccessProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class AccessProvider {

    prefix:string;

  constructor(
      public http: HttpClient,
      public api:ApiProvider,
      public storage:Storage,
      public user:UserProvider
  ) {
      this.prefix="api/v2/";
  }


    //Get the user coupons access to questions.
    getCoupons(token):Promise<any>{

     return new Promise<any>((resolve,reject) => {
        this.api.get(`${this.prefix}qcm/coupons?token=${token}`)
            .subscribe(data=>{
                if(data){
                    this.storage.set('user-coupons',data);
                }
                resolve(data);

            },err=>{

                if(err)
                    this.user.handle_user_error(err);
                reject(err);   
            });

        });
    }


    //Get activate a coupons access.
    activateCoupons(data,token):Promise<any>{
        
        return new Promise<any>((resolve,reject) => {
        this.api.post(`${this.prefix}qcm/valider_coupon?token=${token}`,data)
        
            .subscribe(data=>{

            },err=>{

                if(err)
                    this.user.handle_user_error(err);
            });

        });
    }


    //Get activate a coupons access.
    checkValidCoupons(data,token):Promise<any>{

    return new Promise<any>((resolve,reject) => {
      this.api.post(`${this.prefix}qcm/coupons?token=${token}`,data)
        
            .subscribe(data=>{

            },err=>{

                if(err)
                    this.user.handle_user_error(err);
            });

        });
    }


}
