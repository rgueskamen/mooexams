import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { Camera,CameraOptions  } from "@ionic-native/camera";
import { Matiere } from "../../models/matiere";
import { User } from "../../models/user";
import { Participants } from "../../models/participants";

import { Storage } from '@ionic/storage';
import { Observable } from "rxjs/Observable";
import { HttpClient } from "@angular/common/http";
import { SocialSharing } from '@ionic-native/social-sharing';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeAudio } from '@ionic-native/native-audio';

//import { HTTP } from '@ionic-native/http';

/*
 Generated class for the ApiProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular DI.
 */

interface country{
    as:string,
    city:string,
    country:string,
    countryCode:string,
    isp:string,
    lat:number,
    lon:number,
    org:string,
    query:string,
    region:string,
    regionName:string,
    status:number,
    timezone:string,
    zip:string
    }

    interface countries{
        id:number;
        code:string;
        nom:string;
        created_at:Date;
        updated_at:Date;
    }

@Injectable()
export class ApiProvider {

    private url : string ="http://mooexams.sdkgames.com";
    options: CameraOptions;
    prefix:string;
    static index : number;


    constructor(
        public  storage:Storage,
        private http: HttpClient,
        public  alertCtrl: AlertController,
        private nativeGeocoder: NativeGeocoder,
        private nativeAudio: NativeAudio,
        private geolocation: Geolocation,
        private camera: Camera,
        private social : SocialSharing

    ) {
        this.prefix="api/v2/";
        ApiProvider.index=0;


    }


    //Play success sound
    playSuccessSound():Promise<boolean>{
        ApiProvider.index++;
      return new Promise<boolean>((resolve,reject) =>{
        let index= Math.floor(Math.random()*4 + 1);
        this.nativeAudio.preloadSimple('successSound'+ApiProvider.index, 'assets/audio/happy/'+index+'.mp3').then(()=>{

        this.nativeAudio.loop('successSound'+ApiProvider.index).then(()=>{
            setTimeout(() => {
                    this.nativeAudio.stop('successSound'+ApiProvider.index);
                    resolve(true);
                }, 3000);},()=>{
                    reject(false);
             });
            },()=>{
                reject(false);
            });
        });

    }

    stopSuccessSound(){
        this.nativeAudio.stop('FailedSound'+ApiProvider.index);
    }


    playFailledSound():Promise<boolean>{
        ApiProvider.index++;
        return new Promise<boolean>((resolve,reject) =>{
            let index= Math.floor(Math.random()*4 + 1);
            this.nativeAudio.preloadSimple('FailedSound'+ApiProvider.index, 'assets/audio/sad/'+index+'.mp3').then(()=>{
                this.nativeAudio.loop('FailedSound'+ApiProvider.index).then(()=>{
                    setTimeout(() => {
                        this.nativeAudio.stop('FailedSound'+ApiProvider.index);
                        resolve(true);
                    }, 3000);
                 },()=>{
                        reject(false);
                 });
            },()=>{
                reject(false);
            })

      });

    }

    stopFailedSound(){
        this.nativeAudio.stop('FailedSound'+ApiProvider.index);
    }


    // Ends points
    get(endpoint: string, params?: any):Observable<any> {

        return this.http.get(this.url + '/' + endpoint, params);
    }

    post(endpoint: string, body: any, options?:any) :Observable<any>{
        return this.http.post(this.url + '/' + endpoint, JSON.stringify(body), options);
    }

    put(endpoint: string, body: any, options?: any):Observable<any> {
        return this.http.put(this.url + '/' + endpoint, JSON.stringify(body), options);
    }

    delete(endpoint: string, options?: any) :Observable<any>{
        return this.http.delete(this.url + '/' + endpoint, options);
    }

    patch(endpoint: string, body: any, options?: any):Observable<any> {
        return this.http.put(this.url + '/' + endpoint, JSON.stringify(body), options);
    }


    // Show the alert
    showAlert(title:string,subTitle:string,buttons:string[]) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: subTitle,
            buttons: buttons
        });
        alert.present();
    }


  /*
  * Share the score of the user
  * */
 share(survey):Promise<boolean>{

    return new Promise<boolean>((resolve,reject)=>{

    this.social
        .share(survey.message,survey.name,survey.image,survey.link)
        .then((result)=>{
            resolve(true);
        }, function (err) {
            reject(false);
        });
    });
}

//Get the current country position

getCurrentCountry():Promise<any>{

return new Promise<any>((resolve,reject)=>{

    this.geolocation.getCurrentPosition().then((resp) => {

        alert(resp);

        if(resp){
            this.nativeGeocoder.reverseGeocode(resp.coords.latitude, resp.coords.longitude)
            .then((result: NativeGeocoderReverseResult) => {
                alert(JSON.stringify(result));
                resolve(result);
            })
            .catch((error: any) => {
                reject(null);
            });
        }

    }).catch((error) => {
         reject(null);
    });

});

}

//Get country by Ip adress
getCountryByIpAdress():Promise<any>{

    return new Promise<any>((resolve,reject)=>{
        this.http.get('http://ip-api.com/json').subscribe((success:any)=>{


            if(success&&success.status==='success'){
                this.storage.set('curentCountry',success);

                resolve(success);
            }else{
                reject(null);
            }
        },error=>{

            console.log(error);
            this.storage.get('curentCountry').then(country=>{
                    if(country){
                        resolve(country);
                    }else{
                        reject(null);
                    }
            });
        });
    });
}


//Get database information of the current country
getCountryInfos(countries : countries[], country:country) : countries{

    let found=false,i=0,resultat :countries ;

    while(i<countries.length&&!found){

        if(countries[i].code.toLocaleLowerCase()==country.countryCode.toLocaleLowerCase()){

                 resultat=countries[i];
                 found=true;
        }
        i++;
    }

    return resultat;
}


    //return and array of number
    numberToArray(nombre:number):Array<number> {

        let init=20, compteur =0, nbQuestion =[];
        if (nombre > 0) {
            if (nombre <= init) {
                nbQuestion[0] = nombre;
            } else {
                compteur = Math.floor(nombre/init);

                for (let i=0; i < compteur; i++) {
                    nbQuestion[i] = init;
                    init += 20;
                }
            }
        }

        return nbQuestion;
    }


    // Get the number of invitations
    updateBadge():Promise<number>{

        return new Promise<number>((resolve,reject) => {

            let nbreInvitations = 0;

            this.storage.get('token').then(token => {
                if (token) {
                    this.get(`${this.prefix}user/infos?token=${token}`)
                        .subscribe(data => {

                            if (data && data.reponse == "success") {

                                nbreInvitations = data.invitations.length;
                                this.storage.get('user').then(user => {
                                    let param = {"user_id": user.id};
                                    this.post(`${this.prefix}challenge/invited?token=${token}`, param)
                                        .subscribe(data => {

                                            if (data && data.reponse == "success") {
                                                nbreInvitations += data.challenge.length;
                                                resolve(nbreInvitations);
                                            }
                                        }, err => {
                                            reject(nbreInvitations);
                                        });
                                });
                            }

                        }, err => {
                            reject(nbreInvitations);
                        });
                }else{
                    reject(nbreInvitations);
                }
            });
        });

    }


    //take a picture with camera
    takePicture() : Promise<string>{

        return new Promise<string>((resolve,reject) => {
            //Camera options
            this.options = {
                quality: 100,
                destinationType: this.camera.DestinationType.DATA_URL,
                encodingType: this.camera.EncodingType.JPEG,
                mediaType: this.camera.MediaType.PICTURE
            };

            this.camera.getPicture(this.options).then((imageData) => {
                let base64Image = 'data:image/jpeg;base64,' + imageData;
                resolve(base64Image);
            }, (err) => {
                reject("none");
            });

        });

    }


    //Convert an array to object
    objetToArray(data:any):Array<Matiere>{

        let matiere:Matiere[] = [];

        if(data){
            Object.keys(data).map(function(key) {
                matiere.push(data[key]);
            });
        }

        return matiere;
    }

    //Verify it a subjets is  in the array
    isSubjectsIn(matieres:Array<Matiere>,id:number):boolean{
        let isIn=false,i=0;
        if(matieres&&matieres.length>0) {
            while (!isIn && i < matieres.length) {

                if (matieres[i].id == id) isIn = true;
                i++;
            }
        }
        return isIn;
    }

    //Verify it a user is  in the array
    isUserIn(users:Array<User>,id:number):boolean{
        let isIn=false,i=0;
        if(users&&users.length>0) {
            while (!isIn && i < users.length) {

                if (users[i].id == id) isIn = true;
                i++;
            }
        }
        return isIn;
    }

    //Verify it a user is  in the array
    isUserParticipated(users:Array<Participants>,id:number):boolean{
        let isIn=false,i=0;
        if(users&&users.length>0) {
            while (!isIn && i < users.length) {

                if (users[i].participant_id == id) isIn = true;
                i++;
            }
        }
        return isIn;
    }


    //Filter the user array
    userFilter(usersTab1:Array<User>,usersTab2:Array<User>):Array<User>{
        let user:Array<User>=[];

                for(let tab of usersTab1){
                    if(!this.isUserIn(usersTab2,tab.id)){
                        user.push(tab);
                    }
                }

        return user;
    }


    //Filter the user array
    userParticpantsFilter(usersTab1:Array<User>,usersTab2:Array<Participants>):Array<User>{
        let user:Array<User>=[];

        for(let tab of usersTab1){
            if(!this.isUserParticipated(usersTab2,tab.id)){
                user.push(tab);
            }
        }
        return user;
    }


    //Pick up a picture to the library
    selectPicture() : Promise<string>{

        return new Promise<string>((resolve,reject) => {
            this.options = {
                destinationType: this.camera.DestinationType.DATA_URL,
                sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
            };

            this.camera.getPicture(this.options).then((imageData) => {
                let base64Image = 'data:image/jpeg;base64,' + imageData;
                resolve(base64Image);

            }, (err) => {
                reject("none");
            });

        });

    }

}
