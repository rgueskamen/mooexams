import { Injectable } from '@angular/core';
import { Storage } from "@ionic/storage";
import { UserProvider } from "../user/user";
import { ApiProvider } from "../api/api";
import {HttpClient} from "@angular/common/http";

/*
  Generated class for the ChallengeProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ChallengeProvider {

    prefix:string;

  constructor(
      public http: HttpClient,
      public storage:Storage,
      public user:UserProvider,
      public api:ApiProvider

  ) {
      this.prefix="api/v2/";
  }


    //Create a new challenge questions
    createChallenge(data:any,token:string):Promise<any>{

        return new Promise <any> ((resolve,reject)=>{
        
            this.api.post(`${this.prefix}challenge/create?token=${token}`,data)
            .subscribe(data=>{
                resolve(data);
            },err=>{
                reject(err);
                if(err)
                    this.user.handle_user_error(err);
            });

        });

    
    }

    //Added a criteria to a challenge
  addCriteria(data:any,token:string):Promise<any>{

    return new Promise <any> ((resolve,reject)=>{

        this.api.post(`${this.prefix}challenge/add_criteria?token=${token}`,data)
      
          .subscribe(data=>{
            resolve(data);
          },err=>{

                reject(err);
              if(err)
                  this.user.handle_user_error(err);
          });

    });

  }

  //Enable a challenge
    enableChallenge(data:any,token:string):Promise<any>{
      
    return new Promise <any> ((resolve,reject)=>{

      this.api.post(`${this.prefix}challenge/activate?token=${token}`,data)
      
          .subscribe(data=>{
            resolve(data);
          },err=>{
              reject(err);
              if(err)
                  this.user.handle_user_error(err);
          });

        });

  }

  //Disable a challenge
  disableChallenge(data:any,token:string):Promise<any>{

    return new Promise <any> ((resolve,reject)=>{
      this.api.post(`${this.prefix}challenge/disable?token=${token}`,data)
      
          .subscribe(data=>{
            resolve(data);
          },err=>{
            reject(err);
              if(err)
                  this.user.handle_user_error(err);
          });

        });
  }

  //Liste des challenges crees par un utilisateur
  getChallenges(data,token:string):Promise<any>{

    return new Promise <any> ((resolve,reject)=>{
    this.api.post(`${this.prefix}challenge/list?token=${token}`,data)
      
          .subscribe(data=>{

              if(data){
                  this.storage.set('my-challenges',data);
              }
              resolve(data);

          },err=>{

            reject(err);
              if(err)
                  this.user.handle_user_error(err);
          });

        });
  }


  //Invite friends to a challenge
  inviteFriends(data:any,token:string):Promise<any>{

    return new Promise <any> ((resolve,reject)=>{

      this.api.post(`${this.prefix}challenge/invited_friends?token=${token}`,data)
          .subscribe(data=>{
            resolve(data);
          },err=>{
            reject(err);
              if(err)
                  this.user.handle_user_error(err);
          });

        });

  }


  //Accepted the user invitations
  acceptedInvitations(data:any,token:string):Promise<any>{

    return new Promise <any> ((resolve,reject)=>{

    this.api.post(`${this.prefix}challenge/accepted?token=${token}`,data)
      
          .subscribe(data=>{
            resolve(data);
          },err=>{

             reject(err);
              if(err)
                  this.user.handle_user_error(err);
          });

        });

  }

  //Refused the user
    refusedInvitations(data:any,token:string):Promise<any>{

     return new Promise <any> ((resolve,reject)=>{
       this.api.post(`${this.prefix}challenge/decline?token=${token}`,data)
            .subscribe(data=>{
                resolve(data);
            },err=>{
                reject(err);
                if(err)
                    this.user.handle_user_error(err);
            });

        });
    }

    //the list of challenge where the user is invited
    getChallengeInvited(data:any,token:string):Promise<any>{

        return new Promise <any> ((resolve,reject)=>{
         this.api.post(`${this.prefix}challenge/invited?token=${token}`,data)
            .subscribe(data=>{

                if(data&&data.reponse=="success"){
                    this.storage.set('challenges-invites',data);
                }
                resolve(data);
            },err=>{

                if(err)
                    this.user.handle_user_error(err);

                reject(err);
            });

        });
    }

    //The list of challenge where the user particpate
    getChallengeParticpated(data:any,token:string):Promise<any>{

        return new Promise <any> ((resolve,reject)=>{
             this.api.post(`${this.prefix}challenge/participed?token=${token}`,data)
        
            .subscribe(data=>{

                if(data&&data.reponse=="success"){
                    this.storage.set('challenges-participated',data);
                }
                resolve(data);
            },err=>{
                reject(err);
                if(err)
                    this.user.handle_user_error(err);
            });

        });
    }

    //The list of challenge where the user accept invitations
    getChallengeAccepted(data:any,token:string):Promise<any>{

        return new Promise <any> ((resolve,reject)=>{
            this.api.post(`${this.prefix}challenge/invitation_accepted?token=${token}`,data)
        
            .subscribe(data=>{

                if(data&&data.reponse=="success"){
                    this.storage.set('challenges-accepted',data);
                }
                resolve(data);
            },err=>{

                if(err)
                    this.user.handle_user_error(err);
                reject(err);    
            });

        });
    }


    //Save the score of the user on the Challenge
    saveScore(data:any,token:string):Promise<any>{

    return new Promise <any> ((resolve,reject)=>{
       this.api.post(`${this.prefix}challenge/saveScore?token=${token}`,data)
        
            .subscribe(data=>{

            },err=>{

                if(err)
                    this.user.handle_user_error(err);
            });

        });
    }


    //The result of  the Challenge
    challengeResults(data:any,token:string):Promise<any>{

        return new Promise <any> ((resolve,reject)=>{ 
            this.api.post(`${this.prefix}challenge/resultat?token=${token}`,data)
        
            .subscribe(data=>{

            },err=>{

                if(err)
                    this.user.handle_user_error(err);
            });

        });
    }


}
