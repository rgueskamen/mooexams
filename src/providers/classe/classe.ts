import { Injectable } from '@angular/core';
import { ApiProvider } from "../api/api";
import { Classe } from "../../models/classe"
import { Storage } from "@ionic/storage"
import { UserProvider } from "../user/user";
import {HttpClient} from "@angular/common/http";

/*
  Generated class for the ClasseProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ClasseProvider {

    prefix:string;

  constructor(
      public http: HttpClient,
      public api : ApiProvider,
      private storage: Storage,
      public user:UserProvider
  ) {
        this.prefix="api/v2/"
  }

  //Get the list of all classes
  list():Promise<any> {

    return new Promise <any> ((resolve,reject)=>{ 
      this.api.get('qcm/signup')
          .subscribe(res=>{

              let classes:Classe[];
              classes=res;
              if(classes.length>0) this.storage.set('classes',classes);
              resolve(res);

          },err=>{
              reject(err);
              if(err)
                  this.user.handle_user_error(err);
          });

    });

  }


  //Get the list of countries by countries and langauge
  listByCountry(data):Promise<any>{

    return new Promise <any> ((resolve,reject)=>{ 

        this.api.post(`${this.prefix}classe/list`,data)
             .subscribe(data=>{

              if(data&&data.reponse=="success"){
                  this.storage.set('classes-by-country',data.classes);
              }
              resolve(data);

           },err=>{

              reject(err);
              if(err)
                  this.user.handle_user_error(err);
        });

    });

  }


  //Get  the current classes of the user
    userClasses(token):Promise<any>{

       return new Promise <any> ((resolve,reject)=>{   

          this.api.get(`${this.prefix}user/classes?token=${token}`)
            .subscribe(data=>{

                if(data&&data.reponse=="success"){
                    this.storage.set('user-classes',data.classes);
                }
                resolve(data);

            },err=>{

                reject(err);
                if(err)
                    this.user.handle_user_error(err);
            });

        });
        
    }

}


