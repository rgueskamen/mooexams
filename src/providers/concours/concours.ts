import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserProvider } from "../user/user";
import { ApiProvider } from "../api/api";


/*
  Generated class for the ConcoursProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConcoursProvider {

  prefix:string;

  constructor(
    public http: HttpClient,
    public api : ApiProvider,
    public storage : Storage,
    public user:UserProvider
  ) {
    this.prefix="api/v2/";
  }

      //Get the list of concours
      getconcours(token:string):Promise<any>{

        return new Promise <any> ((resolve,reject)=>{
        
            this.api.get(`${this.prefix}getconcours?token=${token}`)
            .subscribe(data=>{
                resolve(data);
                this.storage.set('concours',data);
            },err=>{
                reject(err);
                if(err)
                    this.user.handle_user_error(err);
            });

        });    
      }

    //Get the list of questions of the concours
    getconcoursquestions(data:any,token:string):Promise<any>{

      return new Promise <any> ((resolve,reject)=>{
      
          this.api.post(`${this.prefix}questionsconcours?token=${token}`,data)
          .subscribe(data=>{
              resolve(data);
          },err=>{
              reject(err);
              if(err)
                  this.user.handle_user_error(err);
          });

      });    
    }

        //Save questions  of the concours
       saveconcoursquestions(data:any,token:string):Promise<any>{

          return new Promise <any> ((resolve,reject)=>{
          
              this.api.post(`${this.prefix}saveconcoursanswers?token=${token}`,data)
              .subscribe(data=>{
                  resolve(data);
              },err=>{
                  reject(err);
                  if(err)
                      this.user.handle_user_error(err);
              });
    
          });    
        }


        //Get the list of sponsors
        getsponsors(token:string):Promise<any>{

          return new Promise <any> ((resolve,reject)=>{
          
              this.api.get(`${this.prefix}getsponsors?token=${token}`)
              .subscribe(data=>{
                  resolve(data);
                  this.storage.set('sponsors',data);
              },err=>{
                  reject(err);
                  if(err)
                      this.user.handle_user_error(err);
              });
  
          });    
        }

        //Save the view of page of concours

        saveconcoursviews(data:any,token:string):Promise<any>{

          return new Promise <any> ((resolve,reject)=>{
          
              this.api.post(`${this.prefix}savesponsorview?token=${token}`,data)
              .subscribe(data=>{
                  resolve(data);
              },err=>{
                  reject(err);
                  if(err)
                      this.user.handle_user_error(err);
              });
    
          });    
        }


        // create the team of concours
        createteam(data:any,token:string):Promise<any>{

          return new Promise <any> ((resolve,reject)=>{
          
              this.api.post(`${this.prefix}createteam?token=${token}`,data)
              .subscribe(data=>{
                  resolve(data);
              },err=>{
                  reject(err);
                  if(err)
                      this.user.handle_user_error(err);
              });
    
          });    
        }

        //Verify if a user has a team
        findteam(data:any,token:string):Promise<any>{
          return new Promise <any> ((resolve,reject)=>{
          
              this.api.post(`${this.prefix}finduserteam?token=${token}`,data)
              .subscribe(response=>{
                  resolve(response);
                  this.storage.set('team'+data.concours_id,response);
              },err=>{
                  reject(err);
                  if(err)
                      this.user.handle_user_error(err);
              });
    
          });    

        }

        //Add a new member to a team
        addmemberteam(data:any,token:string):Promise<any>{

          return new Promise <any> ((resolve,reject)=>{
          
              this.api.post(`${this.prefix}addmember?token=${token}`,data)
              .subscribe(data=>{
                  resolve(data);
              },err=>{
                  reject(err);
                  if(err)
                      this.user.handle_user_error(err);
              });
    
          });    

        }

        //Update the information of a member

        updatememberteam(data:any,token:string):Promise<any>{

          return new Promise <any> ((resolve,reject)=>{
          
              this.api.post(`${this.prefix}updatemember?token=${token}`,data)
              .subscribe(data=>{
                  resolve(data);
              },err=>{
                  reject(err);
                  if(err)
                      this.user.handle_user_error(err);
              });
    
          });    

        }

        //Delete the member of a team

        deletememberteam(data:any,token:string):Promise<any>{

          return new Promise <any> ((resolve,reject)=>{
          
              this.api.post(`${this.prefix}deletemember?token=${token}`,data)
              .subscribe(data=>{
                  resolve(data);
              },err=>{
                  reject(err);
                  if(err)
                      this.user.handle_user_error(err);
              });
    
          });    

        }


        //Invite a friend to MooExams
        inviteFriend(data:any,token:string):Promise<any>{

            return new Promise <any> ((resolve,reject)=>{
            
                this.api.post(`${this.prefix}saveinvitation?token=${token}`,data)
                .subscribe(data=>{
                    resolve(data);
                },err=>{
                    reject(err);
                    if(err)
                        this.user.handle_user_error(err);
                });
      
            });    
  
        }


        //Leave the team created by another user
        leaveTeam(data:any,token:string):Promise<any>{

            return new Promise <any> ((resolve,reject)=>{
            
                this.api.post(`${this.prefix}leaveteam?token=${token}`,data)
                .subscribe(data=>{
                    resolve(data);
                },err=>{
                    reject(err);
                    if(err)
                        this.user.handle_user_error(err);
                });
      
            });    
  
        }

}
