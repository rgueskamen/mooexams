import { Injectable } from '@angular/core';
import {ApiProvider} from "../api/api";
import {UserProvider} from "../user/user";
import {HttpClient} from "@angular/common/http";


/*
  Generated class for the FeedbacksProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class FeedbacksProvider {

  prefix:string;

  constructor(
      public http: HttpClient,
      public api:ApiProvider,
      public user:UserProvider
  ) {

      this.prefix="api/v2/";
  }


    /**
     * View a post
     *
     */

    sendMessage(data:any,token:string):Promise<any>{
        
        return new Promise<any>((resolve,reject) => {

       this.api.post(`${this.prefix}feedbacks/create?token=${token}`,data)
        
            .subscribe(res => {
                resolve(res);

            },err=>{

                reject(err);
                if(err){
                    this.user.handle_user_error(err);
                }
            });

        });
    }

}
