import { Injectable } from '@angular/core';
import { ApiProvider } from "../api/api";
import { Storage } from "@ionic/storage";
import { UserProvider } from "../user/user";
import {HttpClient} from "@angular/common/http";


/*
  Generated class for the ForumProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ForumProvider {

    prefix:string;


  constructor(
      public http: HttpClient,
      public api : ApiProvider,
      public user:UserProvider,
      private storage:Storage
  ) {
      this.prefix="api/v2/";

  }

    /**
     * Add a new post
     *
     */

    addPost(data:any,token:string):Promise<any>{

        return new Promise<any>((resolve,reject)=>{
             this.api.post(`${this.prefix}forum/addPost?token=${token}`,data)
                .subscribe(res => {
                    resolve(res);
                },err=>{
                    reject(err);
                    if(err){
                        this.user.handle_user_error(err);
                    }
                });
        });

    }


    /**
     * comment a post
     *
     */

    commentPost(data:any,token:string):Promise<any>{

        return new Promise<any>((resolve,reject)=>{
        this.api.post(`${this.prefix}forum/commentPost?token=${token}`,data)
        
            .subscribe(res => {
               
                if(res&&res.reponse==="success"){
                    this.storage.set('posts-comments',res);
                }
                resolve(res);
            },err=>{
                if(err){
                    this.user.handle_user_error(err);
                }
                reject(err);
            });
         });
    }

    /**
     * Like a post
     *
     */

    likePost(data:any,token:string):Promise<any>{

        return new Promise<any>((resolve,reject)=>{
        this.api.post(`${this.prefix}forum/likePost?token=${token}`,data)
        .subscribe(res => {
                resolve(res);
            },err=>{
                reject(err);
                if(err){
                    this.user.handle_user_error(err);
                }
            });

        });
    }


    /**
     * Note a post
     *
     */

    notePost(data:any,token:string):Promise<any>{

       return new Promise<any>((resolve,reject)=>{

       this.api.post(`${this.prefix}forum/notePost?token=${token}`,data)
        
            .subscribe(res => {
                resolve(res);
            },err=>{
                reject(err);
                if(err){
                    this.user.handle_user_error(err);
                }
            });

        });
    }



    /**
     * View a post
     *
     */

    viewPost(data:any,token:string):Promise<any>{

        return new Promise<any>((resolve,reject)=>{

            this.api.post(`${this.prefix}forum/addPostview?token=${token}`,data)
        
            .subscribe(res => {
                resolve(res);
            },err=>{
                reject(err);
                if(err){
                    this.user.handle_user_error(err);
                }
            });

        });
    }

    /**
     * disable a post
     *
     */

    disablePost(data:any,token:string):Promise<any>{

        return new Promise<any>((resolve,reject)=>{

        this.api.post(`${this.prefix}forum/disabledPost?token=${token}`,data)
        
            .subscribe(res => {
                resolve(res);
            },err=>{
                reject(err);
                if(err){
                    this.user.handle_user_error(err);
                }
            });

        });
    }



    /**
     * delete a post
     *
     */

    deletePostComment(data:any,token:string):Promise<any>{

        return new Promise<any>((resolve,reject)=>{
            this.api.post(`${this.prefix}forum/delComment?token=${token}`,data)
        
            .subscribe(res => {
                resolve(res);
            },err=>{
                reject(err);
                if(err){
                    this.user.handle_user_error(err);
                }
            });

        });
    }

    /**
     * Note a post comment
     */

    notePostComment(data:any,token:string):Promise<any>{

        return new Promise<any>((resolve,reject)=>{

            this.api.post(`${this.prefix}forum/noteComment?token=${token}`,data)
        
            .subscribe(res => {
                resolve(res);
            },err=>{
                reject(err);
                if(err){
                    this.user.handle_user_error(err);
                }
            });

        });
    }

    /**
     * Like a post comment
     */

    likePostComment(data:any,token:string):Promise<any>{

        return new Promise<any>((resolve,reject)=>{

          this.api.post(`${this.prefix}forum/likeComment?token=${token}`,data)
        
            .subscribe(res => {
                resolve(res);
            },err=>{
                reject(err);
                if(err){
                    this.user.handle_user_error(err);
                }
            });

        });

    }



    /**
     * Get the list of Post and their comments
     *
     */
    getPosts(token:string):Promise<any>{

        return new Promise<any>((resolve,reject)=>{

          this.api.get(`${this.prefix}forum/newsFeeds?token=${token}`)
        
            .subscribe(res => {
                resolve(res);
                if(res){
                    this.storage.set('posts',res);
                }
            },err=>{
                reject(err);
                if(err){
                    this.user.handle_user_error(err);
                }
            });

        });
    }

}
