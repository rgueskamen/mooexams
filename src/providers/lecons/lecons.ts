import { Injectable } from '@angular/core';
import { Storage } from "@ionic/storage";
import { ApiProvider } from "../api/api";
import { UserProvider } from "../user/user";
import {HttpClient} from "@angular/common/http";

/*
  Generated class for the LeconsProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class LeconsProvider {

    token:string;
    prefix:string;

  constructor(
      public http: HttpClient,
      public storage : Storage,
      public api : ApiProvider,
      public user:UserProvider
  ) {

      this.prefix="api/v2/";
  }


    //Get the list of lessons with number of questions
    lessons_with_nb_questions(data,token) :Promise<any>{

        return new Promise<any>((resolve,reject)=>{

        this.api.post(`qcm/nombre_questions?token=${token}`,data)
    
            .subscribe(data=>{
                let id = data.id_matiere;
                if(data){
                    this.storage.set(`lessons${id}`,data);
                }
                resolve(data);

            },err=>{
                reject(err);
                if(err)
                    this.user.handle_user_error(err);

            });

        });

    }


    //Get the list of lessons of a subject
    courseLessons(data,token):Promise<any>{

      return new Promise<any>((resolve,reject)=>{
        this.api.post(`${this.prefix}matieres/lecons?token=${token}`,data)
        
            .subscribe(data=>{

                if(data&&data.reponse=="success"){
                    this.storage.set('courses-lessons',data);
                }
                resolve(data);

            },err=>{

                reject(err);
                if(err)
                    this.user.handle_user_error(err);
            });

        });
    }


    //Get the list of levels by lessons
    lessonsLevels(data,token):Promise<any>{

     return new Promise<any>((resolve,reject)=>{

       this.api.post(`${this.prefix}lecon/niveaux?token=${token}`,data)
        
            .subscribe(data=>{
                resolve(data);
                if(data&&data.reponse=="success"){
                    this.storage.set('lessons-levels',data);
                }

            },err=>{
                reject(err);
                if(err)
                    this.user.handle_user_error(err);
            });

        });
    }



    //Get the user course's levels
    userlessonsLevel(data,token):Promise<any>{

    return new Promise<any>((resolve,reject)=>{
        this.api.post(`${this.prefix}lecon/userNiveaux?token=${token}`,data)
        
            .subscribe(data=>{
                resolve(data);
            },err=>{
                reject(err);
                if(err)
                    this.user.handle_user_error(err);
            });

     });

    }


}
