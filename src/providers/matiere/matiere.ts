import { Injectable } from '@angular/core';
import { ApiProvider } from "../api/api";
import { Storage } from "@ionic/storage";
import { Matiere } from "../../models/matiere";
import { UserProvider } from "../user/user";
import { HttpClient } from "@angular/common/http";

/*
  Generated class for the MatiereProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class MatiereProvider {

    token : string;
    prefix:string;

    constructor(
        public http: HttpClient,
        public api : ApiProvider,
        public storage : Storage,
        public user:UserProvider

    ) {

        this.prefix="api/v2/";
    }

    //Get all the courses list
    list():Promise<any> {

        return new Promise<any>((resolve,reject)=>{

        this.api.get(`qcm/allmatieres`)

            .subscribe(res=>{

                let matiere:Matiere[] = [];

                if(res){
                    Object.keys(res).map(function(key) {
                        matiere.push(res[key]);
                    });
                }

                if(matiere.length>0){
                    this.storage.set('matieres',matiere);
                }

                resolve(res);

            },err=>{

                if(err)
                    this.user.handle_user_error(err);

                reject(err);    

            });

        });

    }


    //Get the courses list of a user class
    classCourses(token) :Promise <any>{

        return new Promise<any>((resolve,reject)=>{

         this.api.get(`qcm/matieres?token=${token}`)

            .subscribe(res => {

                let matiere: Matiere[] = [];

                if(res){
                    Object.keys(res).map(function (key) {
                        matiere.push(res[key]);
                    });
                }

                if(matiere.length>0){
                    this.storage.set('classe_matieres', matiere);
                }
                resolve(res);

            }, err => {

                //traiter les erreurs
                reject(err);
                this.user.handle_user_error(err);

            });

        });


    }


    //Get the lesson list teaches by a user

    coursesTeaches(data,token):Promise<any>{

        return new Promise<any>((resolve,reject)=>{

         this.api.post(`qcm/teacher/matieres?token=${token}`,data)

        .subscribe(data=>{

            if(data&&data.matieres){
                this.storage.set('user-matieres',data.matieres);
            }
            resolve(data);
        },err=>{

            reject(err);
            if(err)
                this.user.handle_user_error(err);
        });

        });
    }


    //Add a course to a teacher
    teacherCourses(data,token):Promise<any>{

    return new Promise<any>((resolve,reject)=>{  

       this.api.post(`qcm/teacher/addmatiere?token=${token}`,data)

        .subscribe(data=>{
            resolve(data);
        },err=>{
            reject(err);
            if(err)
                this.user.handle_user_error(err);
        });

     });

    }


    //Get the system level
    getLevels(token):Promise<any>{

    return new Promise<any>((resolve,reject)=>{ 
      
        this.api.get(`${this.prefix}matieres/niveaux?token=${token}`)

        .subscribe(data=>{

            if(data&&data.reponse==='success'){
                this.storage.set('niveaux-matieres',data.niveaux);
            }
            resolve(data);

        },err=>{
            reject(err);
            if(err)
                this.user.handle_user_error(err);
        });

    });

 }


    //Get the users levels in a course
    getUsersCourseLevel(data,token):Promise<any>{

      return new Promise<any>((resolve,reject)=>{  

        this.api.post(`${this.prefix}matieres/user/niveaux?token=${token}`,data)

         .subscribe(data=>{
             resolve(data);

          },err=>{

             reject(err);
             if(err)
                this.user.handle_user_error(err);

         });

       });

    }


    //Get the users levels by courses
    getUsersLevels(data,token):Promise<any>{

    return new Promise<any>((resolve,reject)=>{    
    
        this.api.post(`${this.prefix}matieres/user/allniveaux?token=${token}`,data)

        .subscribe(data=>{

            if(data&&data.reponse==='success'){

                this.storage.set('user-niveaux',data.niveaux_matiere);
            }

            resolve(data);

        },err=>{

            if(err)
                this.user.handle_user_error(err);

            reject(err);
        });

     });

    }

    //Get all lessons for each course(all subjects)

    getCourseLesson(data,token):Promise<any>{

      return new Promise<any>((resolve,reject)=>{ 

            this.api.post(`${this.prefix}matieres/courseLessons?token=${token}`,data)

            .subscribe(data=>{

                if(data&&data.reponse==='success'){
                    this.storage.set('course-lessons',data.leconMatiere);
                }
                resolve(data);

            },err=>{

                if(err)
                    this.user.handle_user_error(err);

                reject(err);    

           });

    });

 }



    //Delete a teacher subject
    deleteSubject(data,token):Promise<any>{

        return new Promise<any>((resolve,reject)=>{ 

            this.api.post(`${this.prefix}matiere/delete?token=${token}`,data)

            .subscribe(data=>{

                     if(data&&data.reponse==='success'){
                            this.storage.set('user-matieres',data.matieres);
                     }
                     resolve(data);

            },err=>{
                    reject(err); 
                    if(err)
                        this.user.handle_user_error(err);
            });

        });

    }



    //The list of the group of subjects

    groupsSubjects(token):Promise<any>{

        return new Promise<any>((resolve,reject)=>{

        this.api.get(`${this.prefix}matieres/groupes?token=${token}`)

        .subscribe(data=>{

            resolve(data);
            if(data&&data.reponse==='success'){

                     this.storage.set('subjects-group',data);
            }

        },err=>{

            reject(err); 
            if(err)
                this.user.handle_user_error(err);
        });

    });
    }



    //The list of the group of subjects

    subjetsGroup(data,token):Promise<any>{

        return new Promise<any>((resolve,reject)=>{ 

        this.api.post(`${this.prefix}matiere/groupeMatiere?token=${token}`,data)

        .subscribe(data=>{

            if(data&&data.reponse==='success'){
                this.storage.set('group-subjects',data.matieres);
            }
            resolve(data);

        },err=>{
            reject(err); 
            if(err)
                this.user.handle_user_error(err);
        });

     });
    }

}
