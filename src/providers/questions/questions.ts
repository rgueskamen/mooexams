import { Injectable } from '@angular/core';
import { ApiProvider } from "../api/api";
import { Storage } from "@ionic/storage";
import { UserProvider } from "../user/user";
import {HttpClient} from "@angular/common/http";


/*
  Generated class for the QuestionsProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class QuestionsProvider {

    token:string;
    prefix:string;

  constructor(
      public http: HttpClient,
      public api : ApiProvider,
      public storage : Storage,
      public user:UserProvider

  ) {
      this.prefix="api/v2/";
  }


    //Get list of questions by lesson with limit
    training_questions(token,data) :Promise<any>{

        return new Promise<any>((resolve,reject)=>{

      this.api.post(`${this.prefix}qcm/rubric_traning_question?token=${token}`,data)

        .subscribe(res=>{

            resolve(res);

            },err=>{

                reject(err);
                //traiter les erreurs
                if(err)
                    this.user.handle_user_error(err);
            });

        });

    }

    //Get the list of question a subject
    challenge_by_course_questions(data,token):Promise<any>{

        return new Promise<any>((resolve,reject)=>{

        this.api.post(`${this.prefix}questions/mode_defis_matiere?token=${token}`,data)

            .subscribe(questions=>{
                resolve(questions);
                },err=>{
                    reject(err);
                    if(err)
                        this.user.handle_user_error(err);
                });

        });
    }


    //Get the list of question of a lesson
    challenge_by_lessons_questions(data,token):Promise<any>{

     return new Promise<any>((resolve,reject)=>{

        this.api.post(`${this.prefix}questions/mode_defis_lecons?token=${token}`,data)      
        .subscribe(data=>{
            resolve(data);

            },err=>{

            reject(err);
                if(err)
                    this.user.handle_user_error(err);
            });

        });
    }


    //Get the list of questions of the challenge
    user_challenge_questions(data,token):Promise<any>{

        return new Promise<any>((resolve,reject)=>{

          this.api.post(`${this.prefix}questions/mode_challenge?token=${token}`,data)

            .subscribe(data=>{

                resolve(data);

            },err=>{

                reject(err);
                
                if(err)
                    this.user.handle_user_error(err);
            });

        });
    }


}
