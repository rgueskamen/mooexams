import { Injectable } from '@angular/core';
import { Storage } from "@ionic/storage";
import { UserProvider } from "../user/user";
import { ApiProvider } from "../api/api";
import { HttpClient } from "@angular/common/http";

/*
  Generated class for the ScoreProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ScoreProvider {

    prefix:string;

  constructor(
      public http: HttpClient,
      public user:UserProvider,
      public storage:Storage,
      public api:ApiProvider

  ) {
    this.prefix="api/v2/";

  }


    //Save the answer of the questions
    saveResultat(data,token) : Promise<any>{

        return new Promise<any>((resolve,reject)=>{

         this.api.post(`${this.prefix}questions/resultats?token=${token}`,data)
        
            .subscribe(data=>{
                //Update the user Levels.
                resolve(data);
            },err=>{

                reject(err);
                if(err)
                    this.user.handle_user_error(err);
            });

        });
    }


    //Get the history courses
    getCourseHistory(data,token):Promise<any>{

        return new Promise<any>((resolve,reject)=>{

         this.api.post(`${this.prefix}resultats/matieres?token=${token}`,data)
        
            .subscribe(data=>{

                if(data.reponse==="success"){
                    this.storage.set('course-results',data);
                }
                resolve(data);

            },err=>{
                reject(err);
                if(err)
                    this.user.handle_user_error(err);
            });

        });
    }



    //Get the history courses
    getLessonHistory(data,token):Promise<any>{

        return new Promise<any>((resolve,reject)=>{
         this.api.post(`${this.prefix}resultats/leçon?token=${token}`,data)
        
            .subscribe(data=>{

                if(data.reponse==="success"){
                    this.storage.set('lesson-results',data);
                }
                resolve(data);

            },err=>{
                if(err)
                    this.user.handle_user_error(err);
                reject(err);    
            });

        });
    }

    //Get the history in all subjects
    getAllHistory(data,token):Promise<any>{

        return new Promise<any>((resolve,reject)=>{
            
         this.api.post(`${this.prefix}resultats/allmatieres?token=${token}`,data)
        
            .subscribe(data=>{

                if(data.reponse==="success"){
                    this.storage.set('user-results',data);
                }
                resolve(data);

            },err=>{
                reject(err);
                if(err)
                    this.user.handle_user_error(err);
            });

        });
    }

}
