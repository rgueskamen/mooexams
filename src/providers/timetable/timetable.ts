import { Injectable } from '@angular/core';
import { ApiProvider } from "../api/api";
import { UserProvider } from "../user/user";
import { Storage } from "@ionic/storage";
import { HttpClient } from "@angular/common/http";

/*
  Generated class for the TimetableProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class TimetableProvider {

  prefix:string;

  constructor(
      public http: HttpClient,
      public api:ApiProvider,
      public storage:Storage,
      public user:UserProvider

  ) {
      this.prefix="api/v2/";
  }


  //Add the user timetable for learning

  addTimeTable(data,token):Promise<any>{

    return new Promise<any>((resolve,reject) => {
      this.api.post(`${this.prefix}timetable/add?token=${token}`,data)

            .subscribe(res=>{

                if(res&&res.reponse==="success"){
                    this.storage.set('user-timetable',res.timetable);
                }

                resolve(res);

        },err=>{

                reject(err);
                if(err)
                    this.user.handle_user_error(err);
        });

    });

  }


  //Modify the user timetable
    updateTimeTable(data,token):Promise<any>{

    return new Promise<any>((resolve,reject) => {
        this.api.post(`${this.prefix}timetable/update?token=${token}`,data)

            .subscribe(res=>{

                if(res&&res.reponse==="updated"){
                    this.storage.set('user-timetable',res.timetable);
                }

                resolve(res);
            },err=>{

                reject(err);
                if(err)
                    this.user.handle_user_error(err);
            });

        });
    }


    //delete the user timetable
    deleteTimeTable(data,token):Promise<any>{

        return new Promise<any>((resolve,reject) => {

             this.api.post(`${this.prefix}timetable/delete?token=${token}`,data)
            .subscribe(res=>{

                if(res&&res.reponse==="deleted"){
                    this.storage.remove('user-timetable');
                    this.storage.set('user-timetable',res.timetable);
                }
                resolve(res);

            },err=>{

                reject(err);

                if(err)
                    this.user.handle_user_error(err);
            });

        });
    }



    //SHOW user timetable for a year
    listTimeTable(data,token):Promise<any>{

        return new Promise<any>((resolve,reject) => {
        this.api.post(`${this.prefix}timetable/list?token=${token}`,data)
            .subscribe(res=>{

                if(res&&res.reponse==="deleted"){
                    this.storage.remove('user-timetable');
                }
                resolve(res);

            },err=>{

                reject(err);
                if(err)
                    this.user.handle_user_error(err);
            });

        });
    }

}
