import { Injectable } from '@angular/core';
import { ToastController, Toast } from 'ionic-angular';

import  { Storage } from "@ionic/storage";

import { TranslateService } from '@ngx-translate/core';

import { ApiProvider } from '../api/api';
import { OneSignal } from "@ionic-native/onesignal";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";


@Injectable()
export class UserProvider {

    prefix:string;
    body:any;
    playerId:string;
    toast:Toast;


    constructor(
        public http: HttpClient,
        public api : ApiProvider,
        private storage:Storage,
        private translate: TranslateService,
        public  toastCtrl:ToastController,
         public onesignal:OneSignal
    ) {

        this.prefix="api/v2/";
        this.playerId="";
    }

    /**
     * Show a toast message
     */
    toastAlert(message:string){

        if (this.toast) {
            this.toast.dismiss();
        }
     
         this.toast = this.toastCtrl.create({
            message: message,
            duration: 3000
        });

        this.toast.present();

        setTimeout(()=>{
            if (this.toast) {
                this.toast.dismiss();
            }
        },3000);
    }


    /**
     * Send a POST request to get the list of countries
     *
     */
    getCountries():Promise<any>{

        return new Promise<any>((resolve,reject) => {
        this.api.get(`${this.prefix}pays/getall`)
        
            .subscribe(res => {

                if(res&&res.reponse==="success"){
                    this.storage.set('countries',res.pays);
                }
                resolve(res);

            },err=>{
                if(err){
                    this.handle_user_error(err);
                }
                reject(err);

            });

        });
    }

    /**
     * Send a POST request to our signup endpoint with the data
     * the user entered on the form.
     */

    signup(accountInfo: any) :Promise<any>{

        return new Promise<any>((resolve,reject) => {

        this.api.post(`${this.prefix}user/signup`, accountInfo)

            .subscribe(res => {
                // If the API returned a successful response, mark the user as logged in
                if (res&&res.reponse === 'success') {

                    this._loggedIn(res,accountInfo.user.password);
                    this.userInfos(res.token);
                }
                
                resolve(res);

            }, err=>{

                if(err){
                    if(err.error&&err.error.reponse){
                        this.handleUserError(err.error.reponse);
                    }else{
                        this.handle_user_error(err);
                    }
                }
                reject(err);

            });
        });

    }

    /**
     * Send a POST request to our login endpoint with the data
     * the user entered on the form.
     */
    login(accountInfo: any):Promise<any> {

        return  new Promise<any>((resolve,reject)=>{
       
            this.api.post(`${this.prefix}user/signin`, accountInfo)
            .subscribe(res => {

                // If the API returned a successful response, mark the user as logged in
                if (res&&res.reponse === 'success') {

                    this._loggedIn(res,accountInfo.user.password);
                    this.userInfos(res.token);
                }

                resolve(res);

            }, err => {


                if(err) {

                    if (err.error&&err.error.reponse) {

                        this.handleUserError(err.error.reponse);
                    } else {
                        this.handle_user_error(err);
                    }
                }
                reject(err);
            });

        });
    }



    /**
     *  Send the user email to get a code that will allow the user to change his password
     */
    getcode(data:any):Promise<any>{

        return  new Promise<any>((resolve,reject)=>{
            this.api.post(`${this.prefix}user/getcode`,data)

            .subscribe(res => {
                // If the API returned a successful response, mark the user as logged in
                if(res&&res.reponse ==="success"){
                    this.storage.set('hasCode',true);
                }

                resolve(res);

            }, err => {



                if(err){

                    if(err.error&&err.error.reponse){
                        this.handleUserError(err.error.reponse);
                    } else{
                        this.handle_user_error(err);
                    }
                }

                reject(err);
            });

        });

        
    }


    /**
     *  Reset the user password by sending the correct code and others required informations
     */
    resetPassword(data:any):Promise<any>{

        return  new Promise<any>((resolve,reject)=>{
        
            this.api.post(`${this.prefix}user/resetPassword`,data)
            .subscribe(res => {
                // If the API returned a successful response, mark the user as logged in
                if (res.reponse === 'success') {
                    this._loggedIn(res,data.password);
                    this.userInfos(res.token);
                }
                resolve(res);
            }, err => {

                if(err){
                    if(err.error&&err.error.reponse){
                        this.handleUserError(err.error.reponse);
                    }else{
                        this.handle_user_error(err);
                    }
                }

                reject(err);
            });
        });
    }


    /**
     * Get the code for MooSurvey
     */
    getSurveycode(token):Promise<any>{

        return  new Promise<any>((resolve,reject)=>{
        
            this.api.get(`${this.prefix}getsurveycode?token=${token}`)
            .subscribe(res => {

            }, err => {

                if(err){
                    if(err.error&&err.error.reponse){
                        this.handleUserError(err.error.reponse);
                    }else{
                        this.handle_user_error(err);
                    }
                }

                reject(err);
            });
        });
    }


    /**
     *  Get all the user informations.
     */
    userInfos(token:string):Observable<any>{

       // return new Promise<any>((resolve,reject) => {

       let  seq=  this.api.get(`${this.prefix}user/infos?token=${token}`);
        seq.subscribe(res => {
                // If the API returned a successful response, mark the user as logged in
                if (res&&res.reponse === 'success') {
                    this._userData(res);
                }
               // resolve(res);
             },err => {
               
                if(err){
                    this.handle_user_error(err);
                }
                
               // reject(err);
               
            });

            return seq

       // });

    }


    /**
     *
     * @param data
     * @param {string} token
     * @returns {Observable<any>}
     */

    _userData(data) {

        if (data.user)
            this.storage.set('user', data.user);
        if (data.classes)
            this.storage.set('user-classes', data.classes);
        if (data.userClasse)
            this.storage.set('user-school', data.userClasse);
        if (data.timetables)
            this.storage.set('user-timetable', data.timetables);
        if (data.coupons)
            this.storage.set('user-coupons', data.coupons);
        if (data.matieres)
            this.storage.set('user-matieres', data.matieres);
        if (data.niveaux)
            this.storage.set('user-niveaux', data.niveaux);
        if (data.friends)
            this.storage.set('user-friends', data.friends);
        if (data.invitations)
            this.storage.set('user-invitations', data.invitations);
        if (data.suivi)
            this.storage.set('user-progression', data.suivi);

    }


    /*
     *Update user informations
     *
     */
    updateInfos(data:any,token:string):Promise<any>{

        return new Promise<any>((resolve,reject) => {

        this.api.post(`${this.prefix}user/update?token=${token}`,data)

            .subscribe(res => {
                // If the API returned a successful response, mark the user as logged in
                if (res&&res.reponse == 'success') {
                    this._userData(res);
                }
                resolve(res);
            }, err => {

                if(err) {
                    if (err.error && err.error.reponse) {
                        this.handleUserError(err.error.reponse);
                    } else {
                        this.handle_user_error(err);
                    }
                }
                reject(err);
            });

        });

    }


    /*
     *Update password the user need to give the new and the old password
     *
     */
    updatePassword(data:any,token:string):Promise<any>{

        return new Promise<any>((resolve,reject) => {

       this.api.post(`${this.prefix}user/update?token=${token}`,data)

            .subscribe(res => {
                // If the API returned a successful response, mark the user as logged in
                if (res&&res.reponse == 'success') {
                    this._loggedIn(res,data.password);
                }
                resolve(res);
            }, err => {

                if(err) {
                    if (err.error&&err.error.reponse) {
                        this.handleUserError(err.error.reponse);
                    } else {
                        this.handle_user_error(err);
                    }
                }
                reject(err);
            });
        });

    }


    /*
     * Update the class of the user
     *
     */
    updateClasse(data:any,token:string):Promise<any>{


        return new Promise<any>((resolve,reject) => {

         this.api.post(`${this.prefix}user/updateClasse?token=${token}`,data)

            .subscribe(res => {
                // If the API returned a successful response, mark the user as logged in
                if (res&&res.reponse==='success') {
                    this._userData(res);
                }
                resolve(res);
            }, err => {

                reject(err);
                if(err) {
                    if (err.error && err.error.reponse) {
                        this.handleUserError(err.error.reponse);
                    } else {
                        this.handle_user_error(err);
                    }
                }

            });

        });


    }


    /*
  * Update the role of the user
  *
  */
    updateRole(data:any,token:string):Promise<any>{

        return new Promise<any>((resolve,reject) => {

         this.api.post(`${this.prefix}user/updateRole?token=${token}`,data)

            .subscribe(res => {
                // If the API returned a successful response, mark the user as logged in
                if (res&&res.reponse==='success') {

                    if(res.user)
                        this.storage.set('user',res.user);
                    if(res.classe)
                        this.storage.set('user-classes',res.classe);
                    if(res.userClasse)
                        this.storage.set('user-school',res.userClasse);
                    if(res.matieres&&res.matieres.length>0)
                        this.storage.set('user-matieres',res.matieres);

                    this.userInfos(token);
                }

                resolve(res);

            }, err => {

                if(err) {
                    if (err.error && err.error.reponse) {
                        this.handleUserError(err.error.reponse);
                    } else {
                        this.handle_user_error(err);
                    }
                }
                reject(err);

            });

        });

    }


    /*
   * Create a student account by a teacher or parent
   *
   */
    createAccount(data:any,token:string):Promise<any>{

        return new Promise<any>((resolve,reject) => {

        this.api.post(`${this.prefix}user/addstudent?token=${token}`,data)
        
            .subscribe(res => {
                // If the API returned a successful response, mark the user as logged in
                if (res&&res.token==='success') {
                    this.storage.set('user-students',res);
                }else{
                    this.handleUserError(res.token);
                }
                resolve(res);
            },err=>{

                if(err) {
                    if (err && err.token) {
                        this.handleUserError(err.token);
                    } else {
                        this.handle_user_error(err);
                    }
                }
                reject(err);
            });

        });

    }


    /**
     * Get the list of student
     */
    getAllStudent(token:string):Promise<any>{

        return new Promise<any>((resolve,reject) => {

            this.api.get(`${this.prefix}user/student?token=${token}`)

            .subscribe(res=>{

            if(res&&res.students.length>0){
                this.storage.set('user-students',res);
            }
            resolve(res);
        },err=>{
            reject(err);
            if(err)
                this.handle_user_error(err);
        });

        });
    }


    /**
     * Get the list of friends
     */
    getAllFriends(token:string):Promise<any>{

        return new Promise<any>((resolve,reject) => { 

        this.api.get(`${this.prefix}user/getfriends?token=${token}`)

        .subscribe(data=>{

            if(data&&data.reponse=="success"){
                this.storage.set('user-friends',data.friends);
            }
            resolve(data);
        },err=>{
            if(err)
                this.handle_user_error(err);

            reject(err);    
        });

        });
    }


    /**
     * Get the list of users by courses
     */
    getAllUsersByCourses(token:string):Promise<any>{

        return new Promise<any>((resolve,reject) => { 

         this.api.get(`${this.prefix}user/studentsByCourse?token=${token}`)

        .subscribe(data=>{

            if(data&&data.reponse=="success"){
                this.storage.set('user-courses',data);
            }
            resolve(data);
        },err=>{
            if(err)
                this.handle_user_error(err);

            reject(err);    
        });

    });
    }



    /**
     * Find a student to invite
     */
    searchStudent(data:any,token:string):Observable<any>{

        let seq = this.api.post(`${this.prefix}user/search?token=${token}`,data);

        seq.subscribe(res=>{

        },err=>{
            if(err)
                this.handle_user_error(err);
        });

        return seq;
    }

    /**
     * Send an invitation to a user
     */

    sendInvitation(data:any,token:string):Promise<any>{

        return new Promise<any>((resolve,reject) => { 

        this.api.post(`${this.prefix}user/invitation/send?token=${token}`,data)

        .subscribe(res=>{
            resolve(res);

        },err=>{
            reject(err);
            this.handle_user_error(err);
        });

        });

    }



    /**
     * Accept a user the invitation
     */

    acceptedInvitation(data:any,token:string):Promise<any>{

        return new Promise<any>((resolve,reject) => { 

        this.api.post(`${this.prefix}user/invitation/request?token=${token}`,data)

        .subscribe(res=>{
            resolve(res);

        },err=>{
            reject(err);
            this.handle_user_error(err);
        });

        });
    }


    /**
     * Accept a user the invitation
     */

    invitations(token:string):Promise<any>{

        return new Promise<any>((resolve,reject) => { 

        this.api.post(`${this.prefix}user/invitations?token=${token}`,{})

        .subscribe(data=>{

            if(data.reponse==="success"){
                if(data&&data.invitations.length>0){
                    this.storage.set('user-invitations',data.invitations);
                }else{
                    this.storage.set('user-invitations',[]);
                }
            }

            resolve(data);

        },err=>{
            reject(err);
            this.handle_user_error(err);
        });

        });
    }



    /**
     * send report period
     */

    reportPeriod(data:any,token:string):Promise<any>{

        return new Promise<any>((resolve,reject) => { 

        this.api.post(`${this.prefix}qcm/parameter?token=${token}`,data)

        .subscribe(data=>{

            if(data==="updated"){

                this.translate.get(['PERIODE_SUCCES']).subscribe(value=>{
                    this.toastAlert(value.PERIODE_SUCCES);
                });

            }else{

                this.translate.get(['PERIODE_ERROR']).subscribe(value=>{
                    this.toastAlert(value.PERIODE_ERROR);
                });
            }
            resolve(data);

        },err=>{
            reject(err);
            this.handle_user_error(err);
        });

    });
    }


    /**
     * Log the user out, which forgets the session
     */
    logout() {

        this.storage.remove('role');
        this.storage.remove('hasCode');
        this.storage.remove('posts');
        this.storage.remove('posts-comments');
        this.storage.remove('annee-academique');
        this.storage.remove('device_id');
        this.storage.remove('token');
        this.storage.remove('user');
        this.storage.remove('user-is-login');
        this.storage.remove('user-classes');
        this.storage.remove('user-timetable');
        this.storage.remove('user-coupons');
        this.storage.remove('user-matieres');
        this.storage.remove('user-niveaux');
        this.storage.remove('user-friends');
        this.storage.remove('user-students');
        this.storage.remove('user-friends');
        this.storage.remove('user-courses');
        this.storage.remove('user-data');
        this.storage.remove('user-invitations');
        this.storage.remove('user-matiere-niveaux');
        this.storage.remove('user-results');
        this.storage.remove('niveaux-matieres');
        this.storage.remove('matieres');
        this.storage.remove('my-challenges');
        this.storage.remove('challenges-invites');
        this.storage.remove('challenges-participated');
        this.storage.remove('challenges-accepted');
        this.storage.remove('countries');
        this.storage.remove('classes');
        this.storage.remove('classes-by-country');
        this.storage.remove('classe_matieres');
        this.storage.remove('course-lessons');
        this.storage.remove('courses-lessons');
        this.storage.remove('course-results');
        this.storage.remove('lesson-results');
        this.storage.remove('lessons-levels');
        this.storage.remove('subjects-group');
        this.storage.remove('group-subjects');

        this.storage.clear();
        //Make a forEach in lessons{id} for deletion.

    }

    /**
     * Process a login/signup response to store user data
     */
    _loggedIn(resp,data) {

        this.storage.set('user-is-login',true);
        if(resp.user)
            this.storage.set('user',resp.user);
        if(resp.roles)
            this.storage.set('role',resp.roles);
        if(resp.annee){
            this.storage.set('annee-academique',resp.annee);
        }

        if(resp.token)
            this.storage.set('token',resp.token);
        if(data)
            this.storage.set('password',data);
    }


    /**
     *  Handle  errors from serveur 
     */
    handleUserError(error:any){

        switch (error){

            //signup error
            case  "phone_already_exist" :
                this.translate.get(['PHONE_ALREADY_EXIST']).subscribe((value) => {
                    //alert the user
                    this.toastAlert(value.PHONE_ALREADY_EXIST);
                });
                break;

            case  "email_already_exist" :
                this.translate.get(['EMAIL_ALREADY_EXIST']).subscribe((value) => {
                    //alert the user
                    this.toastAlert(value.EMAIL_ALREADY_EXIST);
                });
                break;

            //

            case  "username_existe" :
                this.translate.get(['USERNAME_EXIST']).subscribe((value) => {
                    //alert the user
                    this.toastAlert(value.USERNAME_EXIST);
                });
                break;

            case  "phone_existe" :
                this.translate.get(['PHONE_ALREADY_EXIST']).subscribe((value) => {
                    //alert the user
                    this.toastAlert(value.PHONE_ALREADY_EXIST);
                });
                break;

            case  "email_existe" :
                this.translate.get(['EMAIL_ALREADY_EXIST']).subscribe((value) => {
                    //alert the user
                    this.toastAlert(value.EMAIL_ALREADY_EXIST);
                });
                break;

            case  "user_already_exist" :
                this.translate.get(['USER_ALREADY_EXIST']).subscribe((value) => {
                    //alert the user
                    this.toastAlert(value.USER_ALREADY_EXIST);
                });
                break;

            //signin error
            case  "username_not_exist" :
                this.translate.get(['USERNAME_NOT_EXIST']).subscribe((value) => {
                    //alert the user
                    this.toastAlert(value.USERNAME_NOT_EXIST);
                });
                break;

            case  "invalid_credentials" :
                this.translate.get(['INVALID_CREDENTIALS']).subscribe((value) => {
                    //alert the user
                    this.toastAlert(value.INVALID_CREDENTIALS);
                });
                break;

            //Reset password error

            case  "email_not_exist" :
                this.translate.get(['EMAIL_NOT_EXIST']).subscribe((value) => {
                    //alert the user
                    this.toastAlert(value.EMAIL_NOT_EXIST);
                });
                break;

            case  "phone_not_exist" :
                this.translate.get(['PHONE_NOT_EXIST']).subscribe((value) => {
                    //alert the user
                    this.toastAlert(value.PHONE_NOT_EXIST);
                });
                break;

            case  "bad_code" :
                this.translate.get(['BAD_CODE']).subscribe((value) => {
                    //alert the user
                    this.toastAlert(value.BAD_CODE);
                });
                break;

            // common error
            case  "failure" :
                this.translate.get(['FAILURE']).subscribe((value) => {
                    //alert the user
                    this.toastAlert(value.FAILURE);
                });
                break;
        }

    }


    //Register a pushNotification
   registerPush() {
        this.onesignal.getIds().then( ids=> {
            if(ids&&ids.userId){
                this.storage.set('device_id',ids.userId);
                this.playerId=ids.userId;
            }else{
                this.storage.get('device_id').then(playerId=>{
                    if(playerId){
                        this.playerId=playerId;
                    }
                });
            }
        });
    }



    /*
     * Refresh the user token. Allow the user to renew his token
    * */

    refreshToken(){

      this.registerPush();

        //we get the store data of the user
        this.storage.get('user').then(data=>{

            if(data){

                //We get the password
                this.storage.get('password').then(pass=>{

                    if(pass){

                        let param={
                            "username_or_email":data.email||data.username,
                            "password":pass,
                            "token_notification":this.playerId
                        };

                        let userData={user:param};
                        // Login to refresh the password
                        this.login(userData);
                    }

                });

            }
        });
    }

    /*
     * Hamdle error
     */

    handle_user_error(error){

        switch (parseInt(error.status)){

            case 0:
                this.translate.get(['INTERNET_ERROR']).subscribe((value) => {
                    this.toastAlert(value.INTERNET_ERROR);
                });
                break;

            case 400:
                this.refreshToken();
                break;

            case 401:
                this.refreshToken();
                break;

            case 500:
                this.translate.get(['SERVER_ERROR']).subscribe((value) => {
                    this.toastAlert(value.SERVER_ERROR);
                });
                break;
            default:
                break;

        }

    }

}
